import { browser, by, element } from 'protractor';

/**
 * The app page.
 */
export class AppPage {
  /**
   * navigateTo
   * @returns {promise.Promise<any>}
   */
  navigateTo() {
    return browser.get('/');
  }

  /**
   * getParagraphText
   * @returns {any}
   */
  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}

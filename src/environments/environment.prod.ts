export const environment = {
  production: true,
  envName: 'prod',
  ayudas: {
    backBaseUrl: 'https://sae-back.herokuapp.com'
  },
  commonData: {
    backBaseUrl: 'http://192.168.70.95:9017/'
  },
};

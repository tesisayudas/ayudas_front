// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  envName: 'dev',
  ayudas: {
    backBaseUrl: 'http://localhost:9000'
    // backBaseUrl: 'http://192.168.1.8:9002'
  },
  commonData: {
    backBaseUrl: 'http://172.17.22.52:9001'
  },
};

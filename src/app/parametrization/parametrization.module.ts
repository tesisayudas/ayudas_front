import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParametrizationRoutingModule } from './parametrization-routing.module';
import {RequesterTypesService} from "./requester-types/requester-types.service";

@NgModule({
  imports: [
    CommonModule,
    ParametrizationRoutingModule
  ],
  declarations: [],
  providers: [RequesterTypesService]
})
export class ParametrizationModule { }

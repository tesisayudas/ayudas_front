import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'resource_types', loadChildren: './resource-types/resource-types.module#ResourceTypesModule'},
  {path: 'requester_types', loadChildren: './requester-types/requester-types.module#RequesterTypesModule'},
  {path: 'unities', loadChildren: './unities/unities.module#UnitiesModule'},
  {path: 'resource_statuses', loadChildren: './resource-statuses/resource-statuses.module#ResourceStatusesModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametrizationRoutingModule { }

import {Component, ViewChild} from '@angular/core';
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {RequesterTypesFormComponent} from "../requester-types-form/requester-types-form.component";
import {RequesterTypesService} from "../requester-types.service";

@Component({
  selector: 'g3-requester-types-list',
  template: `
    <g3-fab (action)="openForm()"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)"
                     [itemList]="itemList"
                     [totalItems]="totalItems"
                     [config]="tableConfig">
    </g3-table-custom>
    <g3-requester-types-form></g3-requester-types-form>
  `,
  styles: []
})
export class RequesterTypesListComponent extends BasicItemListComponent<RequesterTypesService>  {

  @ViewChild(RequesterTypesFormComponent) form : RequesterTypesFormComponent;

  constructor( public service: RequesterTypesService, public paginator: PaginatorService) {
    super(service, paginator);
  }

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, null, null, null);
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.name')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-8')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .build();

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequesterTypesRoutingModule } from './requester-types-routing.module';
import { RequesterTypesListComponent } from './requester-types-list/requester-types-list.component';
import { RequesterTypesFormComponent } from './requester-types-form/requester-types-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CrudModule} from "@g3/crud/crud.module";
import {SoriModule} from "../../sori/sori.module";

@NgModule({
  imports: [
    CommonModule,
    RequesterTypesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [RequesterTypesListComponent, RequesterTypesFormComponent],
})
export class RequesterTypesModule { }

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RequesterTypesListComponent} from "./requester-types-list/requester-types-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: RequesterTypesListComponent,
    data: {
      titleKey: 'navigation.requester_types',
      resource: ACCESS_RESOURCES.requesterType
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequesterTypesRoutingModule {
}

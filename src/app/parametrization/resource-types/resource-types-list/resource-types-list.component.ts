import {Component, OnInit} from '@angular/core';
import {ResourceTypesService} from "../resource-types.service";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {ActivatedRoute, Router} from "@angular/router";
import {NavigationBackService} from "@g3/core/navigation-back.service";

@Component({
  selector: 'g3-resource-types-list',
  template: `
    <g3-fab [routerLink]="'../create'" ></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)" 
                     [itemList]="itemList" 
                     [totalItems]="totalItems" 
                     [config]="tableConfig">
    </g3-table-custom>
  `,
  styles: []
})
export class ResourceTypesListComponent extends BasicItemListComponent<ResourceTypesService> implements OnInit{

  constructor( public service: ResourceTypesService,
               public paginator: PaginatorService,
               private router: Router,
               private route: ActivatedRoute,
               private navigationBackService: NavigationBackService) {
    super(service, paginator);
  }

  ngOnInit(): void {
    this.navigationBackService.urlBack = this.router.url;
    this.goToDetails = ((entity) => this.router.navigate([`../${entity.id}/details`], {relativeTo: this.route}));
    this.goToEdit = ((entity) => this.router.navigate([`../${entity.id}/edit`], {relativeTo:this.route}));
  }

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(null, null, null, null);
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .add(GenericButton.View())
      .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.name')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-4')
            .showInMobile(true)
            .build()
        )
        .addColumn(
          new ColumnBuilder()
            .label('resource_types.details.extra')
            .key('extra')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-4')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .build();
}

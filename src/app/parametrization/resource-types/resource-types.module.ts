import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResourceTypesRoutingModule} from './resource-types-routing.module';
import {ResourceTypesListComponent} from './resource-types-list/resource-types-list.component';
import {ResourceTypesFormComponent} from './resource-types-form/resource-types-form.component';
import {ResourceTypesService} from "./resource-types.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../../sori/sori.module";
import {CrudModule} from "@g3/crud/crud.module";
import { ResourceTypesDetailsComponent } from './resource-types-details/resource-types-details.component';

@NgModule({
  imports: [
    CommonModule,
    ResourceTypesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [ResourceTypesListComponent, ResourceTypesFormComponent, ResourceTypesDetailsComponent],
  providers: [ResourceTypesService]
})
export class ResourceTypesModule {
}

import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {HttpClient} from "@angular/common/http";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {URLS} from "../../sori/elements";
import {HttpUtils} from "@g3/crud/model/http.utils";

@Injectable()
export class ResourceTypesService extends BackItemCrudManager implements Resolve<any>{
  constructor(private http: HttpClient, private router: Router){
    super(http, URLS.resource_types, router);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.getById(route.params.id).then(res => res)
  }

  uploadImage(id, body){
    return this.http.post(`${this.defaultCrudUrls.basicUrl()}/${id}`, body).toPromise().catch((error) => HttpUtils.handleError(error, this.router));
  }
}

import {AfterViewInit, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {FormBuilder, Validators} from "@angular/forms";
import {ResourceTypesService} from "../resource-types.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {RequesterTypesService} from "../../requester-types/requester-types.service";
import {SelectControlComponent} from "../../../shared/form-control/form-control-select/select-control/select-control.component";
import {SingleFileComponent} from "../../../shared/file-form/single-file/single-file.component";
import {NavigationBackService} from "@g3/core/navigation-back.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'g3-resource-types-form',
  template: `    
    <div class="mdl-card mdl-card__supporting-text mdl-shadow--2dp g3-mdl-card__select">
      <h4 class="g3-text-color__primary bold">
        <span *ngIf="isUpdate" g3I18n>resource_types.update</span>
        <span *ngIf="!isUpdate" g3I18n>resource_types.create</span>
      </h4>
      <form id="resourceTypeForm" [formGroup]="customForm.form">
        <!--Name-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="name" id="name"/>
          <label class="mdl-textfield__label" for="name">{{'shared.name' | i18n}}*</label>
        </div>
        <!--allowedRequesters-->
        <g3-select-control [isRequired]="true"
                           [formGroup]="customForm.form"
                           [results]="requesterTypes"
                           [idControl]="'allowedRequesters'"
                           [labelName]="'resource_types.requester_types'"
                           [titleBox]="'resource_types.allowed_requester'">
        </g3-select-control>
        <!--File-->
        <g3-single-file [form]="customForm.form" [isUpdate]="isUpdate" [validFormats]="validFormats"></g3-single-file>
        <!-- Description -->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <textarea class="mdl-textfield__input" rows="3" formControlName="description" id="description"></textarea>
          <label class="mdl-textfield__label" for="description">{{'shared.description' | i18n}}</label>
        </div>
        <!--IsExtra-->
        <div>
          <h5>{{'resource_types.isExtra' | i18n}}</h5>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1" g3UpgradeElements>
            <input type="radio" id="option-1" formControlName="extra" class="mdl-radio__button" value="true">
            <span class="mdl-radio__label" g3I18n>shared.yes</span>&nbsp;&nbsp;
          </label>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2" g3UpgradeElements>
            <input type="radio" id="option-2" formControlName="extra" class="mdl-radio__button" value="false">
            <span class="mdl-radio__label" g3I18n>shared.not</span>
          </label>
        </div>        
      </form>
      
      <div class="mdl-dialog__actions">
        <button class="mdl-button close" [disabled]="validator.invalid" (click)=customForm.save()
                form="resourceTypeForm"
                g3I18n>shared.save
        </button>
        <button class="mdl-button close" (click)="back()" g3I18n>shared.cancel</button>
      </div>
    </div>
  `,
  styles: []
})
export class ResourceTypesFormComponent implements AfterViewInit{

  customForm: CustomForm;
  validator: CustomFormValidator;
  isUpdate: boolean = false;
  validFormats = ['png', 'jpg', 'jpeg'];
  requesterTypes: any[] = [];
  entity;
  @ViewChild(SelectControlComponent) select: SelectControlComponent;
  @ViewChild(SingleFileComponent) singleFile: SingleFileComponent;
  constructor(private fb: FormBuilder,
              private service: ResourceTypesService,
              private requesterTypeService: RequesterTypesService,
              private i18n: I18nService,
              private snack: SnackbarService,
              private navigationBackService: NavigationBackService,
              private route: ActivatedRoute,
              private cd: ChangeDetectorRef) {
    this.navigationBackService.urlBack = 'parametrization/resource_types';
    this.initForm();
    this.putRequesterTypes();
    this.validator = CustomFormUtils.addEditNoChangeValidator(this.customForm);
  }

  ngAfterViewInit(): void {
    const res = this.route.snapshot.data.res;
    if (!!res) {
      this.setEntity(res);
    }
    this.cd.detectChanges();
  }

  back() {
    this.navigationBackService.goToBack();
  }

  setEntity(entity) {
    entity.extra = entity.extra ? "true" : "false";
    this.select.patchValue(entity.allowedRequesters);
    this.customForm.setValue(entity);
    this.validator.active();
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        name: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])],
        photoUrl: [''],
        description:[''],
        extra: [false, Validators.compose([CustomValidators.requiredWithTrim])],
        allowedRequesters: this.fb.array([], CustomValidators.requiredWithTrim)
      })
      .handlerSaveResponse((response, action)=> {
        CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18n);
        this.saveFile(response).then(() => {
          //const id = (response && response.id) ? response.id : this.customForm.form.get('id').value;
          CustomFormUtils.showToastConfirmation(action, this.i18n, this.snack);
          this.back();
        })
      })
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }

  private putRequesterTypes() {
    this.requesterTypeService.getEnabledList().then(res => this.requesterTypes = res);
  }

  saveFile(response): Promise<any> {
    const id = response && response.id ? response.id : this.customForm.form.get('id').value;
    if (id && this.singleFile.file) {
      const body = new FormData();
      body.append('documentUrl', this.singleFile.file);
      return this.service.uploadImage(id, body)
    }
    return Promise.resolve(null);
  }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ResourceTypesService} from "../resource-types.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'g3-resource-types-details',
  templateUrl: './resource-types-details.component.html',
})
export class ResourceTypesDetailsComponent implements OnInit{

  resourceType: any;
  allowedRequesters: string[];
  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: ResourceTypesService) { }

  ngOnInit() {
    this.route.data.pipe(map(res => res.resourceType)).subscribe(res => {
      this.resourceType = res;
      this.allowedRequesters = res.allowedRequesters.map(requester => requester.name);
    });
  }

}

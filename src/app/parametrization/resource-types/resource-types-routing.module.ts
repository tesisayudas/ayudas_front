import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResourceTypesListComponent} from "./resource-types-list/resource-types-list.component";
import {ResourceTypesDetailsComponent} from "./resource-types-details/resource-types-details.component";
import {ResourceTypesService} from "./resource-types.service";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";
import {ResourceTypesFormComponent} from "./resource-types-form/resource-types-form.component";

const routes: Routes = [
  {
    path: '',
    component: ResourceTypesListComponent,
    data: {
      titleKey: 'navigation.resource_types',
      resource: ACCESS_RESOURCES.resourceType
    }
  },
  {
    path: 'create',
    component: ResourceTypesFormComponent,
    data: {
      titleKey: 'navigation.resource_types',
      resource: ACCESS_RESOURCES.resourceType
    }
  },
  {
    path: ':id/edit',
    component: ResourceTypesFormComponent,
    data: {
      titleKey: 'navigation.resource_types',
      resource: ACCESS_RESOURCES.resourceType
    },
    resolve: {
      res: ResourceTypesService
    }
  },
  {
    path: ':id/details',
    component: ResourceTypesDetailsComponent,
    data: {
      titleKey: 'navigation.resource_types',
      resource: ACCESS_RESOURCES.resourceType
    },
    resolve: {
      resourceType: ResourceTypesService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceTypesRoutingModule {
}

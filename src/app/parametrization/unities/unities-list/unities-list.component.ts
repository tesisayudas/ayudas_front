import {Component, ViewChild} from '@angular/core';
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {UnitiesService} from "../unities.service";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {ChangeStatusComponent} from "@g3/crud/view/change-status/change-status.component";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {UnitiesFormComponent} from "../unities-form/unities-form.component";
import {UnitiesDetailsComponent} from "../unities-details/unities-details.component";

@Component({
  selector: 'g3-unities-list',
  template: `
    <g3-fab (action)="openForm()"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)" 
                     [itemList]="itemList" 
                     [totalItems]="totalItems" 
                     [config]="tableConfig">
    </g3-table-custom>
    <g3-change-status idDialog="confirmation" [service]="service" 
                      (onChangeData)="responseDatachanged($event)"></g3-change-status>
    <g3-unities-form></g3-unities-form>
    <g3-unities-details></g3-unities-details>
  `,
  styles: []
})
export class UnitiesListComponent extends BasicItemListComponent<UnitiesService> {

  @ViewChild(ChangeStatusComponent) stateComponent: ChangeStatusComponent;
  @ViewChild(UnitiesFormComponent) form : UnitiesFormComponent;
  @ViewChild(UnitiesDetailsComponent) details: UnitiesDetailsComponent;

  constructor( public service: UnitiesService, public paginator: PaginatorService) {
    super(service, paginator);
  }

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, this.details, this.stateComponent, null);
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .add(GenericButton.View())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.name')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-4')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .build();

}

import {Component, OnInit, ViewChild} from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {FormBuilder, Validators} from "@angular/forms";
import {RequesterTypesService} from "../../requester-types/requester-types.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {UnitiesService} from "../unities.service";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {MultiDataComponent} from "../../../shared/multi-data/multi-data.component";

@Component({
  selector: 'g3-unities-form',
  template: `
    <g3-dialog-form idDialog="unityDialog" [form]="dialogForm" [validatorForm]="validator" idForm="unityForm">
      <form id="unityForm" [formGroup]="customForm.form">
        <!--Name-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="name" id="name"/>
          <label class="mdl-textfield__label" for="name">{{'shared.name' | i18n}}*</label>
        </div>
        <!--classRooms-->
        <g3-multi-data [isRequired]="true" 
                       [formGroup]="customForm.form" 
                       [idControl]="'classrooms'" 
                       [labelName]="'unities.class_rooms'"></g3-multi-data>
      </form>
    </g3-dialog-form>
  `,
  styles: []
})
export class UnitiesFormComponent implements ItemFormDialog, OnInit {

  customForm: CustomForm;
  dialogForm: CustomFormDialog;
  validator: CustomFormValidator;
  @ViewChild(MultiDataComponent) multi: MultiDataComponent;

  constructor(private fb: FormBuilder,
              private service: UnitiesService,
              private requesterTypeService: RequesterTypesService,
              private i18n: I18nService,
              private snack: SnackbarService) { }

  ngOnInit() {
    this.initForm();
    this.initDialog();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  open() {
    this.dialogForm.open();
  }

  setEntity(entity) {
    this.multi.patchValue(entity.classrooms);
    this.dialogForm.setValue(entity);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        name: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])],
        classrooms: this.fb.array([], CustomValidators.requiredWithTrim)
      })
      .handlerSaveResponse((response, action)=> CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n, this.snack))
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }

  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('unities.create')
      .updateTitleKey('unities.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .handlerCloseDialog(() => {
        this.multi.clear();
        this.customForm.reset();
      })
      .build()
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnitiesRoutingModule } from './unities-routing.module';
import { UnitiesListComponent } from './unities-list/unities-list.component';
import { UnitiesFormComponent } from './unities-form/unities-form.component';
import {SoriModule} from "../../sori/sori.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CrudModule} from "@g3/crud/crud.module";
import {UnitiesService} from "./unities.service";
import { UnitiesDetailsComponent } from './unities-details/unities-details.component';

@NgModule({
  imports: [
    CommonModule,
    UnitiesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [UnitiesListComponent, UnitiesFormComponent, UnitiesDetailsComponent],
  providers: [UnitiesService]
})
export class UnitiesModule { }

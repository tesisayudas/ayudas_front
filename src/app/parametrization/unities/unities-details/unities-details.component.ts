import {Component, OnInit, ViewChild} from '@angular/core';
import {ItemDetailsDialog} from "@g3/crud/controller/controller-interfaces";
import {DetailsDialogComponent} from "@g3/crud/view/details-dialog/details-dialog.component";

@Component({
  selector: 'g3-unities-details',
  template: `
    <g3-details-dialog [idDialog]="'unitiesDetailsDialog'" [attrTitle]="'name'">
      <div>
        <p> <strong>{{'unities.class_rooms' | i18n}}:</strong> {{classRooms.join(',')}}</p>
      </div>
    </g3-details-dialog>
  `
})
export class UnitiesDetailsComponent implements OnInit, ItemDetailsDialog {

  classRooms: string[] = [];
  @ViewChild(DetailsDialogComponent) dialog: DetailsDialogComponent;

  constructor() { }

  ngOnInit() {
  }

  open() {
    this.dialog.open();
  }

  setEntity(entity) {
    this.dialog.setEntity(entity);
    this.classRooms = entity.classrooms.map((classRoom) => classRoom.internalCode);
  }

}

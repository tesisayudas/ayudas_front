import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UnitiesListComponent} from "./unities-list/unities-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '', component: UnitiesListComponent,
    data: {
      titleKey: 'navigation.unities',
      resource: ACCESS_RESOURCES.unity
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitiesRoutingModule {
}

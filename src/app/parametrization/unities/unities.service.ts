import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {URLS} from "../../sori/elements";

@Injectable()
export class UnitiesService extends BackItemCrudManager {

  constructor(private http: HttpClient, private router: Router ) {
    super(http, URLS.unities, router);
  }

}

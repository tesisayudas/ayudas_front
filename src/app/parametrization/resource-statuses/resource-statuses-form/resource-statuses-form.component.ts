import {Component} from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {FormBuilder, Validators} from "@angular/forms";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {ResourceStatusesService} from "../resource-statuses.service";


@Component({
  selector: 'g3-resource-statuses-form',
  template: `
    <g3-dialog-form idDialog="resourceStatusDialog" [form]="dialogForm" [validatorForm]="validator" idForm="resourceStatusForm">
      <form id="resourceStatusForm" [formGroup]="customForm.form">
        <!--Name-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="name" id="name"/>
          <label class="mdl-textfield__label" for="name">{{'shared.name' | i18n}}*</label>
        </div>
        <div>
          <h5>{{'resource_statuses.is_borrowable' | i18n}}</h5>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1" g3UpgradeElements>
            <input type="radio" id="option-1" formControlName="borrowable" class="mdl-radio__button" value="true">
            <span class="mdl-radio__label" g3I18n>shared.yes</span>&nbsp;&nbsp;
          </label>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2" g3UpgradeElements>
            <input type="radio" id="option-2" formControlName="borrowable" class="mdl-radio__button" value="false">
            <span class="mdl-radio__label" g3I18n>shared.not</span>
          </label>
        </div>
      </form>
    </g3-dialog-form>
  `,
  styles: []
})
export class ResourceStatusesFormComponent implements ItemFormDialog{

  dialogForm: CustomFormDialog;
  customForm: CustomForm;
  validator: CustomFormValidator;

  constructor(private fb: FormBuilder,
              private service: ResourceStatusesService,
              private i18n: I18nService,
              private snack: SnackbarService) {
    this.initForm();
    this.initDialog();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  open() {
    this.dialogForm.open();
  }

  setEntity(entity) {
    entity.borrowable = entity.borrowable.toString();
    this.dialogForm.setValue(entity);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        name: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])],
        borrowable: [false, Validators.compose([CustomValidators.requiredWithTrim])]
      })
      .handlerSaveResponse((response, action)=> CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n, this.snack))
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }


  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('resource_statuses.create')
      .updateTitleKey('resource_statuses.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .build()
  }

}

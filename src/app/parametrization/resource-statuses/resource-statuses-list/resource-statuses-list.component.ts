import {Component, ViewChild} from '@angular/core';
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {ResourceStatusesService} from "../resource-statuses.service";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {ResourceStatusesFormComponent} from "../resource-statuses-form/resource-statuses-form.component";

@Component({
  selector: 'g3-resource-statuses-list',
  template: `
    <g3-fab (action)="openForm()"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)"
                     [itemList]="itemList"
                     [totalItems]="totalItems"
                     [config]="tableConfig">
    </g3-table-custom>
    <g3-resource-statuses-form></g3-resource-statuses-form>
  `,
  styles: []
})
export class ResourceStatusesListComponent extends BasicItemListComponent<ResourceStatusesService> {

  @ViewChild(ResourceStatusesFormComponent) form: ResourceStatusesFormComponent;

  constructor(public service: ResourceStatusesService,
              public paginator: PaginatorService) {
    super(service, paginator);
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.name')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-8')
            .showInMobile(true)
            .build()
        )
        .addColumn(new ColumnBuilder()
          .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-1')
          .showInMobile(true)
          .key('borrowable')
          .label('resource_statuses.borrowable').build())
        .build()
    )
    .build();

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, null, null, null);
  }

}

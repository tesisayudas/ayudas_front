import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResourceStatusesRoutingModule } from './resource-statuses-routing.module';
import { ResourceStatusesListComponent } from './resource-statuses-list/resource-statuses-list.component';
import { ResourceStatusesFormComponent } from './resource-statuses-form/resource-statuses-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../../sori/sori.module";
import {CrudModule} from "@g3/crud/crud.module";
import {ResourceStatusesService} from "./resource-statuses.service";

@NgModule({
  imports: [
    CommonModule,
    ResourceStatusesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [ResourceStatusesListComponent, ResourceStatusesFormComponent],
  providers: [ResourceStatusesService]
})
export class ResourceStatusesModule { }

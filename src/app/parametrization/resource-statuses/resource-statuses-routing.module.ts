import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResourceStatusesListComponent} from "./resource-statuses-list/resource-statuses-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: ResourceStatusesListComponent,
    data: {
      titleKey: 'navigation.resource_statuses',
      resource: ACCESS_RESOURCES.resourceStatus
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceStatusesRoutingModule { }

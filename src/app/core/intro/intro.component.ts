import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {I18nService} from '../i18n/i18n.service';
import {Router} from "@angular/router";

/**
 * Component of introduction.
 */
@Component({
  selector: 'g3-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class IntroComponent implements OnInit{

  /**
   * The title of the introduction.
   * @item {string}
   */
  title = "default-title";

  firstDialogStatus: string;

  /**
   * Create a new component of introduction, transforming the title key to value depend on current language.
   * @param {I18nService} translate
   */
  constructor(private translate: I18nService) {
    this.translate.translator$.subscribe(translator => {
      this.title = translator.getValue("title");
    });
    this.translate.update();
  }

  ngOnInit() {

  }

  statusDialogHandler(status) {
    this.firstDialogStatus = status;
  }

}

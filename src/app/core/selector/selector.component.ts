import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {I18nService} from '../i18n/i18n.service';
import {SnackbarService} from "../snackbar/snackbar.service";
import {HandlerSidebar} from "@g3/core/loading.component";

/**
 * Selector of languages.
 */
@Component({
  selector: 'g3-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent {

  /**
   * The current language name.
   */
  public currentLanguageName: Observable<string>;

  /**
   * The supported languages.
   */
  public supportedLanguages: Promise<any[]>;

  /**
   * Create a new selector of languages, taking the supported languages.
   * @param {I18nService} translate
   */
  constructor(private translate: I18nService, private snackbarService: SnackbarService) {
    this.supportedLanguages = this.translate.supportedLanguage;
    this.translate.metadataLanguage$.subscribe(metadataLanguage => {
      this.currentLanguageName = (metadataLanguage && metadataLanguage.name) || "";
    });
  }

  /**
   * Display the selector of languages.
   * @param {string} status
   */
  displayLanguageSelector(status: string) {
    let wnd = document.getElementById("languages-list");
    if (status == 'open') {
      wnd.style.maxHeight = "150px";
    } else if (status == 'close') {
      wnd.style.maxHeight = "0";
    }
  }

  /**
   * Set the language for the application.
   * @param language
   */
  selectLanguage(language: any) {
    this.displayLanguageSelector('close');
    if (this.translate.getAbbreviationCurrentLanguage() !== language.abbreviation) {
      this.translate.setLanguage(language.abbreviation);
      setTimeout(() => {
        this.snackbarService.showToast(this.translate.getValue(`language.onChange`))
        HandlerSidebar.handleSidebarBehavior();
      },200)
    }
  }

}

import {Component, ViewEncapsulation} from '@angular/core';
import {I18nService} from "../i18n/i18n.service";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {NavigationBackService} from "../navigation-back.service";
import {AuthService} from "@g3/core/auth/auth.service";
import {PermissionService} from "@g3/core/auth/permission.service";

/**
 * The top bar component.
 */
@Component({
  selector: 'g3-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent {

  /**
   * The title of the top bar, this change according to the active nav.
   * @item {string}
   */
  title = "";

  /**
   * The key to change the value of title depend the language.
   * @item {string}
   */
  titleKey = "";

  hasBack: boolean;

  /**
   * Create a new top bar with the title specified in the data.titleKey of the activated route.
   * @param {I18nService} i18n
   * @param {Router} router
   * @param authService
   * @param permissionService
   * @param {ActivatedRoute} activatedRoute
   * @param back
   */
  constructor(private i18n: I18nService,
              private router: Router,
              private authService: AuthService,
              public permissionService: PermissionService,
              private activatedRoute: ActivatedRoute,
              private back: NavigationBackService) {
    this.i18n.translator$.subscribe(translator => this.title = translator.getValue(this.titleKey));
    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while(route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((data) => {
        this.titleKey = data['titleKey'];
        this.title = i18n.getValue(this.titleKey);
        this.hasBack = (this.titleKey && this.titleKey.includes('home')) || (this.title && this.title.includes('404') || !this.titleKey);
      });
  }

  goToRoute() {
    this.back.goToBack();
  }

  login() {
    this.authService.login();
  }

  logout(){
    this.authService.logout().subscribe(res => {
      this.permissionService.closeSession();
      window.location.href = res.url;
    })
  }

}

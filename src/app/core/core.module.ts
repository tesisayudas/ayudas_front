import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SidenavModule} from './sidenav/sidenav.module';
import {DialogsModule} from './dialogs/dialogs.module';
import {SnackbarModule} from './snackbar/snackbar.module';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LayoutComponent} from './layout/layout.component';
import {TopbarComponent} from './topbar/topbar.component';
import {IntroComponent} from './intro/intro.component';
import {I18nModule} from './i18n/i18n.module';
import {SharedModule} from '../shared/shared.module';
import {ClickOutsideModule} from "ng-click-outside";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {UserReporterService} from "@g3/crud/controller/user-reporter.service";
import {NavigationBackService} from "./navigation-back.service";
import {DateUtils} from "./date-utils.service";
import {CoreRoutingModule} from "./core-routing.module";
import {NotFoundComponent} from "./not-found/not-found.component";
import {AuthService} from "@g3/core/auth/auth.service";
import {AuthenticatedComponent} from "@g3/core/auth/authenticated.component";
import {PermissionService} from "@g3/core/auth/permission.service";
import {TokenInterceptor} from "@g3/core/auth/token.interceptor";
import {RoleGuard} from "@g3/core/auth/role.guard";
import {ScopeGuard} from "@g3/core/auth/scope.guard";
import {CacheInterceptor, HttpCacheService} from "./cache.service";
import {NgHttpLoaderModule} from 'ng-http-loader/ng-http-loader.module';
import {LoadingComponent} from "@g3/core/loading.component";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";

@NgModule({
  imports: [
    CommonModule,
    ClickOutsideModule,
    SidenavModule,
    DialogsModule,
    SnackbarModule,
    I18nModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    CoreRoutingModule,
    NgHttpLoaderModule

  ],
  declarations: [
    LayoutComponent,
    TopbarComponent,
    IntroComponent,
    NotFoundComponent,
    AuthenticatedComponent,
    LoadingComponent
  ],
  exports: [
    LayoutComponent,
    LoadingComponent,
    DialogsModule,
    BrowserAnimationsModule
  ],
  providers: [
    UserReporterService,
    NavigationBackService,
    DateUtils,
    HttpCacheService,
    {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true},
    AuthService,
    PermissionService,
    RoleGuard,
    ScopeGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    PaginatorService
  ]
})
export class CoreModule {
}

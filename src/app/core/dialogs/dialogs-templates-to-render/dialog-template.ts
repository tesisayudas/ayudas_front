import {TemplateRef} from "@angular/core";

/**
 * Represent the template of a dialog component.
 */
export class DialogTemplate {
  /**
   * The id of the dialog tag.
   */
  id: string;

  /**
   * The template of the dialog component.
   */
  template: TemplateRef<Object>;

  /**
   * Create a dialog template with the id of its dialog tag.
   * @param {string} id - The id of the dialog tag.
   * @param {TemplateRef<Object>} template - The template of the dialog component.
   */
  constructor(id: string, template: TemplateRef<Object>) {
    this.id = id;
    this.template = template;
  }
}

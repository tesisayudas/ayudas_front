import {Injectable, TemplateRef} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {DialogTemplate} from "./dialog-template";

/**
 * Container of dialogs templates to render.
 */
@Injectable()
export class DialogsTemplatesToRenderService {

  /**
   * The templates of rendered dialogs.
   * @item {Array}
   * @private
   */
  private _dialogsTemplates: DialogTemplate[] = [];

  /**
   * Subject of the last template of dialog added.
   * @item {Subject<DialogTemplate>}
   */
  private dialogTemplateSource = new Subject<DialogTemplate>();

  /**
   * Observable of the last template of dialog added.
   * @item {Observable<DialogTemplate>}
   */
  dialogTemplate$ = this.dialogTemplateSource.asObservable();

  /**
   * Add new dialog template to render.
   * @param {string} idTemplate - The is of the dialog tag.
   * @param {TemplateRef<Object>} template - The template of the dialog component.
   */
  public addDialogTemplate(idTemplate: string, template: TemplateRef<Object>): void {
    const newTemplate = new DialogTemplate(idTemplate, template);
    this._dialogsTemplates.push(newTemplate);
    this.dialogTemplateSource.next(newTemplate);
  }

  /**
   * Give the dialogs templates existing.
   * @returns {DialogTemplate[]} - The dialogs templates existing.
   */
  public get dialogsTemplates(): DialogTemplate[] {
    return this._dialogsTemplates;
  }

}

import {EmbeddedViewRef, Injectable, ViewContainerRef} from '@angular/core';
import {DialogView} from "./dialog-view";

/**
 * Represent the body container.
 */
@Injectable()
export class BodyContainerService {

  /**
   * The body container.
   */
  private _container: ViewContainerRef;

  /**
   * The array of all dialogs view.
   * @item {Array}
   */
  private dialogViews: DialogView[] = [];

  /**
   * The setter method of _container variable.
   * @param {ViewContainerRef} container - The new container to set.
   */
  public set container(container: ViewContainerRef) {
    this._container = container;
  }

  /**
   * Remove of body container the dialog by id.
   * @param {string} id - The id of dialog to remove.
   */
  public removeDialogById(id: string): void {
    const viewDialog = this.dialogViews.find(dialogView => dialogView.id == id);
    if(viewDialog) {
      this.removeDialogView(this.indexOfDialogView(viewDialog.view));
      this.dialogViews.splice(this.dialogViews.indexOf(this.dialogViews.find(c => c.id == id)), 1);
    }
  }

  /**
   * Return the index position the dialog by the dialog view.
   * @param {EmbeddedViewRef<Object>} dialogView - The dialog view to search.
   * @returns {number} - The position index of the dialog view or -1.
   */
  private indexOfDialogView(dialogView: EmbeddedViewRef<Object>): number {
    return this._container.indexOf(dialogView);
  }

  /**
   * Remove dialog view by index position of body container.
   * @param {number} index - The position index of dialog view.
   */
  private removeDialogView(index: number): void {
    this._container.remove(index);
  }

  /**
   * Add to body container one new dialog view.
   * @param {string} id - The unique id of dialog tag html.
   * @param {EmbeddedViewRef<Object>} dialogView - The dialog view.
   */
  addViewDialog(id: string, dialogView: EmbeddedViewRef<Object>) {
    this.dialogViews.push(new DialogView(id, dialogView));
  }

}

import {EmbeddedViewRef} from "@angular/core";

/**
 * Represent a dialog view.
 */
export class DialogView {
  /**
   * Unique id fo the dialog.
   */
  id: string;

  /**
   * Full view of dialog.
   */
  view: EmbeddedViewRef<Object>;

  /**
   * Create a dialog view.
   * @param {string} id - The id of the dialog tag html.
   * @param {EmbeddedViewRef<Object>} view - The full view of the dialog.
   */
  constructor(id: string, view: EmbeddedViewRef<Object>) {
    this.id = id;
    this.view = view;
  }
}

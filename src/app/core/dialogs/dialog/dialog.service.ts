import {Injectable, NgZone} from '@angular/core';
import {DialogComponent} from "../../../shared/dialogs/components/dialog/dialog.component";

/**
 * Represent the dialog component manage (open and close dialogs).
 */
@Injectable()
export class DialogService {

  /**
   * The dialogs component in the application.
   * @item {Array}
   */
  private dialogsComponents: DialogComponent[] = [];

  /**
   * The stack dialogs components that are showing currently.
   * @item {Array}
   */
  private stackDialogsComponents: DialogComponent[] = [];

  /**
   * The fresh open dialog component.
   */
  private dialogComponentTmp: DialogComponent;

  private idDialogToClose: string;

  /**
   * Create a dialogs component manage.
   */
  constructor(private ngZone: NgZone) {
    this.ngZone.runOutsideAngular(() => {
      this.manageEventClick();
      this.manageEventKeyup();
    });
  }

  /**
   * Manage click event to open and close the dialogs.
   */
  private manageEventClick() {
    document.body.addEventListener("click", () => {
      const dialogToClose = this.stackDialogsComponents[0];
      if (dialogToClose && !dialogToClose.clickHere && this.idDialogToClose == dialogToClose.idDialog) {
        this.closeDialogComponent(dialogToClose.idDialog);
      } else if (this.dialogComponentTmp) {
        this.addOpenedDialogComponent(this.dialogComponentTmp);
        this.dialogComponentTmp = null;
      }
      this.stackDialogsComponents.forEach(dialogComponent => {
        dialogComponent.clickHere = false;
      })
    });
  }

  private manageEventKeyup() {
    document.addEventListener('keyup', (event) => {
      if(event.keyCode == 27) {
        this.idDialogToClose && this.closeDialogComponent(this.idDialogToClose);
      }
    });
  }

  /**
   * Return the dialog tag by id (nativeElement).
   * @param {string} id - The id the dialog tag.
   * @returns {any} - The dialog tag (nativeElement) or undefined.
   */
  private static getDialogElementById(id: string): any {
    return document.querySelector('#' + id) as any;
  }

  /**
   * Add the opened dialog to the [stackDialogsComponents]{@link DialogService#stackDialogsComponents} variable.
   * @param {DialogComponent} dialogComponent - The new opened dialog.
   */
  private addOpenedDialogComponent(dialogComponent: DialogComponent) {
    this.stackDialogsComponents.unshift(dialogComponent);
  }

  /**
   * Remove the first opened dialog.
   */
  private removeFirstOpenedDialogComponent() {
    this.stackDialogsComponents.splice(0, 1);
  }

  /**
   * Return the dialog component by id.
   * @param {string} id - The id of the dialog tag.
   * @returns {DialogComponent} - The dialog component found or undefined.
   */
  private getDialogComponentById(id: string): DialogComponent {
    return this.dialogsComponents.find(dialogComponent => dialogComponent.idDialog == id);
  }

  /**
   * Add new dialog component created.
   * @param {DialogComponent} dialogComponent - The new dialog component created.
   */
  newDialogComponent(dialogComponent: DialogComponent) {
    this.dialogsComponents.push(dialogComponent);
  }

  /**
   * Remove a created dialog component.
   * @param {DialogComponent} dialogComponent - The dialog component to remove.
   */
  removeDialogComponent(dialogComponent: DialogComponent) {
    const index = this.dialogsComponents.indexOf(dialogComponent);
    this.dialogsComponents.splice(index, 1);
  }

  /**
   * Show a dialog component by id.
   * @param {string} id - The id of dialog tag.
   */
  showDialogComponent(id: string) {
    let dialog = DialogService.getDialogElementById(id);
    if (dialog) {
      dialog.classList.add('zoomIn');
      dialog.showModal();
      this.idDialogToClose = id;
      this.dialogComponentTmp = this.getDialogComponentById(id);
      this.dialogComponentTmp.changeStatus("opened");
    }
  }

  /**
   * Close the dialog component by id.
   * @param {string} id - The id of dialog tag.
   */
  closeDialogComponent(id: string) {
    let dialog = DialogService.getDialogElementById(id);
    if (dialog) {
      dialog.classList.remove('zoomIn');
      dialog.hasAttribute('open') && dialog.close();
      this.removeFirstOpenedDialogComponent();
      this.getDialogComponentById(id).changeStatus("closed");
    }
    setTimeout(() => {
      if(this.stackDialogsComponents.length >= 1) this.idDialogToClose = this.stackDialogsComponents[this.stackDialogsComponents.length-1].idDialog;
    })
  }

}

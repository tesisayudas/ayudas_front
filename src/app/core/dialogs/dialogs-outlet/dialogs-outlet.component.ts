import {Component, ViewEncapsulation, ViewContainerRef} from '@angular/core';
import {BodyContainerService} from '../body-container/body-container.service';
import {DialogTemplate} from "../dialogs-templates-to-render/dialog-template";
import {DialogsTemplatesToRenderService} from "../dialogs-templates-to-render/render-dialog.service";

/**
 * Represent the place where will show the dialogs components.
 */
@Component({
  selector: 'g3-dialogs-outlet',
  templateUrl: './dialogs-outlet.component.html',
  styleUrls: ['./dialogs-outlet.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DialogsOutletComponent {

  /**
   * Create a {@link DialogOutletComponent} to set the place where the dialogs components will showed.
   * @param {RenderDialogService} renderDialogs - The contain the dialogs components to render.
   * @param {ViewContainerRef} container - The container of the body.
   * @param {BodyContainerService} body - The service that manage the body container.
   */
  constructor(private dialogsTemplatesToRender: DialogsTemplatesToRenderService, private container: ViewContainerRef, private body: BodyContainerService) {
    this.dialogsTemplatesToRender.dialogTemplate$.subscribe(dialogTemplate => this.renderDialogTemplate(dialogTemplate));
    this.body.container = this.container;
  }

  /**
   * Rendering a dialog template beside to {@link DialogOutletComponent}.
   * @param {Template} template - The template of dialog component to render.
   */
  renderDialogTemplate(template: DialogTemplate): void {
    this.body.addViewDialog(template.id, this.container.createEmbeddedView(template.template));
  }

}

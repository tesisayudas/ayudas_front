import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BodyContainerService} from './body-container/body-container.service';
import {DialogsOutletComponent} from './dialogs-outlet/dialogs-outlet.component';
import {DialogService} from "./dialog/dialog.service";
import {DialogsTemplatesToRenderService} from "./dialogs-templates-to-render/render-dialog.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DialogsOutletComponent
  ],
  exports: [
    DialogsOutletComponent
  ],
  providers: [
    DialogService,
    DialogsTemplatesToRenderService,
    BodyContainerService
  ]
})
export class DialogsModule {
}

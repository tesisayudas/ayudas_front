import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {NavsService} from "../sidenav/navs.service";
import { Spinkit } from 'ng-http-loader/spinkits';

/**
 * Represent the layout of the application.
 */
@Component({
  selector: 'g3-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent {

  public soriSpinner = Spinkit.skThreeBounce;

  /**
   * Div container.
   */
  @ViewChild('layout') layoutElement: any;

  /**
   * Div sidenav container.
   */
  @ViewChild('sidenavContainer') sidenavContainer: any;

  /**
   * Create a new layout component.
   * @param {NavsService} navsService
   */
  constructor(private navsService: NavsService){
    this.navsService.currentNav$.subscribe(currentNav => {
      if(currentNav) {
        if(this.layoutElement.nativeElement.classList.contains('is-small-screen')){
          const sidenavContainerClassList = this.sidenavContainer.nativeElement.classList;
          if(sidenavContainerClassList.contains('is-visible')) {
            this.sidenavContainer.nativeElement.setAttribute("aria-hidden", "true");
            sidenavContainerClassList.remove('is-visible');
            document.getElementsByClassName("mdl-layout__obfuscator")[0].classList.remove('is-visible');
          }
        }
      }
    });
  }

}

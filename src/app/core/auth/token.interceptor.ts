import {Injectable, Injector} from "@angular/core";
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../../environments/environment";
import {PermissionService} from "@g3/core/auth/permission.service";
import {SESSION_CONSTS} from "@g3/core/auth/storage";
import {tap} from "rxjs/operators";
import {AuthService} from "@g3/core/auth/auth.service";
import {Router} from "@angular/router";

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

  private readonly noAddToken: string[] = ['login', 'authenticate'];
  private readonly baseAddToken: string[] = [environment.ayudas.backBaseUrl];
  private permissionService: PermissionService;
  private authService: AuthService;
  constructor(private injector: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.permissionService = this.injector.get(PermissionService);
    this.authService = this.injector.get(AuthService);

    if (!this.checkCanToken(req.url)) return next.handle(req);

    const token = sessionStorage.getItem(SESSION_CONSTS.currentUser);
    if (!token) {
      this.removeSession();
      return;
    }

    const customRequest = req.clone({
      setHeaders: { 'session-token': token }
    });

    return next.handle(customRequest).pipe(
      tap(
        (res)=> {
          if (res instanceof HttpResponse) {
            const token = res.headers.get('session-token');
            if (token) {
              this.permissionService.token = token;
            }

          }
        },
        err => this.onError(err)
      )
    );
  }

  private onError(err) {
    if (err && err instanceof HttpErrorResponse) {
      if (err.status === 401) {
        this.removeSession()
      }
    }
  }

  removeSession(){
    const router = this.injector.get(Router);
    sessionStorage.setItem(SESSION_CONSTS.currentPath, router.url);
    this.permissionService.closeSession();
    this.authService.login();
  }

  private isBase(url: string): boolean {
    let _isBase: boolean = false;
    this.baseAddToken.forEach(baseBack => {
      if (url.includes(baseBack)) _isBase = true;
    });
    return _isBase;
  }

  private isResource(url: string): boolean {
    let _isResource: boolean = false;
    this.noAddToken.forEach(item => {
      if (url.includes(item)) _isResource = true;
    });
    return _isResource;
  }

  private checkCanToken(url: string) {
    return this.isBase(url) && !this.isResource(url)
  }
}

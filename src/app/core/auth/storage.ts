export const SESSION_CONSTS = {
  currentUser: "SORIcurrentUser",
  permissions: "SORIpermissions",
  sessionData: "SORIsessionData",
  currentPath: "SORIcurrentPath"
};

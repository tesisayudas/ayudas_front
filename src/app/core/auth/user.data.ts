import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";

export class UserData {
  private _username: string;
  private _rol: string;

  constructor(data) {
    this._username = data.preferred_username;
    this._rol = this.getRol(data.roles)
  }

  private getRol(roles: string[]): string {
    const _roles = AVAILABLE_ROLES;
    for(const _role in _roles ){
      if (roles.indexOf(_roles[_role]) != -1){
        this._rol = _roles[_role];
        return this._rol;
      }
    }
    return ''
  }

  get username(): string {
    return this._username
  }

  get rol(): string {
    return this._rol;
  }
}

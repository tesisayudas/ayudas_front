import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {PermissionService} from "@g3/core/auth/permission.service";
import {tap} from "rxjs/operators";

/**
 * @whatItDoes
 * Check roles provided in data roles and authorize.
 *
 * @implements CanActivate
 *
 * @howToUse
 * This class must be provided as a canActive attribute and specify role list
 * which can access at specified path.
 * Roles are parametrized and there are some available. Those can be found
 * in AVAILABLE_ROLES constant.
 *
 * @example
 * {
 *   path: 'internationalization',
 *   canActivate: [RoleGuard],
 *   data: {
 *     roles: [
 *       AVAILABLE_ROLES.student,
 *       AVAILABLE_ROLES.administrative
 *      ]
 *   },
 * }
 *
 * @stable
 */
@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private permissionService: PermissionService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const roles: string[] = Array.isArray(route.data.roles) ? route.data.roles : [];
    return this.permissionService.hasRolAccess(...roles)
      .pipe(tap((canAccess: boolean) => {
        if (!canAccess) this.router.navigateByUrl('/not-found')
      }));
  }

}

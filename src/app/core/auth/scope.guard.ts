import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {PermissionService} from "@g3/core/auth/permission.service";
import {tap} from "rxjs/operators";

/**
 * @whatItDoes
 * Check resource and scope provided in data resoruce and scope to authorize.
 *
 * @implements CanActivate
 *
 * @howToUse
 * This class must be provided as a canActive attribute and specify resource and
 * scope which allow access at specified path.
 * Available resources could be found in  ACCESS_RESOURCES from "@g3/core/auth/permissions";
 * Available scopes could be found in  AVAILABLE_SCOPES from "@g3/core/auth/permissions";
 *
 * @example
 * {
 *   path: 'add',
 *   component: InternationalizationsFormComponent,
 *   canActivate: [ScopeGuard],
 *   data: {
 *     resource: ACCESS_RESOURCES.Internationalizations,
 *     scope: AVAILABLE_SCOPES.create
 *   }
 * }
 *
 * @stable
 */
@Injectable()
export class ScopeGuard implements CanActivate {

  constructor(private permissionService: PermissionService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.permissionService.hasResourceWithScope(route.data.resource, route.data.scope)
      .pipe(tap((canAccess: boolean) => {
        if (!canAccess) this.router.navigateByUrl('/not-found')
      }));
  }

}

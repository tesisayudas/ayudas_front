import {Injectable} from "@angular/core";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {SESSION_CONSTS} from "@g3/core/auth/storage";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "@g3/core/auth/auth.service";
import {UserData} from "@g3/core/auth/user.data";
import {of} from "rxjs/observable/of";

@Injectable()
export class PermissionService {

  public permissions$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public roles$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  constructor(private httpClient: HttpClient,
              private router: Router,
              private authService: AuthService) { }

  /**
   * Get token according to the specified code.
   * Check if a current path have been stored in session storage and redirect when authentication is refreshed.
   * @param {string} code - this  is assigned when user login in keyclock.
   * @return {Observable<any>}
   */
  public authenticate(code: string): Observable<any> {
    const url: string = `${environment.ayudas.backBaseUrl}/authenticate?code=${code}`;
    return this.httpClient.get(url)
      .pipe(tap((res: answerAuthenticate) => this.token = res.token),
        switchMap((res: answerAuthenticate) => this.loadCurrentUser()),
        tap(res => {
          const currentRoute = sessionStorage.getItem(SESSION_CONSTS.currentPath);
          sessionStorage.removeItem(SESSION_CONSTS.currentPath);
          if (currentRoute) {
            this.router.navigateByUrl(currentRoute)
          }else {
            this.router.navigateByUrl('/')
          }
        })
      )
  }

  /**
   * Verify if current user has at least a required role.
   * @param {string} roles - list of roles which are required.
   * @return {Observable<boolean>}
   */
  public hasRolAccess(...roles: string[]): Observable<boolean> {
    if(roles.length >= 1){
      return this.roles$.pipe(map((currentUserRoles: string[])=> {
      return roles.filter((role: any) => currentUserRoles.indexOf(role) > -1).length > 0;
      }))
    } else {
      return of(true);
    }
  }

  /**
   * Make a request to get current user data, load permissions and roles from the request.
   * @return {Observable<any>}
   */
  public loadCurrentUser(): Observable<any> {
    return this.httpClient.get(`${environment.ayudas.backBaseUrl}/current_user`)
      .pipe(
        tap((res:any) => this.permissions = res.permissions),
        tap((res: any) => this.authService.userData = new UserData(res) ),
        tap((res:any) => this.roles = res.roles),
        tap(console.log),
        catchError(err => {
          this.router.navigateByUrl('/');
          this.authService.login();
          return of(null)
        })
      );
  }

  /**
   * Check specified resource in route and validate if this one has the specified scope
   * @param {ActivatedRoute} activatedRoute - Must be defined a "resource" atribute typeof "string" in data object in the route.
   * @param {string} scope - must be checked in AVAILABLE_SCOPES to handle according with the resource specified resource in active route.
   * @return {Observable<boolean>}
   */
  public hasPermission(activatedRoute: ActivatedRoute, scope: string): Observable<boolean> {
    return activatedRoute.data.pipe(switchMap((_data: any) => this.hasResourceWithScope(_data.resource, scope)));
  }

  /**
   * Validate resource and scope existence in the loaded permissions.
   * @param {string} resourceName
   * @param {string} scope
   * @return {Observable<boolean>}
   */
  public hasResourceWithScope(resourceName: string, scope: string | string[] | any): Observable<boolean> {
    return this.permissions$.pipe(map((_permissions:any[]) => {
      if (_permissions && typeof Array.isArray(_permissions)) {
        const resource =  _permissions.find(item => item.resource_set_name === resourceName);
        if (resource && Array.isArray(resource.scopes)) {
          const scopes: string[] = resource.scopes;
          return scopes.indexOf(scope) > -1;
        }
        return false
      }
      return false;
    }))
  }

  /**
   * Remove token, permissions and roles stored in platform for current user..
   */
  public closeSession(): void {
    this.roles$.next([]);
    this.permissions$.next(null);
    this.authService.userData = null;
    sessionStorage.removeItem(SESSION_CONSTS.currentUser);

  }

  set token(token: string) {
    sessionStorage.setItem(SESSION_CONSTS.currentUser, token);
  }

  get token(): string {
    return sessionStorage.getItem(SESSION_CONSTS.currentUser);
  }

  set permissions(permissions: any) {
    this.permissions$.next(permissions);
  }

  set roles(roles: any) {
    this.roles$.next(roles);
  }

}

export type answerAuthenticate = { token: string }


import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";
import {UserData} from "@g3/core/auth/user.data";

@Injectable()
export class AuthService {

  public userData: UserData;

  constructor(private httpClient: HttpClient) { }

  /**
   * Get authentication url and navigate to this one.
   */
  public login(): void {
    const url: string = `${environment.ayudas.backBaseUrl}/login`;
    this.httpClient.get(url)
      .subscribe((res: answerLogin) => window.location.href = res.url);
  }

  /**
   * Make a request to close current connection
   * @return {Observable<any>}
   */
  public logout(): Observable<any> {
    const url: string = `${environment.ayudas.backBaseUrl}/logout`;
    return this.httpClient.delete(url);
  }

}

export type answerLogin = { url: string }


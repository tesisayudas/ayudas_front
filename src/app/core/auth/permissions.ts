export const AVAILABLE_SCOPES = {
  show: "show",
  paginate: "paginate",
  update: "update",
  create: "create",
  changeStatus: "changeStatus",
  list: "list",
  delete: "delete",
  reports: "reports"
};

export const ACCESS_RESOURCES = {
  requesterType: "RequesterType",
  resource: "Resource",
  resourceStatus: "ResourceStatus",
  resourceType: "ResourceType",
  unity: "Unity",
  reservation: "Reservation",
  reservationType: "ReservationType",
  reports: "Reports"
};

export const AVAILABLE_ROLES = {
  admin: "admin",
  coordinator: "coordinador",
  monitor: "monitor",
  auxiliar: "auxiliar",
  SENA: "SENA",
  teacher: "profesor",
  functionary: "funcionario",
  graduated: "graduated",
  student: "estudiante"
};

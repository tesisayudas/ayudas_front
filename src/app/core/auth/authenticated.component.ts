import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "@g3/core/auth/auth.service";
import {PermissionService} from "@g3/core/auth/permission.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  template: ``
})
export class AuthenticatedComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private permissionService: PermissionService) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      (res: authenticatedParams) => {
        this.permissionService.authenticate(res.code)
          .subscribe(
            () => { },
            err => {
              if (err instanceof HttpErrorResponse && err.status === 500) {
                this.authService.login();
              }
            }
          );
      })
  }

}

export type authenticatedParams = { code: string }

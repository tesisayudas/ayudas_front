import {Observable} from "rxjs/Observable";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {tap} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {of} from "rxjs/observable/of";

@Injectable()
export class HttpCacheService {

  private requests: Map<string, HttpResponse<any>> = new Map<string, HttpResponse<any>>();

  /**
   *
   * @param {string} url
   * @param {HttpResponse<any>} res
   * @public
   */
  public put(url: string, res: HttpResponse<any>): void {
    this.requests.set(url, res);
  }

  public get(url: string): HttpResponse<any> | undefined {
    return this.requests.get(url);
  }

  public cleanUrl(url: string): void {
    this.requests.delete(url);
  }

  public cleanCache(): void {
    this.requests.clear();
  }

}

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

  constructor(private httpCacheService: HttpCacheService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const cachedResponse = this.httpCacheService.get(req.url);
    if (cachedResponse)
      return of(cachedResponse).pipe(tap(res => this.httpCacheService.cleanUrl(req.url)));
    return next.handle(req);
  }

}

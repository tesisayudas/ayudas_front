import {Injectable} from '@angular/core';
import * as moment from 'moment';

export const BASIC_DATE_FORMAT = 'DD/MM/YYYY';

@Injectable()
export class DateService {

  constructor() {
  }

  static getCurrentUTCDateWithTime(date) {
    const stillUtc = moment.utc(date).toDate();
    return moment(stillUtc).local().format(BASIC_DATE_FORMAT + " hh:mm a");
  }

  static getDateWithFormHyphenSeparateValue(date){
    return moment(date, BASIC_DATE_FORMAT).local().format('YYYY-MM-DD');
  }

  static getCurrentUTCDate(date) {
    const stillUtc = moment.utc(date).toDate();
    return moment(stillUtc).local().format(BASIC_DATE_FORMAT);
  }

}

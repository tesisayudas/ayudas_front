import * as moment from 'moment';

export const BASIC_DATE_FORMAT = 'DD/MM/YYYY';
export const DATE_TIME_FORMAT = 'DD/MM/YYYY hh:mm a';
export const DATE_TIME_24_FORMAT = 'DD/MM/YYYY HH:mm';

export class DateUtils {

  static getCurrentUTCDateWithTime(date) {
    return DateUtils.convertUTCDate(date, DATE_TIME_FORMAT);
  }

  static getCurrentUTCDate(date) {
    return DateUtils.convertUTCDate(date, BASIC_DATE_FORMAT);
  }

  private static convertUTCDate(date, format) {
    const stillUtc = moment.utc(date, format).toDate();
    return moment(stillUtc, format).local().format(format);
  }

}

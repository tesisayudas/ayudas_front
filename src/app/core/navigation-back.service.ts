import {Injectable} from '@angular/core';
import {Router} from "@angular/router";

@Injectable()
export class NavigationBackService {

  urlBack;

  constructor(private router: Router) {
  }

  goToBack() {
    if (this.urlBack != null) {
      if (this.urlBack == '')
        this.router.navigate(['/']);
      else
        this.router.navigate([this.urlBack]);
    } else
      this.router.navigate(['/']);
    this.urlBack = null;
  }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {IntroComponent} from './intro/intro.component';
import {NotFoundComponent} from "./not-found/not-found.component";


export const routes: Routes = [
  {path: 'intro', component: IntroComponent, data: {titleKey: 'home'}},
  {path: 'not-found', component: NotFoundComponent, data: {titleKey: 'navigation.not_found'}},
  {path: '**', component: NotFoundComponent, data: {titleKey: 'navigation.not_found'}}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}

import {Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {SnackbarService} from '../snackbar.service';

/**
 * The toast component.
 */
@Component({
  selector: 'g3-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ToastComponent implements AfterViewInit {

  /**
   * The element that contain the nativeElement of the toast.
   */
  @ViewChild("toast") toast: ElementRef;

  /**
   * Create a new component toast.
   * @param {SnackbarService} snackbar
   */
  constructor(private snackbar: SnackbarService) {
  }

  /**
   * Set to the [toast]{@link SnackbarService#toast} the nativeElement of the toast.
   */
  ngAfterViewInit() {
    this.snackbar.toast = this.toast.nativeElement;
  }

}

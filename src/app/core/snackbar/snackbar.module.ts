import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from './toast/toast.component';
import { SnackbarService } from './snackbar.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ToastComponent
  ],
  exports: [
    ToastComponent
  ], 
  providers: [
    SnackbarService
  ]
})
export class SnackbarModule { }

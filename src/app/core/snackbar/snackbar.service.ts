import {Injectable} from '@angular/core';
import {SnackbarPartner} from "../../shared/toast/snackbar-partner";

/**
 * Service to show the toast component with a determined content.
 */
@Injectable()
export class SnackbarService {

  /**
   * The nativeElement of the toast.
   */
  toast: any;

  /**
   * The partners that response when the snackbar is showed.
   * @item {Array}
   */
  partners: SnackbarPartner[] = [];

  /**
   * Review if is a string message or a Object.
   *
   * If is a Object assumes the this has a message property.
   *
   * If is a string then create a Object with the message property with the string message.
   * @param data
   * @returns {any}
   */
  static structureData(data: any): any {
    if (!(data instanceof Object))
      data = {message: data};
    return data;
  }

  /**
   * Add a new partner.
   * @param {SnackbarPartner} partner - The new partner.
   */
  addPartner(partner: SnackbarPartner): void {
    this.partners.push(partner);
  }

  /**
   * Remove a partner.
   * @param {SnackbarPartner} partner
   */
  removePartner(partner: SnackbarPartner): void {
    this.partners.splice(this.partners.indexOf(partner), 1);
  }

  /**
   * Show the toast whit the data assigned.
   * @param data - A string message or a Object with property message with the message.
   */
  showToast(data: any): void {
    data = SnackbarService.structureData(data);
    data.timeout = !data.timeout ? 2750 : data.timeout;
    this.toast && this.toast.MaterialSnackbar.showSnackbar(data);
    this.partners.forEach(partner => partner.update(this.toast, data));
  }

}

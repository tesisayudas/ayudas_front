import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'g3-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  internalCode: number;
  message: string;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if(params.errorCode){
        this.internalCode = params.errorCode;
        this.message = `error${this.internalCode}`;
      } else {
        this.internalCode = 404;
        this.message = 'not_found.description';
      }
    })
  }

  ngOnInit() {
  }

}

import {Injectable} from '@angular/core';
import {Nav} from './clases/nav';
import {Subject} from "rxjs/Subject";
import {NavBuilder} from "./clases/nav-builder";
import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";

/**
 * Manager of the navs of the application.
 */
@Injectable()
export class NavsService {

  /**
   * Subject of the current nav.
   * @item {Subject<Nav>}
   */
  private currentNavSource = new Subject<Nav>();

  /**
   * Observable of the current nav.
   * @item {Observable<Nav>}
   */
  currentNav$ = this.currentNavSource.asObservable();

  /**
   * Example construction of navs, you can do it too so:
   *
   * with this method the ids are automatic generated, but if you want you can set id manually with .id("theId").
   *
   * @example
   * And you don't need to put in the subnavs, the route of the composite parent separated with /. You don't need this .route("other/option3").
   *
   *---private _navs: Nav[] = new NavBuilder()
   *
   *------.simple().icon("home").label("home").route("intro").id("simple-nav-id").roles("admin")
   *
   *------.composite().icon("inbox").label("other").route("other").subNavs(new NavBuilder()
   *
   *---------.simple().icon("fiber_manual_record").label("option3").route("option3").roles("admin").build()
   *
   *------)
   *
   *------.simple().icon("inbox").label("option").route("option4").roles("admin").build();
   *
   * @item {[NavSimple , NavComposite , NavSimple]}
   * @private
   */
  private _navs: Nav[] = new NavBuilder()
    .simple().icon('home').label('home').route('intro')
    .composite().icon('settings').label('navigation.parametrization').route('parametrization').subNavs(new NavBuilder()
      .simple().label('navigation.requester_types').roles([AVAILABLE_ROLES.coordinator]).route('requester_types')
      .simple().label('navigation.resource_types').roles([AVAILABLE_ROLES.coordinator]).route('resource_types')
      .simple().label('navigation.unities').roles([AVAILABLE_ROLES.coordinator]).route('unities')
      .simple().label('navigation.resource_statuses').roles([AVAILABLE_ROLES.coordinator]).route('resource_statuses')
      .build()
    )
    .simple().label('navigation.monitors').route('monitors').roles([AVAILABLE_ROLES.coordinator])
    .composite().icon('package').label('navigation.inventory').route('inventory').subNavs(new NavBuilder()
      .simple().label('navigation.resources').roles([AVAILABLE_ROLES.coordinator]).route('normal_resources')
      .simple().label('navigation.extras').roles([AVAILABLE_ROLES.coordinator]).route('extra_resources')
      .build()
    )
    .composite().icon('settings').label('navigation.reservation').route('reservations').subNavs(new NavBuilder()
      .simple().label('navigation.request_reservation').route('request').roles([AVAILABLE_ROLES.student, AVAILABLE_ROLES.monitor, AVAILABLE_ROLES.auxiliar, AVAILABLE_ROLES.coordinator, AVAILABLE_ROLES.functionary, AVAILABLE_ROLES.SENA, AVAILABLE_ROLES.teacher])
      .simple().label('navigation.reservation').route('reservation').roles([AVAILABLE_ROLES.student, AVAILABLE_ROLES.monitor, AVAILABLE_ROLES.auxiliar, AVAILABLE_ROLES.coordinator, AVAILABLE_ROLES.functionary, AVAILABLE_ROLES.SENA, AVAILABLE_ROLES.teacher])
      .simple().label('navigation.reservation_types').roles([AVAILABLE_ROLES.coordinator]).route('reservation_types')
      .build()
    )
    .simple().icon('file-chart').label('navigation.reports').route('reports').roles([AVAILABLE_ROLES.auxiliar, AVAILABLE_ROLES.coordinator, AVAILABLE_ROLES.admin, AVAILABLE_ROLES.SENA])
    .build();

  /**
   * Give the navs.
   * @returns {Promise<Nav[]>} - The navs.
   */
  public get navs(): Promise<Nav[]> {
    return new Promise(resolve => setTimeout(() => resolve(this._navs), 0));
  }

  /**
   * Set the current nav.
   * @param {Nav} activedNav - The current nav.
   */
  public setCurrentNav(activedNav: Nav) {
    this.currentNavSource.next(activedNav);
  }

}

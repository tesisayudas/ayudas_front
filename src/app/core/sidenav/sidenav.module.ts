import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {SidenavComponent, SideNavGroupComponent, SideNavLinkComponent} from './sidenav.component';

import {NavsComponent} from './components/navs/navs.component';
import {NavSimpleComponent} from './components/nav-simple/nav-simple.component';
import {NavCompositeComponent} from './components/nav-composite/nav-composite.component';
import {SelectorComponent} from '../selector/selector.component';
import {SharedModule} from '../../shared/shared.module';
import {NavsService} from "./navs.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    SidenavComponent,
    NavsComponent,
    NavSimpleComponent,
    NavCompositeComponent,
    SelectorComponent,
    SideNavGroupComponent,
    SideNavLinkComponent
  ],
  exports: [
    SidenavComponent
  ],
  providers: [NavsService]
})
export class SidenavModule {
}

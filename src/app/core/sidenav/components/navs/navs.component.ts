import {Component, Input, ViewEncapsulation} from '@angular/core';
import {Nav} from '../../clases/nav';
import {NavComposite} from '../../clases/nav-composite';
import {NavSimple} from '../../clases/nav-simple';

/**
 * Represent the array of navs existing.
 */
@Component({
  selector: 'g3-navs',
  templateUrl: './navs.component.html',
  styleUrls: ['./navs.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NavsComponent {

  /**
   * The array of navs model.
   * @item {Array}
   */
  @Input() public navs: Nav[] = [];

  /**
   * It says if a nav is composite.
   * @param nav
   * @returns {boolean} - The value is true if is a composite nav, else false.
   */
  isNavComposite(nav): boolean {
    return nav instanceof NavComposite
  }

  /**
   * It says if a nav is simple.
   * @param nav
   * @returns {boolean} - The value is true if is a simple nav, else false.
   */
  isNavSimple(nav): boolean {
    return nav instanceof NavSimple
  }

}

import {AfterViewInit, Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NavSimple} from '../../clases/nav-simple';
import {NavigationEnd, Router} from "@angular/router";
import {NavsService} from "../../navs.service";
import {I18nService} from "../../../i18n/i18n.service";

/**
 * Represent graphic of the {@link NavSimple}.
 */
@Component({
  selector: 'g3-nav-simple',
  templateUrl: './nav-simple.component.html',
  styleUrls: ['./nav-simple.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NavSimpleComponent implements OnInit, AfterViewInit {

  /**
   * The nav simple model.
   */
  @Input('navSimple') navSimple: NavSimple;

  /**
   * The level of depth.
   * @item {number}
   */
  @Input('level') level: number = 0;

  finalLabel;

  /**
   * Create a new nav simple component, it is active or inactive depend of the actual url.
   * @param {Router} router
   * @param {NavsService} navs - The navs service to set the current nav.
   * @param i18n
   */
  constructor(private router: Router, private navs: NavsService, private i18n: I18nService) {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if (e.url == "/" + this.navSimple.route) {
          this.active();
        } else {
          this.desactive();
        }
      }
    });
  }

  ngOnInit() {
    this.i18n.translator$.subscribe(translator => this.finalLabel = translator.getValue(this.navSimple.label));
    this.i18n.update();
  }

  /**
   * Set this nav simple as active if the url is equals to the route of this nav simple.
   */
  ngAfterViewInit() {
    if (this.router.url == "/" + this.navSimple.route) {
      this.active();
    }
  }

  /**
   * Put as active this simple nav.
   */
  active() {
    this.getElement().classList.add("active");
    this.navs.setCurrentNav(this.navSimple);
  }

  /**
   * Put as inactive this simple nav.
   */
  desactive() {
    this.getElement().classList.remove("active");
  }

  /**
   * Give the nativeElement of this simple nav.
   * @returns {any}
   */
  getElement(): any {
    return document.querySelector("#" + this.navSimple.id);
  }

}

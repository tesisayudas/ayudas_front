import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NavComposite} from '../../clases/nav-composite';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NavsComponent} from '../navs/navs.component';
import {NavigationEnd, Router} from "@angular/router";
import {I18nService} from "../../../i18n/i18n.service";

/**
 * Represent graphic of the {@link NavComposite}.
 */
@Component({
  selector: 'g3-nav-composite',
  templateUrl: './nav-composite.component.html',
  styleUrls: ['./nav-composite.component.css'],
  animations: [trigger('sidenavAnimate', [
    state('close', style({
      height: '0px',
      display: 'none'
    })),
    state('open', style({
      display: 'block',
      height: '*'
    })),
    transition('* => *', animate('150ms ease'))
  ])],
  encapsulation: ViewEncapsulation.None
})
export class NavCompositeComponent extends NavsComponent implements OnInit {

  /**
   * The nav composite model.
   */
  @Input() navComposite: NavComposite;

  /**
   * The level of depth;
   * @item {number}
   */
  @Input('level') level: number = 0;

  /**
   * The state of the accordion.
   * @item {string}
   */
  public accordionStatus: string = 'close';

  finalLabel;

  /**
   * Create a new nav composite component.
   * @param {Router} router
   * @param i18n
   */
  constructor(private router: Router, private i18n: I18nService) {
    super();
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        const urlSegments = e.url.split("/");
        if (urlSegments.find(seg => seg == this.navComposite.route)) {
          this.open();
        } else {
          this.close();
        }
      }
    });
  }

  ngOnInit() {
    this.i18n.translator$.subscribe(translator => this.finalLabel = translator.getValue(this.navComposite.label));
    this.i18n.update();
  }

  /**
   * Open or close the accordion (show or hide children navs).
   */
  doingAccordion() {
    if (this.accordionStatus == "open") {
      this.close();
      return;
    }
    this.open();
  }

  /**
   * Open the accordion (show the children navs).
   */
  open() {
    this.getAccordion().classList.add('mdl-accordion--opened');
    this.accordionStatus = "open";
  }

  /**
   * Close the accordion (hide the children navs).
   */
  close() {
    this.getAccordion().classList.remove('mdl-accordion--opened');
    this.accordionStatus = "close";
  }

  /**
   * Give the accordion nativeElement.
   * @returns {any} - The accordion nativeElement.
   */
  getAccordion(): any {
    return document.querySelector('#accordion-' + this.navComposite.id);
  }

}

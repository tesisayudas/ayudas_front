/**
 * Represent abstract nav.
 */
export abstract class Nav {

  /**
   * The id of nav.
   * @item {string}
   */
  public id: string = "default-id";

  /**
   * The icon of nav.
   * @item {string}
   */
  public icon: string = 'default';

  /**
   * The label of nav.
   * @item {string}
   */
  public label: string = "default-label";

  /**
   * The route to navigate.
   * @item {string}
   */
  public route: string = "default-route";
  public roles: string[] = []

  /**
   * Create a new abstract nav.
   * @param {string} id
   * @param {string} icon
   * @param {string} label
   * @param {string} route
   */
  constructor(id: string, icon: string, label: string, route: string) {
    this.id = id;
    this.icon = icon;
    this.label = label;
    this.route = route;
  }

  /**
   * Set the icon.
   * @param {string} icon
   * @returns {Nav} - This same nav.
   */
  public setIcon(icon: string): Nav {
    this.icon = icon;
    return this;
  }

  /**
   * Set the label.
   * @param {string} label
   * @returns {Nav} - This same nav.
   */
  public setLabel(label: string): Nav {
    this.label = label;
    return this;
  }

  /**
   * Set the id.
   * @param {string} id
   * @returns {Nav} - This same nav.
   */
  public setId(id: string): Nav {
    this.id = id;
    return this;
  }

  /**
   * Set the route.
   * @param {string} route
   * @returns {Nav} - This same nav.
   */
  public setRoute(route: string): Nav {
    this.route = route;
    return this;
  }

  public setRoles(roles: string[]){
    this.roles = roles;
  }

}

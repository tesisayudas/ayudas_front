import { Nav } from "./nav";

/**
 * Represent a sheet nav.
 */
export class NavSimple extends Nav {

  /**
   * Create a new simple nav.
   * @param {string} id
   * @param {string} icon
   * @param {string} label
   * @param {string} route
   */
  constructor(id: string, icon: string, label: string, route: string ){
    super(id, icon, label, route);
  }
}

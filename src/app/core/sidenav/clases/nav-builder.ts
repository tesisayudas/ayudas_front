import {NavSimple} from "./nav-simple";
import {Nav} from "./nav";
import {NavComposite} from "./nav-composite";

/**
 * Builder to create navs.
 */
export class NavBuilder {

  /**
   * The patter to use for the id of the navs.
   * @item {string}
   */
  private static readonly idNavPattern = "g3-nav-id-";

  /**
   * The array of navs created.
   * @item {Array}
   */
  private navs: Nav[] = [];

  /**
   * Create a new {@link NavSimple}.
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  simple(): NavBuilder {
    this.addNav(new NavSimple("default-id", "default-icon", "default-label", "default-route"));
    return this;
  }

  roles(roles: string[]){
    this.getCurrentNav().setRoles(roles);
    return this;
  }

  /**
   * Create a new {@link NavComposite}.
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  composite(): NavBuilder {
    this.addNav(new NavComposite("default-id", "default-icon", "default-label", "default-route", []));
    return this;
  }

  /**
   * Set the icon of the current nav.
   * @param {string} icon
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  icon(icon: string): NavBuilder {
    this.getCurrentNav().setIcon(icon);
    return this;
  }

  /**
   * Set the label of the current nav.
   * @param {string} label
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  label(label: string): NavBuilder {
    this.getCurrentNav().setLabel(label);
    return this;
  }

  /**
   * Set the id of current nav.
   * @param {string} id
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  id(id: string): NavBuilder {
    this.getCurrentNav().setId(id);
    return this;
  }

  /**
   * Set the id with the pattern determined in [idNavPattern]{@link NavBuilder#idNavPattern} of current nav.
   */
  private assignId() {
    this.getCurrentNav().setId(NavBuilder.idNavPattern + this.navs.length);
  }

  /**
   * Set the subnavs of the current composite nav.
   * @param {Nav[]} navs
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  subNavs(navs: Nav[]): NavBuilder {
    navs && navs.forEach(n => this.addParentRoute(n));
    (this.getCurrentNav() as NavComposite).addSubNavs(navs);
    return this;
  }

  addParentRoute(nav) {
    nav.route = this.getCurrentNav().route + "/" + nav.route;
    if (nav.subnavs) nav.subnavs.forEach(n => this.addParentRoute(n))
  }

  /**
   * Set the route of current nav.
   * @param {string} route
   * @returns {NavBuilder} - This same {@link NavBuilder}.
   */
  route(route: string): NavBuilder {
    this.getCurrentNav().setRoute(route);
    return this;
  }

  /**
   * Build the navs.
   * @returns {Nav[]} - The final array of navs.
   */
  build(): Nav[] {
    return this.navs.reverse();
  }

  /**
   * Give the last nav added.
   * @returns {Nav}
   */
  private getCurrentNav(): Nav {
    return this.navs[0];
  }

  /**
   * Add new nav.
   * @param {Nav} newNav
   * @returns {Nav} - The nav added.
   */
  private addNav(newNav: Nav): Nav {
    this.navs.unshift(newNav);
    this.assignId();
    return newNav;
  }

}

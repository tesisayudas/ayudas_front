import {Nav} from "./nav";

/**
 * Represent a branch nav.
 */
export class NavComposite extends Nav {
  /**
   * The children navs.
   * @item {Array}
   */
  public subnavs: Nav[] = [];

  /**
   * Create a new composite nav.
   * @param {string} id
   * @param {string} icon
   * @param {string} label
   * @param {string} route
   * @param {Nav[]} subnavs
   */
  constructor(id: string, icon: string, label: string, route: string, subnavs: Nav[]) {
    super(id, icon, label, route);
    this.subnavs = subnavs;
  }

  /**
   * Set the children navs.
   * @param {Nav[]} navs - The children navs.
   * @returns {NavComposite} - This same {@link NavComposite}.
   */
  addSubNavs(navs: Nav[]): NavComposite {
    this.subnavs = navs;
    return this;
  }

}


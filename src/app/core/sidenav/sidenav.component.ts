import {AfterViewInit, Component, Input, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Nav} from './clases/nav';
import {NavsService} from "./navs.service";
import {NavigationEnd, Router} from "@angular/router";
import {tap} from "rxjs/operators";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {PermissionService} from "@g3/core/auth/permission.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {NavComposite} from "@g3/core/sidenav/clases/nav-composite";
import {NavSimple} from "@g3/core/sidenav/clases/nav-simple";
import {AuthService} from "@g3/core/auth/auth.service";

/**
 * The sidenav component.
 */
@Component({
  selector: 'g3-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidenavComponent {
  /**
   * The navs to show.
   */
  navs: Promise<Nav[]>;

  /**
   * The name of the application.
   */
  appname: String;

  /**
   * Create a new sidenav.
   * @param {NavsService} navsService - The gives the navs.
   */
  constructor(private navsService: NavsService,
              public authService: AuthService,
              public permissionService: PermissionService
  ) {
    this.navs = navsService.navs;
    this.appname = "Application name";
  }
}



@Component({
  selector: 'g3-custom-nav-group',
  template: `
    <!--<ng-container *ngIf="canShow() | async">-->
      <g3-custom-nav-link [group]="group"></g3-custom-nav-link>
      <ul class="mdl-list" [ngClass]="group.isCollapse ? 'activated': 'hidden'">
        <ng-container *ngFor="let nav of group.subnavs">
          <g3-custom-nav-group [group]="nav"></g3-custom-nav-group>
        </ng-container>
      </ul>      
    <!--</ng-container>-->

  `,
})
export class SideNavGroupComponent {
  @Input() public group: any;

  canShow(): Observable<boolean> {
    return new BehaviorSubject(true).pipe()
  }
}

@Component({
  selector: 'g3-custom-nav-link',
  template: `
    <ng-container *ngIf="canShow$ | async">
      <li (click)="onToggle()"
          *ngIf="group.subnavs; else noChildren"
          [ngClass]="{'active--parent': group.isCollapse, 'active': group.isOpen}"
          class="mdl-list__item">
        <a class="arrow">
          <i *ngIf="!group.isCollapse; else noCollapse" class="material-icons">chevron_right</i>
          <ng-template #noCollapse>
            <i class="material-icons">expand_more</i>
          </ng-template>
        </a>
        {{group.label | i18n}}
        <!--<i class="sidebar-item__icon material-icons ">{{group.icon}}</i>-->
        <i class="sidebar-item__icon material-icons mdi mdi-{{group.icon}}"></i>
      </li>
      <ng-template #noChildren>
        <li [routerLink]="group.route"
            routerLinkActive="active"
            class="mdl-list__item link">
          {{group.label | i18n}}
        </li>
      </ng-template>  
    </ng-container>
    
  `
})
export class SideNavLinkComponent implements AfterViewInit, OnDestroy {
  @Input() public group: any;
  subUrlEvents: Subscription;
  public canShow$: Observable<boolean>;

  constructor(public router: Router, private permissionService: PermissionService) {}

  onToggle() {
    this.group.isCollapse = !this.group.isCollapse;
  }

  ngAfterViewInit() {
    setTimeout(()=> this.canShow$ = this.canShow());
    this.subUrlEvents = this.router.events.pipe(tap(res => {

      if (res instanceof NavigationEnd) {
        if (res.urlAfterRedirects == this.group.route)
          return this.group.isOpen = false;

        const currentUrl: string[] = res.urlAfterRedirects
          .split('/')
          .filter(res => res);
        const currentNav: string[] = this.group.route
          .split('/')
          .filter(res => res);
        let flag: boolean = true;
        for (let i = 0; i < currentNav.length; i++) {
          if (currentNav[i] != currentUrl[i]) flag = false

        }
        this.group.isOpen = flag;
        this.group.isCollapse = flag;
      }
    })).subscribe()
  }

  public canShow(): Observable<boolean>{
    if (this.group instanceof NavSimple && this.group.roles.length === 0) return new BehaviorSubject(true).pipe();
    if (this.group instanceof NavComposite){
      let roles: string[] = [];
      this.group.subnavs.forEach(_subNav => {
        const _roles = _subNav.roles;
        _roles.forEach(_role => {
          if (roles.indexOf(_role) === -1) roles.push(_role);
        });
      });
     return this.permissionService.hasRolAccess(...roles);
    }
    return this.permissionService.hasRolAccess(...this.group.roles);
  }

  ngOnDestroy() {
    if (this.subUrlEvents) this.subUrlEvents.unsubscribe()
  }

}


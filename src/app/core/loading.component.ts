import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";

@Component({
  selector: 'g3-loading',
  template: `
    <div class="loading-container">
      <div g3UpgradeElements id="p2" *ngIf="loading$ | async"
           class="mdl-progress g3-full-width mdl-js-progress mdl-progress__indeterminate"></div>
    </div>
  `
})
export class LoadingComponent implements OnInit {

  public loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.events
      .pipe(
        tap(res => this.loading$.next(true)),
        debounceTime(500),
        distinctUntilChanged(),
        tap(res => {
          if (res instanceof NavigationEnd) this.onNavigationEnd();
          else this.loading$.next(true)
        })
      ).subscribe()
  }

  /**
   * Handle behavior on navigation end.
   */
  private onNavigationEnd(): void {
    this.loading$.next(false);
    HandlerSidebar.handleSidebarBehavior();
  }

  // /**
  //  * Disappears sidebar when window width is too small.
  //  */
  // private handleSidebarBehavior(): void {
  //   const sidebar = document.getElementById('g3-sidebar');
  //   if (window.innerWidth <= 1024 && sidebar.classList.contains('is-visible')) {
  //     sidebar.classList.remove('is-visible');
  //     sidebar.setAttribute("aria-hidden", "true");
  //     document.getElementsByClassName('mdl-layout__obfuscator')[0].classList.remove('is-visible');
  //   }
  // }

}


export class HandlerSidebar {
  /**
   * Disappears sidebar when window width is too small.
   */
  static handleSidebarBehavior(): void {
    const sidebar = document.getElementById('g3-sidebar');
    if (window.innerWidth <= 1024 && sidebar.classList.contains('is-visible')) {
      sidebar.classList.remove('is-visible');
      sidebar.setAttribute("aria-hidden", "true");
      document.getElementsByClassName('mdl-layout__obfuscator')[0].classList.remove('is-visible');
    }
  }
}

import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {CookieService} from './cookie.service';
import {Translator} from "./translator";
import {HttpClient} from "@angular/common/http";

/**
 * Service for the internationalization
 */
@Injectable()
export class I18nService {

  /**
   * The url of json that contain the languages to manage in the application.
   * @item {string}
   */
  private static readonly urlJsonLanguages = "assets/locale/languages.json";

  /**
   * The cookie name for the language of the application.
   * @item {string}
   */
  private static readonly cookieLanguageName = "LANGUAGE";

  /**
   * The path fo the cookie of language.
   * @item {string}
   */
  private static readonly pathCookieLanguage = "/";

  /**
   * The abbreviation of the default language.
   * @item {string}
   */
  private static readonly defaultLanguage = "es";

  /**
   * The of the variable that represent the language in this class (it's important set correctly).
   * @item {string}
   */
  private static readonly variableNameLanguage = "language";

  /**
   * The abbreviation of current language.
   * @example
   * Spanish abbreviation = "es" and english abbreviation = "en".
   */
  private abbreviationLanguage: string;

  /**
   * Represent the language currently used in the application.
   *
   * The name of this variable should be the value of [variableNameLanguage]{@link I18nService#variableNameLanguage}.
   */
  private language: any;

  /**
   * The subject translator.
   * @item {Subject<Translator>}
   */
  private translatorSource = new Subject<Translator>();

  /**
   * The Observable translator.
   * @item {Observable<Translator>}
   */
  translator$ = this.translatorSource.asObservable();

  /**
   * The supported languages in the application.
   */
  private _supportedLanguages: any[];

  /**
   * The request in form of promise of the supported languages in the application.
   */
  private requestSupportedLanguages: Promise<any>;

  /**
   * The metadata of current language of the application.
   */
  private _metadataLanguage: any;

  /**
   * Subject of metadata current language.
   * @item {Subject<Object>}
   */
  private metadataLanguageSource = new Subject<any>();

  /**
   * Observable of metadata current language.
   * @item {Observable<Object>}
   */
  metadataLanguage$ = this.metadataLanguageSource.asObservable();

  /**
   * Create a new {@link I18nService}.
   * @param {Http} http - The that allow to get the languages supported and the languages as such from json files.
   * @param {CookieService} cookie - The that allow manage the language cookie.
   */
  constructor(private http: HttpClient, private cookie: CookieService) {
    this.init();
  }

  /**
   * Give the supported languages and set the language initial.
   */
  private init() {
    this.requestSupportedLanguages = this.getSupportedLanguages();
    this.requestSupportedLanguages.then(languages => {
      this._supportedLanguages = languages;
      this.setLanguage(this.getSelectedLanguageStr());
    });
  }

  /**
   * Give the supported languages.
   * @returns {Promise<any[]>} - The supported languages.
   */
  public get supportedLanguage(): Promise<any[]> {
    return this.requestSupportedLanguages;
  }

  /**
   * Give the abbreviation of browser language.
   * @returns {string}
   */
  private static getBrowserLanguageStr(): string {
    return window.navigator.language.split('-')[0];
  }

  /**
   * If exist a language cookie then take this, else take the browser language.
   * @returns {string} - The abbreviation of selected language.
   */
  private getSelectedLanguageStr(): string {
    const cookieLanguageStr = this.cookie.getValueOfCookie(I18nService.cookieLanguageName);
    return cookieLanguageStr || I18nService.getBrowserLanguageStr();
  }

  /**
   * Set the language in a cookie.
   * @param {string} languageStr - The abbreviation of the language.
   */
  private setLanguageInCookie(languageStr: string) {
    this.cookie.setCookie(I18nService.cookieLanguageName, languageStr, 1, I18nService.pathCookieLanguage);
  }

  /**
   * Give the abbreviation of current language.
   * @returns {string} - The string of current language.
   */
  public getAbbreviationCurrentLanguage(): string {
    return this.abbreviationLanguage;
  }

  /**
   * Set the language of the application.
   * @param {string} abbreviationLanguage - The abbreviation of the language.
   */
  public setLanguage(abbreviationLanguage: string): void {
    this._metadataLanguage = this._supportedLanguages.find(language => language.abbreviation == abbreviationLanguage);
    !this._metadataLanguage && (this._metadataLanguage = I18nService.defaultLanguage);
    this.getDataJson(this._metadataLanguage.urlJson).then(language => {
      this.language = language;
      this.abbreviationLanguage = abbreviationLanguage;
      this.update();
      this.setLanguageInCookie(abbreviationLanguage);
    });
  }

  /**
   * Give the supported languages from json file.
   * @returns {Promise<any[]>} - The array of supported languages.
   */
  private getSupportedLanguages(): Promise<any[]> {
    return this.getDataJson(I18nService.urlJsonLanguages);
  }

  /**
   * Give data object form json file.
   * @param {string} urlJson - The uri of the json file.
   * @returns {Promise<any>} - The data object from json file.
   */
  private getDataJson(urlJson: string): Promise<any> {
    return this.http.get(urlJson)
      .toPromise()
      .catch(error => {
        console.error("An error has occured", error);
        Promise.reject(error)
      });
  }

  /**
   * Update the [translatorSource]{@link I18nService#translatorSource} and [metadataLanguageSource]{@link I18nService#metadataLanguageSource} to current values.
   */
  public update(): void {
    this.translatorSource.next(new Translator(this));
    this.metadataLanguageSource.next(this._metadataLanguage);
  }


  /**
   * Give the value by key depend of current language.
   * @param {string} key
   * @returns {string} - The value depend of current language.
   */
  public getValue(key: string): string {
    const query = "this." + I18nService.variableNameLanguage + "." + key;
    let value = key;
    if (this.language && key) {
      value = eval(query);
    }
    return value;
  }

  /**
   * Give the metadata of current language.
   * @returns {any} - The metadata of current language.
   */
  public get metadataLanguage(): any {
    return this._metadataLanguage;
  }

}

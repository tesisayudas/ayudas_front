import {I18nService} from "./i18n.service";

/**
 * Represent a translator.
 */
export class Translator {

  /**
   * Create a new Translator.
   * @param {I18nService} i18n
   */
  constructor(private i18n: I18nService) {
  }

  /**
   * Give the value by key depend of current language.
   * @param {string} key
   * @returns {string} - The value in the current language.
   */
  getValue(key: string): string {
    return this.i18n.getAbbreviationCurrentLanguage() && this.i18n.getValue(key);
  }

}

import {Injectable} from '@angular/core';

/**
 * Represent the manager of cookies.
 */
@Injectable()
export class CookieService {

  /**
   * Give the value of a cookie by name.
   * @param {string} name - The name of the cookie.
   * @returns {string} - The value of the cookie.
   */
  public getValueOfCookie(name: string): string {
    const foundCookie: string = this.getCookie(name);
    return (foundCookie && foundCookie.split("=")[1]) || null;
  }

  /**
   * Delete a cookie by name.
   * @param name - The name of the cookie to delete.
   */
  public deleteCookie(name) {
    this.getCookie(name) && this.setCookie(name, "", -1);
  }

  /**
   * Create a new cookie.
   * @param {string} name - The name of the new cookie.
   * @param {string} value - The value of the new cookie.
   * @param {number} expireDays - The days to expire the cookie.
   * @param {string} path - The path of the cookie.
   */
  public setCookie(name: string, value: string, expireDays: number, path?: string) {
    const expireDate: Date = new Date();
    expireDate.setDate(expireDate.getDate() + expireDays);
    const pathStr = (path && `path=${path}`) || "";
    document.cookie = `${name}=${value}; expires=${expireDate.toUTCString()}; ${pathStr}`;
  }

  /**
   * Give the cookie by name.
   * @param {string} name - The name of the cookie.
   * @returns {string} - The cookie.
   */
  private getCookie(name: string): string {
    return this.getCookiesArray().find(c => c.startsWith(name));
  }

  /**
   * Give the array of cookies existing.
   * @returns {string[]} - The array of cookies existing.
   */
  private getCookiesArray(): string[] {
    return document.cookie.split(";");
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookieService } from './cookie.service';
import { I18nService } from './i18n.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    I18nService,
    CookieService
  ]
})
export class I18nModule { }

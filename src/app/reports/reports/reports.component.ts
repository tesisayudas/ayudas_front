import {Component, OnInit} from '@angular/core';
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {ReportsService} from "../reports.service";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {DateService} from "@g3/core/date.service";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";

@Component({
  selector: 'g3-reports',
  templateUrl: './reports.component.html',
  styles: []
})
export class ReportsComponent implements OnInit {

  readonly reportTypes = [
    {
      name: "Reporte por solicitante",
      value: "resource-types-reservations-by-requester-types"
    },
    {
      name: "Reporte por programa",
      value: "resource-types-reservations-by-student-programs"
    }
  ];

  reportDataDoughnut: {resource: string, requesters: string[], count: number[]}[] = [];
  reportDataBar: {requesters: string[], data: {label: string, data: number[]}[]} = {
    requesters: [],
    data: []
  };
  customForm: CustomForm;
  validator: CustomFormValidator;

  constructor(private fb: FormBuilder,
              private i18n: I18nService,
              private snack: SnackbarService,
              private service: ReportsService) {
  }

  ngOnInit() {
    this.initForm();
    this.validator = CustomFormUtils.addEditNoChangeValidator(this.customForm);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        reportType: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        from: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        to: ['', Validators.compose([CustomValidators.requiredWithTrim])]
      })
      .handlerSaveResponse((response, action) => {
        this.dountsReport(response);
        this.barReport(response);
        CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18n);
        this.validator.active();
      })
      .create((value) => {
        this.reportDataDoughnut = [];
        this.reportDataBar = {
          requesters: [],
            data: []
        };
        this.service.getReport(value.reportType).then(response => this.customForm.handlerSaveResponse(response, null));
      })
      .updatePredicate(() => false)
      .prepareValue((val) => {
        const map = new Map<string, string>();
        map.set('from', DateService.getDateWithFormHyphenSeparateValue(val.from));
        map.set('to', DateService.getDateWithFormHyphenSeparateValue(val.to));
        this.service.setFilters(map);
        delete val.from;
        delete val.to;
        return val
      })
      .refreshList(false)
      .submiter(this.service)
      .build()
  }

  get reportType(): FormControl {
    return this.customForm.form.get('reportType') as FormControl;
  }

  get to(): FormControl {
    return this.customForm.form.get('to') as FormControl;
  }
  get from(): FormControl {
    return this.customForm.form.get('from') as FormControl;
  }

  private barReport(response: any){
    response.forEach(element => {
      const existRequester = !!element.requester ? this.reportDataBar.requesters.findIndex(requester => requester == element.requester): this.reportDataBar.requesters.findIndex(requester => requester == element.program);
      const existResource = this.reportDataBar.data.find(data => data.label == element.resource);
      if(existRequester < 0) {
        if(!!element.requester) this.reportDataBar.requesters.push(element.requester);
        else if (!!element.program) this.reportDataBar.requesters.push(element.program);
      }
      const index = existRequester >= 0 ? existRequester : (this.reportDataBar.requesters.length - 1);
      if(!existResource) {
        const label = element.resource;
        const data = [element.total];
        this.reportDataBar.data.push({data, label});
      } else {
        existResource.data.push(index, 0 ,element.total);
      }
    });
  }

  private dountsReport(response: any) {
    response.forEach(element => {
      let isElementAdd = false;
      for(let i = 0; i < this.reportDataDoughnut.length; i++){
        if(this.reportDataDoughnut[i].resource == element.resource) {
          if(element.requester) {
            this.reportDataDoughnut[i].requesters.push(element.requester);
          } else if(element.program) {
            this.reportDataDoughnut[i].requesters.push(element.program);
          }
          this.reportDataDoughnut[i].count.push(element.total);
          isElementAdd = true;
        }
      }
      if(!isElementAdd) {
        const resource = element.resource;
        const requesters = [];
        if(element.requester) {
          requesters.push(element.requester);
        } else if(element.program) {
          requesters.push(element.program);
        }
        const count = [];
        count.push(element.total);
        const data = {resource, requesters, count};
        this.reportDataDoughnut.push(data)
      }
    });
  }
}

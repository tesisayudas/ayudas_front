import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {HttpUtils} from "@g3/crud/model/http.utils";
import {URLS} from "../sori/elements";

@Injectable()
export class ReportsService extends BackItemCrudManager {

  constructor(private httpClient: HttpClient, private router: Router) {
    super(httpClient, URLS.reports, router);
  }

  getReport(name: string) {
    const urlParams = this.defaultCrudUrls.getUrlParameters().split('?');
    const params = urlParams[0].split('limit=10&');
    const url = `${this.defaultCrudUrls.basicUrl()}/${name}?${params[1]}`;
    return this.httpClient.get(url)
      .toPromise()
      .then(response => response)
      .catch(error => HttpUtils.handleError(error, this._router));
  }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportsRoutingModule} from './reports-routing.module';
import {ReportsComponent} from './reports/reports.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../sori/sori.module";
import {CrudModule} from "@g3/crud/crud.module";
import {ReportsService} from "./reports.service";
import {ChartsModule} from "ng2-charts";

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule,
    ChartsModule
  ],
  declarations: [ReportsComponent],
  providers: [ReportsService]
})
export class ReportsModule { }

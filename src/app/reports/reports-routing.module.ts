import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportsComponent} from "./reports/reports.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    data: {
      titleKey: 'navigation.reports',
      resource: ACCESS_RESOURCES.reports
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }

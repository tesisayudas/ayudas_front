import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {URLS} from "../../sori/elements";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {ItemDeletableModel, ItemFilterableModel} from "@g3/crud/model/model-interfaces";
import {HttpUtils} from "@g3/crud/model/http.utils";
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";

@Injectable()
export class ReservationService extends BackItemCrudManager implements ItemFilterableModel, ItemDeletableModel, Resolve<any> {

  constructor(private http: HttpClient, private router: Router ) {
    super(http, URLS.request_reservation, router);
  }

  delete(id) {
    return super.deleted(id, null);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.getById(route.params.id);
  }

  assignResources(id: number, body: any){
    return this.http.post(`${this.defaultCrudUrls.basicUrl()}/${id}/assign-resources`, body)
      .toPromise()
      .then(item => item)
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  changeStatus(id: string, body: any) {
    const url = `${this.defaultCrudUrls.basicUrl()}/${id}`;
    return this.http.patch(url, body).toPromise()
      .then(item => item)
      .catch(error => HttpUtils.handleError(error, this._router));
  }

}

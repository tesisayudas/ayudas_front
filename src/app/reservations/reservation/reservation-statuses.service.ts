import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {HttpClient} from "@angular/common/http";
import {URLS} from "../../sori/elements";
import {Router} from "@angular/router";
import {fromPromise} from "rxjs/observable/fromPromise";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {distinctUntilChanged, switchMap} from "rxjs/operators";
import {ItemFilterableModel, ItemSearchableModel} from "@g3/crud/model/model-interfaces";

@Injectable()
export class ReservationStatusesService extends BackItemCrudManager implements ItemFilterableModel, ItemSearchableModel {

  private searchTermSubject: Subject<{ term: string, reservationStatusId: string }> = new Subject<{ term: string, reservationStatusId: string }>();
  private _reservationStatusId: any;

  constructor(private http: HttpClient, private router: Router ) {
    super(http, URLS.reservation_statuses, router);
  }

  nextTerm(term: string) {
    if (!!term) {
      this.searchTermSubject.next({term: term, reservationStatusId: this.reservationStatusId});
    } else {
      this.searchTermSubject.next({term: '', reservationStatusId: this.reservationStatusId});
    }
  }

  search(terms: Observable<any>): Observable<any[]> {
    return terms.pipe(
      distinctUntilChanged((a: any, b: any) => {
        return a.term == b.term && a.reservationStatusId == b.reservationStatusId;
      }), switchMap(term => {
        return this.searchItems(term)
      }));
  }

  searchItems(value): Observable<any[]> {
    const filter = new Map<string, string>();
    filter.set('currentStatus', value.reservationStatusId);
    this.setFilters(filter);
    return fromPromise(this.getAll());
  }

  searchSubject(): Subject<any> {
    return this.searchTermSubject;
  }

  get reservationStatusId(): any {
    return this._reservationStatusId;
  }

  set reservationStatusId(value: any) {
    this._reservationStatusId = value;
    this.searchTermSubject.next({term: '', reservationStatusId: value});
  }

}

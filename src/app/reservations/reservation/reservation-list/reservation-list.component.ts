import {Component, OnInit} from '@angular/core';

import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  IOnActionClick,
  TableBuilder,
  TableColumnsBuilder, TableOptionItemBuilder,
  TableOptionsItemBuilder
} from "../../../crud/view/table/generic-table";
import {ReservationService} from "../reservation.service";
import {Observable} from "rxjs/Observable";
import {map, tap} from "rxjs/operators";
import * as moment from 'moment';
import {DATE_TIME_FORMAT} from "@g3/core/date-utils.service";
import {ReservationFilterComponent} from "../reservation-filter/reservation-filter.component";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {NavigationBackService} from "@g3/core/navigation-back.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {AVAILABLE_SCOPES} from "@g3/core/auth/permissions";
import {AuthService} from "@g3/core/auth/auth.service";

@Component({
  selector: 'g3-reservation-list',
  templateUrl: './reservation-list.component.html',
  styles: []
})
export class ReservationListComponent extends BasicItemListComponent<ReservationService> implements OnInit {

  public customList: Observable<any[]>;

  private reservation: any;

  constructor(public service: ReservationService,
              private paginator: PaginatorService,
              private i18nService: I18nService,
              private snack: SnackbarService,
              private navigationBackService: NavigationBackService,
              private router: Router,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private authService: AuthService) {
    super(service, paginator)
  }

  ngOnInit() {
    this.customList = this.service.list$.pipe(tap(console.log), map((list: any) => list.items.map(item => {
      item.requester = item.requester.ldapId;
      item.startDate = moment.unix(item.startDate / 1000).format(DATE_TIME_FORMAT);
      item.endDate = moment.unix(item.endDate / 1000).format(DATE_TIME_FORMAT);
      item.color = item.currentStatus.color;
      item.currentStatus = item.currentStatus.name;
      return item;
    })));
    this.goToDetails = (entity) => {
      this.navigationBackService.urlBack = this.router.url;
      this.router.navigate([`../${entity.id}/details`], {relativeTo: this.route})
    }
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(new TableOptionsItemBuilder()
      .add(new TableOptionItemBuilder()
        .key('remove')
        .label('shared.remove')
        .icon('close-circle')
        .canShow((entity) => entity.requester === this.authService.userData.username)
        .scopeKey(AVAILABLE_SCOPES.delete)
        .build())
      .add(GenericButton.View())
      .build())
    .columns(new TableColumnsBuilder()
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--0 mdl-data-table__cell--non-numeric')
        .hasDot(true)
        .showAlways(true)
        .addColorKey('color')
        .showInMobile(true).build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(true)
        .key('startDate')
        .label('reservation.start_date').build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(false)
        .key('endDate')
        .label('reservation.end_date').build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(true)
        .key('requester')
        .label('reservation.user').build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(false)
        .key('locationDescription')
        .label('reservation.form.locationDescription').build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(true)
        .key('currentStatus')
        .label('shared.status').build())
      .build())
    .filter({component: ReservationFilterComponent})
    .build();

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(null, null, null, null);
  }

  onActionClick(event: IOnActionClick): void {
    switch (event.key) {
      case 'edit':
        return this.edit(event.entity);
      case 'view':
        return this.goToDetails(event.entity);
      case 'remove':
        return this.confirmationCancel(event.entity);
      default:
        break;
    }
  }

  confirmationCancel(entity) {
    this.reservation = entity;
    this.dialogService.showDialogComponent('confirmationCancelDialog');
  }

  onDelete() {
    this.service.deleted(this.reservation.id, this.paginator)
      .then(() => {
        this.snack.showToast(this.i18nService.getValue('reservation.cancellation_success'))
      })
  }
}

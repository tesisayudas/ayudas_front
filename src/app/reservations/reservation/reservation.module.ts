import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReservationRoutingModule} from './reservation-routing.module';
import {ReservationListComponent} from './reservation-list/reservation-list.component';
import {ReservationFormComponent} from './reservation-form/reservation-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../../sori/sori.module";
import {ReservationService} from "./reservation.service";
import {ReservationFilterComponent} from './reservation-filter/reservation-filter.component';
import {ReservationDetailComponent} from './reservation-detail/reservation-detail.component';
import {ReservationAssignResourceFormComponent} from './reservation-detail/reservation-assign-resource-form/reservation-assign-resource-form.component';
import {ReservationResourcesComponent} from './reservation-detail/reservation-resources/reservation-resources.component';
import {CrudModule} from "@g3/crud/crud.module";

@NgModule({
  imports: [
    CommonModule,
    ReservationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  exports: [ReservationResourcesComponent],
  declarations: [ReservationListComponent, ReservationFormComponent, ReservationFilterComponent, ReservationDetailComponent, ReservationAssignResourceFormComponent, ReservationResourcesComponent],
  providers: [ReservationService],
  entryComponents: [ReservationFilterComponent]
})
export class ReservationModule { }

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReservationListComponent} from "./reservation-list/reservation-list.component";
import {ReservationDetailComponent} from "./reservation-detail/reservation-detail.component";
import {ReservationService} from "./reservation.service";
import {ACCESS_RESOURCES, AVAILABLE_SCOPES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: ReservationListComponent,
    data: {
      titleKey: 'navigation.reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.paginate
    }
  },
  /*
  {
    path: 'create',
    component: ReservationFormComponent,
    data: {
      titleKey: 'navigation.reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.create
    }
  },*/
  {
    path: ':id/details',
    component: ReservationDetailComponent,
    data: {
      titleKey: 'navigation.reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.show
    },
    resolve: {reservation: ReservationService}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule { }

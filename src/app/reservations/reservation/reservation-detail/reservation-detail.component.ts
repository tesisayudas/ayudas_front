import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {map, tap} from "rxjs/operators";
import {Subscription} from "rxjs/Subscription";
import {ReservationService} from "../reservation.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ConfirmationDialogComponent} from "@g3/crud/view/confirmation-dialog/confirmation-dialog.component";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import * as moment from 'moment';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {combineLatest} from "rxjs/observable/combineLatest";
import {PermissionService} from "@g3/core/auth/permission.service";
import {BASIC_DATE_FORMAT, DateService} from "@g3/core/date.service";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {MonitorService} from "../../../monitors/monitor.service";
import {FormBuilder, Validators} from "@angular/forms";
import {NavigationBackService} from "@g3/core/navigation-back.service";
import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";

@Component({
  selector: 'g3-reservation-detail',
  templateUrl: './reservation-detail.component.html',
  styleUrls: []
})
export class ReservationDetailComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  reservation;
  reservationsResources: Map<string, any | any[]> = new Map< string, any| any[]>();
  resource: any = null;
  canAssignResources$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  assignResources: BehaviorSubject<boolean>[] = [];
  existResource$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  customForm: CustomForm;
  canAssign: boolean = false;
  canBorrow: boolean = false;

  @ViewChild(ConfirmationDialogComponent) dialog: ConfirmationDialogComponent;
  monitors: any[] = [];

  constructor(private route: ActivatedRoute,
              private permissionService: PermissionService,
              private reservationService: ReservationService,
              private snack: SnackbarService,
              private i18n: I18nService,
              private dialogService: DialogService,
              private monitorService: MonitorService,
              private fb: FormBuilder,
              private navigationBackService: NavigationBackService) {
  }

  ngOnInit() {
    this.navigationBackService.urlBack = 'reservations/reservation';
    this.initForm();
    this.sub = this.route.data.pipe(
      map(res => {
        res.reservation.startDate = DateService.getCurrentUTCDateWithTime(res.reservation.startDate);
        res.reservation.endDate = DateService.getCurrentUTCDateWithTime(res.reservation.endDate);
        return res.reservation
      }),
      tap(res => {
        this.reservation = res;
      })
    ).subscribe();
    this.reservation.resourceReservations.forEach(() => {
      const event = new BehaviorSubject<boolean>(false);
      this.assignResources.push(event);
    });
    combineLatest(this.assignResources).subscribe((res) => {
      if(res.findIndex(val => val === false) === -1) this.canAssignResources$.next(true);
    });
    this.putMonitors();
    this.permissionService.hasRolAccess(AVAILABLE_ROLES.SENA, AVAILABLE_ROLES.auxiliar, AVAILABLE_ROLES.coordinator).subscribe(val => this.canAssign = val);
    this.permissionService.hasRolAccess(AVAILABLE_ROLES.monitor, AVAILABLE_ROLES.SENA, AVAILABLE_ROLES.auxiliar, AVAILABLE_ROLES.coordinator).subscribe(val => this.canBorrow = val);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  showData(event, index){
    this.assignResources[index].next(event);
  }

  sendReservation(){
    const resources = [];
    const extras = [];
    const date = moment(this.reservation.startDate, BASIC_DATE_FORMAT + '  hh:mm a').unix() * 1000;
    this.reservationsResources.forEach(element => {
      const prevResources = element;
      if(prevResources && prevResources.length) {
        prevResources.forEach(resource => resources.push(resource));
      } else {
        extras.push(element);
      }
    });
    const body = {reservationRepeatDate: date ,assignedResources : resources, assignedResourceExtras: extras, assignedMonitor: this.customForm.form.value};
    this.reservationService.assignResources(this.reservation.id ,body).then(res => {
      if(res == null) {
        this.existResource$.next(true);
        this.reservationService.getById(this.reservation.id).then(reservation => {
          this.reservation = reservation;
          this.snack.showToast(this.i18n.getValue('reservation.assign_resource_success'));
        })
      }
    });
  }

  private putMonitors() {
    this.monitorService.getEnabledList().then(res => {
      this.monitors = res;
    });
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: ['', Validators.required]
      })
      .build();
  }

  reservationChangeState(status: string) {
    switch (status) {
      case 'DENY':
        this.dialogService.showDialogComponent('confirmationDenied');
        break;
      case 'BORROW':
        this.dialogService.showDialogComponent('confirmationBorrow');
        break;
      case 'FINISH':
        this.dialogService.showDialogComponent('confirmationFinish');
        break;
    }
  }

  changeReservationStatus(action: string){
    const body = {action};
    this.reservationService.changeStatus(this.reservation.id, body).then(() => {
      this.reservationService.getById(this.reservation.id).then(reservation => {
        this.reservation = reservation;
        switch (action) {
          case 'DENY':
            this.snack.showToast(this.i18n.getValue('reservation.denied_success'));
            break;
          case 'BORROW':
            this.snack.showToast(this.i18n.getValue('reservation.borrow_success'));
            break;
          case 'FINISH':
            this.snack.showToast(this.i18n.getValue('reservation.finish_success'));
            break;
        }
      });
    })
  }
}

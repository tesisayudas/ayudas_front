import { Component, OnInit } from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {FormBuilder, Validators} from "@angular/forms";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {ReservationService} from "../../reservation.service";
import {ResourcesService} from "../../../../inventory/normal-resources/resources.service";

@Component({
  selector: 'g3-reservation-assign-resource-form',
  templateUrl: './reservation-assign-resource-form.component.html',
  styles: []
})
export class ReservationAssignResourceFormComponent implements OnInit, ItemFormDialog {

  startDate;
  endDate;
  resourceTypeId;
  reservationId;
  customForm: CustomForm;
  dialogForm: CustomFormDialog;
  validator: CustomFormValidator;
  resourcesTypes: any[] = [];

  constructor(private fb: FormBuilder,
              private i18n: I18nService,
              private snack: SnackbarService,
              private resourceService: ResourcesService,
              private service: ReservationService) {
    this.initForm();
    this.initDialog();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  ngOnInit() {
  }

  putAvailablesResources() {

  }

  open() {
    this.dialogForm.open();
  }

  setEntity(entity) {
    this.startDate = entity.startDate;
    this.endDate = entity.endDate;
    this.resourceTypeId = entity.resourceTypeId;
    this.reservationId = entity.reservationId;
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        type: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        quantity: ['', Validators.compose([CustomValidators.requiredWithTrim, CustomValidators.positiveNumber, CustomValidators.onlyInteger])]
      })
      .handlerSaveResponse((response, action)=> CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n, this.snack))
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }

  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('extra_resources.create')
      .updateTitleKey('extra_resources.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .build()
  }

}

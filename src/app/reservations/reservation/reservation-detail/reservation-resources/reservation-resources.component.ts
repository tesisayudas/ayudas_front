import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {shrinkOut} from "../../../../shared/animations";
import {FormArray, FormBuilder, Validators} from "@angular/forms";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {ResourcesService} from "../../../../inventory/normal-resources/resources.service";
import * as moment from 'moment';
import {ExtraResourcesService} from "../../../../inventory/extra-resources/extra-resources.service";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {PermissionService} from "@g3/core/auth/permission.service";
import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";
import {BehaviorSubject, Subscription} from "rxjs";

@Component({
  selector: 'g3-reservation-resources',
  templateUrl: './reservation-resources.component.html',
  styles: [`p{margin-bottom: 0}`],
  animations: [shrinkOut]
})
export class ReservationResourcesComponent implements OnInit, OnDestroy {
  isExtra: boolean = false;
  canShow: boolean;
  @Input() resource: any;
  @Input() dataMap: Map<string, any | any[]>;
  customForm: CustomForm;
  validator: CustomFormValidator;
  resources: any[] = [];
  selectedResources: any[] = [];
  @Output() dataSelected: EventEmitter<any> = new EventEmitter<any>();
  canAssign: boolean = false;
  @Input('existResource') existResources$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  subs : Subscription[] = [];

  constructor(private fb: FormBuilder,
              private resourcesServices: ResourcesService,
              private i18n: I18nService,
              private extraService: ExtraResourcesService,
              private permissionService: PermissionService) {
  }

  ngOnInit() {
    this.isExtra = this.resource.resourceType.extra;
    let availableToAssign;
    this.subs.push(this.permissionService.hasRolAccess(AVAILABLE_ROLES.monitor,
      AVAILABLE_ROLES.SENA,
      AVAILABLE_ROLES.coordinator,
      AVAILABLE_ROLES.auxiliar).subscribe(val => availableToAssign = val));
    this.canAssign = this.resource && (this.resource.assignedResources.length <= 0 && this.resource.assignedResourceExtras.length <= 0) && !!availableToAssign;
    if(!!(this.resource.assignedResources.length > 0)) {
      this.handlingAssignedResources();
    }
    if(!!(this.resource.assignedResourceExtras.length > 0)) {
      this.handlingAssignedResourcesExtra();
    }
    this.initForm();
    this.validator = CustomFormUtils.addEditNoChangeValidator(this.customForm);
    this.putResources();
  }

  show() {
    this.canShow = !this.canShow;
  }

  private initForm() {
    if (this.isExtra) {
      this.customForm = new CustomFormBuilder(this.fb).formStructure({
        quantity: ['', Validators.compose([CustomValidators.requiredWithTrim, CustomValidators.positiveNumber, Validators.max(this.resource.quantity)])],
        extra: this.fb.group({
          id: [this.resource.resourceType.id]
        })
      })
        .handlerSaveResponse((response, action) => CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18n))
        .refreshList(false)
        .update(() => this.save())
        .create(() => this.save())
        .build();
    } else {
      this.customForm = new CustomFormBuilder(this.fb)
        .formStructure({
          resources: this.fb.array([])
        })
        .handlerSaveResponse((response, action) => CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18n))
        .refreshList(false)
        .update(() => this.save())
        .create(() => this.save())
        .build();
      this.addControls();
    }
  }

  private addControls() {
    for (let i = 0; i < this.resource.quantity; i++) {
      const form = this.fb.group({
        resource: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        })
      });
      this.resourcesForm.push(form);
    }
    this.removeResources();
  }

  save() {
    if (!this.isExtra) {
      let val = this.customForm.form.value;
      val.resources.forEach(elementForm => {
        const newElement = this.selectedResources.find(elementSelected => elementSelected.id === elementForm.resource.id);
        elementForm.serial = !!newElement ? newElement.serial : null;
        elementForm.internalNumber = !!newElement ? newElement.internalNumber : null;
        elementForm.activeCode = !!newElement ? newElement.activeCode : null;
      });
      this.dataMap.set(this.resource.id, val.resources);
      this.canShow = false;
      this.dataSelected.next(true);
    } else {
      this.dataMap.set(this.resource.id, this.customForm.form.value);
      this.canShow = false;
      this.dataSelected.next(true);
    }
  }

  private putResources() {
    if (!this.isExtra) {
      const body = {
        dateStart: moment(this.resource.startDate).unix() * 1000,
        dateEnd: moment(this.resource.endDate).unix() * 1000,
        resourceType: {
          id: this.resource.resourceType.id
        }
      };
      this.resourcesServices.getFreeResources(body).then(res => {
        this.resources = res;
      });
    }
  }

  render(item) {
    return `${item.serial}`;
  }

  private removeResources() {
    this.resourcesForm.controls.forEach(element => {
      this.subs.push(element.valueChanges.subscribe(val => {
        const index = this.resources.findIndex(x => x.id === val.resource.id);
        this.selectedResources.push(this.resources[index]);
        if (index >= 0) this.resources.splice(index, 1);
      }));
    });
  }

  get resourcesForm(): FormArray {
    return this.customForm.form.get('resources') as FormArray;
  }

  private handlingAssignedResources(){
    const newArray = this.resource.assignedResources.map(element => element.resource);
    this.dataMap.set(this.resource.id, newArray);
    this.canAssign = false;
  }

  handlingAssignedResourcesExtra() {
    const totalResources = this.resource.assignedResourceExtras.find(element => element.quantity);
    this.dataMap.set(this.resource.id, totalResources);
    this.canAssign = false;
  }

  ngOnDestroy(): void {
    !!this.subs && this.subs.forEach(sub => sub.unsubscribe());
  }
}

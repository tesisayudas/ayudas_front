import {Component, ViewChild} from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {FormBuilder, Validators} from "@angular/forms";
import {ReservationTypesService} from "../reservation-types.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {RequesterTypesService} from "../../../parametrization/requester-types/requester-types.service";
import {SelectControlComponent} from "../../../shared/form-control/form-control-select/select-control/select-control.component";

@Component({
  selector: 'g3-reservation-types-form',
  template: `
    <g3-dialog-form [idDialog]="'reservationTypeDialog'" [form]="dialogForm" [validatorForm]="validator" [idForm]="'reservationTypeForm'">
      <form id="reservationTypeForm" [formGroup]="customForm.form">
        <!--Name-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="name" id="name"/>
          <label class="mdl-textfield__label" for="name">{{'shared.name' | i18n}}*</label>
        </div>

        <!--allowedRequesters-->
        <g3-select-control [isRequired]="true"
                           [formGroup]="customForm.form"
                           [results]="requesterTypes"
                           [idControl]="'allowedRequesters'"
                           [labelName]="'resource_types.requester_types'"
                           [titleBox]="'resource_types.allowed_requester'">
        </g3-select-control>
        <div>
          <h5>{{'reservation_types.isRepeatable' | i18n}}</h5>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1" g3UpgradeElements>
            <input type="radio" id="option-1" formControlName="isRepeatable" class="mdl-radio__button" value="true">
            <span class="mdl-radio__label" g3I18n>shared.yes</span>&nbsp;&nbsp;
          </label>
          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2" g3UpgradeElements>
            <input type="radio" id="option-2" formControlName="isRepeatable" class="mdl-radio__button" value="false">
            <span class="mdl-radio__label" g3I18n>shared.not</span>
          </label>
        </div>
      </form>
    </g3-dialog-form>
  `,
  styles: []
})
export class ReservationTypesFormComponent implements ItemFormDialog {

  dialogForm: CustomFormDialog;
  customForm: CustomForm;
  validator: CustomFormValidator;
  requesterTypes = [];
  @ViewChild(SelectControlComponent) select: SelectControlComponent;

  constructor(private fb: FormBuilder,
              private service: ReservationTypesService,
              private i18n: I18nService,
              private snack: SnackbarService,
              private requesterTypeService: RequesterTypesService) {
    this.initForm();
    this.initDialog();
    this.putRequesterTypes();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  open() {
    this.dialogForm.open();
    const dialog = document.getElementById("reservationTypeDialog");
    dialog.style.setProperty('overflow', 'visible');
  }

  setEntity(entity) {
    this.select.patchValue(entity.allowedRequesters);
    entity.isRepeatable = entity.isRepeatable.toString();
    this.dialogForm.setValue(entity);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        name: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])],
        allowedRequesters: this.fb.array([], CustomValidators.requiredWithTrim),
        isRepeatable: [false, Validators.compose([CustomValidators.requiredWithTrim])]
      })
      .handlerSaveResponse((response, action) => {
        CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n)
      })
      .prepareValue(value => value)
      .submiter(this.service)
      .build()
  }

  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('reservation_types.create')
      .updateTitleKey('reservation_types.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .handlerCloseDialog(() => {
        this.select.clear();
        this.customForm.reset();
      })
      .build()
  }

  private putRequesterTypes() {
    this.requesterTypeService.getEnabledList().then(res => this.requesterTypes = res);
  }
}

import { Injectable } from '@angular/core';
import {URLS} from "../../sori/elements";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";

@Injectable()
export class ReservationTypesService extends BackItemCrudManager {

  constructor(private http: HttpClient, private router: Router ) {
    super(http, URLS.reservation_types, router);
  }

}

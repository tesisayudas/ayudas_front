import {Component, OnInit, ViewChild} from '@angular/core';
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {ReservationTypesService} from "../reservation-types.service";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder
} from "@g3/crud/view/table/generic-table";
import {TableOptionsItemBuilder} from "@g3/crud/view/table/generic-table/table-option-item-builder";
import {ReservationTypesFormComponent} from "../reservation-types-form/reservation-types-form.component";

@Component({
  selector: 'g3-reservation-types-list',
  templateUrl: './reservation-types-list.component.html',
  styles: []
})
export class ReservationTypesListComponent extends BasicItemListComponent<ReservationTypesService> implements OnInit {

  @ViewChild(ReservationTypesFormComponent) form: ReservationTypesFormComponent;

  constructor(public service: ReservationTypesService, private paginator: PaginatorService) {
    super(service, paginator)
  }

  ngOnInit() {
  }

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, null, null, null);
  }

  public tableConfig: GenericTableInputs = new TableBuilder()
    .options(new TableOptionsItemBuilder()
      .add(GenericButton.View())
      .add(GenericButton.edit())
      .build())
    .columns(new TableColumnsBuilder()
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(true)
        .key('name')
        .label('shared.name').build())
      .addColumn(new ColumnBuilder()
        .styleClass('g3-flex-grow--3 mdl-data-table__cell--non-numeric')
        .showInMobile(true)
        .key('isRepeatable')
        .label('reservation_types.repeatable').build())
      .build())
    .build()

}

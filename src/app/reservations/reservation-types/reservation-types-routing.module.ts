import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReservationTypesListComponent} from "./reservation-types-list/reservation-types-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: ReservationTypesListComponent,
    data: {
      titleKey: 'navigation.reservation_types',
      resource: ACCESS_RESOURCES.reservationType
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationTypesRoutingModule { }

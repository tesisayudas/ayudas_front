import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservationTypesRoutingModule } from './reservation-types-routing.module';
import { ReservationTypesListComponent } from './reservation-types-list/reservation-types-list.component';
import { ReservationTypesFormComponent } from './reservation-types-form/reservation-types-form.component';
import {SoriModule} from "../../sori/sori.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CrudModule} from "@g3/crud/crud.module";

@NgModule({
  imports: [
    CommonModule,
    ReservationTypesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [ReservationTypesListComponent, ReservationTypesFormComponent]
})
export class ReservationTypesModule { }

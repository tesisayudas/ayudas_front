import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservationsRoutingModule } from './reservations-routing.module';
import {ReservationTypesService} from "./reservation-types/reservation-types.service";
import {RequesterTypesService} from "../parametrization/requester-types/requester-types.service";
import {ResourceTypesService} from "../parametrization/resource-types/resource-types.service";
import {ReservationStatusesService} from "./reservation/reservation-statuses.service";
import {ResourcesService} from "../inventory/normal-resources/resources.service";
import {DateService} from "@g3/core/date.service";
import {ExtraResourcesService} from "../inventory/extra-resources/extra-resources.service";
import {MonitorService} from "../monitors/monitor.service";

@NgModule({
  imports: [
    CommonModule,
    ReservationsRoutingModule
  ],
  declarations: [],
  providers: [
    ReservationTypesService,
    RequesterTypesService,
    ResourceTypesService,
    ReservationStatusesService,
    ResourcesService,
    DateService,
    ExtraResourcesService,
    MonitorService
  ]
})
export class ReservationsModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'reservation', loadChildren: './reservation/reservation.module#ReservationModule'},
  {path: 'reservation_types', loadChildren: './reservation-types/reservation-types.module#ReservationTypesModule'},
  {path: 'request', loadChildren: './request-reservation/request-reservation.module#RequestReservationModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationsRoutingModule { }

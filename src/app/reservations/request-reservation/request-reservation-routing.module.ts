import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestReservationListComponent} from "./request-reservation-list/request-reservation-list.component";
import {ACCESS_RESOURCES, AVAILABLE_SCOPES} from "@g3/core/auth/permissions";
import {RequestReservationFormComponent} from "./request-reservation-form/request-reservation-form.component";
import {RequestReservationDetailsComponent} from "./request-reservation-details/request-reservation-details.component";
import {RequestReservationService} from "./request-reservation.service";

const routes: Routes = [
  {
    path: '',
    component: RequestReservationListComponent,
    data: {
      titleKey: 'navigation.request_reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.paginate
    }
  },
  {
    path: 'create',
    component: RequestReservationFormComponent,
    data: {
      titleKey: 'navigation.request_reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.create
    }
  },
  {
    path: ':id/details',
    component: RequestReservationDetailsComponent,
    data: {
      titleKey: 'navigation.request_reservation',
      resource: ACCESS_RESOURCES.reservation,
      scope: AVAILABLE_SCOPES.show
    },
    resolve: {reservation: RequestReservationService}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestReservationRoutingModule { }

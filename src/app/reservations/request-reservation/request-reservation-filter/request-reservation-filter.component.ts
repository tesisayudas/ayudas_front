import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {FormBuilder} from "@angular/forms";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {isNumeric} from "rxjs/util/isNumeric";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {BASIC_DATE_FORMAT} from "@g3/core/date.service";
import * as moment from 'moment';
import {shrinkOut} from "../../../shared/animations";
import {G3DatepickerComponent} from "../../../shared/form-control/form-control-date/g3-datepicker.component";

@Component({
  selector: 'g3-request-reservation-filter',
  templateUrl: './request-reservation-filter.component.html',
  styles: [],
  animations: [shrinkOut]
})
export class RequestReservationFilterComponent implements OnInit, OnDestroy {

  canShow: boolean = false;
  filterForm: CustomForm;
  approved = [
    {
      id: 'null',
      name: 'En espera'
    },
    {
      id: true,
      name: 'Aprobado'
    },
    {
      id: false,
      name: 'Denegado'
    }
  ];
  @ViewChild(G3DatepickerComponent) picker: G3DatepickerComponent;

  constructor(private fb: FormBuilder,
              private paginator: PaginatorService,
              private i18n: I18nService,) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.filterForm = new CustomFormBuilder(this.fb)
      .formStructure({
        approved: [''],
        from: ['']
      })
      .handlerSaveResponse(response => {
        if (!isNumeric(response) && response != null && !response.id) {
          CustomFormUtils.handlerFormErrors(this.filterForm, response, this.i18n);
        }
      })
      .updatePredicate(() => false)
      .prepareValue(value => {
        return value;
      })
      .create(filter => {
        filter.from = moment(filter.from, BASIC_DATE_FORMAT).utc().format('YYYY-MM-DDTHH:mm:ssZ');
        const map = new Map<string, string>();
        for (let property in filter)
          if (filter.hasOwnProperty(property))
            map.set(property, filter[property]);
        this.paginator.service.clearFilters();
        this.paginator.service.setFilters(map);
        this.paginator.page = 0;
        this.paginator.updateList();
      })
      .build();
  }

  public reset(): void {
    this.filterForm.reset();
    this.picker._dateValue = null;
    this.paginator.service.clearFilters();
    this.paginator.updateList();
  }

  ngOnDestroy(): void {
    this.paginator.service.clearFilters();
  }
}

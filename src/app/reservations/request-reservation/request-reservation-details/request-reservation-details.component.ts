import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {ConfirmationDialogComponent} from "@g3/crud/view/confirmation-dialog/confirmation-dialog.component";
import {ActivatedRoute} from "@angular/router";
import {PermissionService} from "@g3/core/auth/permission.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {map, tap} from "rxjs/operators";
import {DateService} from "@g3/core/date.service";
import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";
import {RequestReservationService} from "../request-reservation.service";

@Component({
  selector: 'g3-request-reservation-details',
  templateUrl: './request-reservation-details.component.html',
  styles: []
})
export class RequestReservationDetailsComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  reservation;
  reservationsResources: Map<string, any | any[]> = new Map< string, any| any[]>();
  public showAprove: Observable<boolean>;
  resource: any = null;

  @ViewChild(ConfirmationDialogComponent) dialog: ConfirmationDialogComponent;

  constructor(private route: ActivatedRoute,
              private permissionService: PermissionService,
              private reservationService: RequestReservationService,
              private snack: SnackbarService,
              private i18n: I18nService,
              private dialogService: DialogService) {
  }

  ngOnInit() {
    this.sub = this.route.data.pipe(
      map(res => {
        res.reservation.startDate = DateService.getCurrentUTCDateWithTime(res.reservation.startDate);
        res.reservation.endDate = DateService.getCurrentUTCDateWithTime(res.reservation.endDate);
        res.reservation.resourceReservations.forEach(element => element.assignedResources = []);
        return res.reservation
      }),
      tap(res => {
        this.reservation = res;
      })
    ).subscribe();
    this.showAprove = this.permissionService.hasRolAccess(AVAILABLE_ROLES.coordinator);
  }

  getApproved(approved: boolean | null) {
    switch (approved) {
      case true:
        return 'Aprobado';
      case false:
        return 'Denegado';
      default:
        return 'En espera';
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  toApproved(approve: boolean){
    this.reservationService.enable(this.reservation.id, !this.reservation.approved).then(res => {
      if(res.status == 204) {
        this.reservation.approved = approve;
        this.snack.showToast(this.i18n.getValue(approve ? 'reservation.approved_success' : 'reservation.denied_success'))
      }
    });
  }

  confirmationApproved(approve: boolean){
    if (approve) {
      this.dialogService.showDialogComponent('confirmationApproved');
    } else {
      this.dialogService.showDialogComponent('confirmationDenied')
    }
  }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {BASIC_DATE_FORMAT} from "@g3/core/date.service";
import {BehaviorSubject, Subscription} from "rxjs";
import {FormArray, FormBuilder, FormControl, Validators} from "@angular/forms";
import {ReservationTypesService} from "../../reservation-types/reservation-types.service";
import {ResourceTypesService} from "../../../parametrization/resource-types/resource-types.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {NavigationBackService} from "@g3/core/navigation-back.service";
import {ActivatedRoute} from "@angular/router";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {DATE_TIME_24_FORMAT} from "@g3/core/date-utils.service";
import * as moment from 'moment';
import {RequestReservationService} from "../request-reservation.service";
import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';

@Component({
  selector: 'g3-request-reservation-form',
  templateUrl: './request-reservation-form.component.html',
  styles: []
})
export class RequestReservationFormComponent implements OnInit, OnDestroy {

  customForm: CustomForm;
  validator: CustomFormValidator;
  reservationTypes = [];
  resourceTypes = [];
  today = moment().subtract(1, "day").format(BASIC_DATE_FORMAT);
  private isRepeatableTye$ = new BehaviorSubject<boolean>(null);
  isSelectedReservationType: boolean = false;
  subs: Subscription[] = [];

  constructor(private fb: FormBuilder,
              public reservationService: RequestReservationService,
              public reservationTypeService: ReservationTypesService,
              public resourceTypeService: ResourceTypesService,
              private i18n: I18nService,
              private snack: SnackbarService,
              private navigationBackService: NavigationBackService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.navigationBackService.urlBack = 'reservations/request';
    this.initForm();
    this.validator = CustomFormUtils.addEditNoChangeValidator(this.customForm);
    this.putReservationTypes();
    this.putResourceTypes();
    this.subs.push(this.isRepeatableTye$.subscribe((val) => {
      if (!val && this.isSelectedReservationType) {
        this.removeRepeatableForm();
      } else {
        this.addRepeatableForm();
      }
    }));
    this.subs.push(this.reservationType.valueChanges.subscribe(val => {
      if (!!val.id) {
        this.isSelectedReservationType = true;
        this.isRepeatableTye$.next(this.isRepeatableType(val.id));
      }
    }));
    this.customForm.form.get('startDate').valueChanges.pipe(distinctUntilChanged()).subscribe(() => this.customForm.form.get('rangeTime').updateValueAndValidity());
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        reason: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        locationDescription: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        reservationType: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        startDate: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        rangeTime: this.fb.group({
          startTime: ['08:00', CustomValidators.requiredWithTrim],
          endTime: ['09:00', CustomValidators.requiredWithTrim],
        }, {validator: CustomValidators.endGreaterThanStartTimeAndAfterNow})
      })
      .prepareValue(value => {
        if(!value.finalDate) {
          const startTime = moment(`${value.startDate} ${value.rangeTime.startTime}`, DATE_TIME_24_FORMAT).unix() * 1000;
          const endTime = moment(`${value.startDate} ${value.rangeTime.endTime}`, DATE_TIME_24_FORMAT).unix() * 1000;
          value.startDate = startTime;
          value.endDate = endTime;
        } else {
          const startTime = moment(`${value.startDate}`, DATE_TIME_24_FORMAT).unix() * 1000;
          const endTime = moment(`${value.finalDate}`, DATE_TIME_24_FORMAT).unix() * 1000;
          delete value.finalDate;
          value.startDate = startTime;
          value.endDate = endTime;
        }
        return value;
      })
      .refreshList(false)
      .handlerSaveResponse((response, action) => {
        CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18n);
        if (response < 400) {
          this.navigationBackService.goToBack();
        }
      })
      .submiter(this.reservationService)
      .build();
    this.customForm.form.addControl('resourceReservations', new FormArray([]));
    this.addResource();
  }

  private putReservationTypes() {
    this.reservationTypeService.getEnabledList().then(value => {
      this.reservationTypes = value;
    });
  }

  private putResourceTypes() {
    this.resourceTypeService.getEnabledList().then(value => {
      this.resourceTypes = value;
    });
  }

  addResource() {
    const resourceForm = this.fb.group({
      quantity: ['', Validators.compose([CustomValidators.requiredWithTrim, CustomValidators.positiveNumber])],
      resourceType: this.fb.group({
        id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
      })
    });
    this.resourceReservation.push(resourceForm);
  }

  removeResource(index: number) {
    this.resourceReservation.removeAt(index);
  }

  public back() {
    this.navigationBackService.goToBack();
  }

  get startDate() {
    return this.customForm.form.get('startDate');
  }

  get endDate() {
    return this.customForm.form.get('endDate');
  }

  get resourceReservation(): FormArray {
    return this.customForm.form.get('resourceReservations') as FormArray;
  }

  get reservationType(): FormControl {
    return this.customForm.form.get('reservationType') as FormControl;
  }

  get days() : FormArray {
    return this.customForm.form.get('days') as FormArray;
  }

  private isRepeatableType(id: string) {
    const type = this.reservationTypes.find(val => val.id == id);
    return type.isRepeatable;
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub && sub.unsubscribe());
  }

  private addRepeatableForm() {
    this.customForm.form.addControl('finalDate', new FormControl('', [Validators.compose([CustomValidators.requiredWithTrim])]));
    const days = this.fb.array([]);
    this.customForm.form.addControl('days', days);
  }

  private removeRepeatableForm() {
    this.customForm.form.removeControl('days');
    this.customForm.form.removeControl('finalDate');
  }

  addDay(day: string) {
    const days = this.days.value;
    const index = days.findIndex(element => element == day);
    if(index < 0) {
      this.days.push(new FormControl(day));
    } else {
      this.days.removeAt(index);
    }
  }
}

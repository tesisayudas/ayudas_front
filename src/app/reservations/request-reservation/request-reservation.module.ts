import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestReservationRoutingModule } from './request-reservation-routing.module';
import { RequestReservationListComponent } from './request-reservation-list/request-reservation-list.component';
import { RequestReservationFormComponent } from './request-reservation-form/request-reservation-form.component';
import {RequestReservationService} from "./request-reservation.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../../sori/sori.module";
import {CrudModule} from "@g3/crud/crud.module";
import { RequestReservationDetailsComponent } from './request-reservation-details/request-reservation-details.component';
import {ReservationModule} from "../reservation/reservation.module";
import { RequestReservationFilterComponent } from './request-reservation-filter/request-reservation-filter.component';

@NgModule({
  imports: [
    CommonModule,
    RequestReservationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule,
    ReservationModule
  ],
  declarations: [RequestReservationListComponent, RequestReservationFormComponent, RequestReservationDetailsComponent, RequestReservationFilterComponent],
  providers: [RequestReservationService],
  entryComponents: [RequestReservationFilterComponent]
})
export class RequestReservationModule { }

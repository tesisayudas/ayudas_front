import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {ItemDeletableModel, ItemFilterableModel} from "@g3/crud/model/model-interfaces";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {URLS} from "../../sori/elements";
import {Observable} from "rxjs";
import {HttpUtils} from "@g3/crud/model/http.utils";

@Injectable()
export class RequestReservationService extends BackItemCrudManager implements ItemFilterableModel, ItemDeletableModel, Resolve<any>  {

  constructor(private http: HttpClient, private router: Router) {
    super(http, URLS.reservation, router);
  }

  delete(id) {
    return super.deleted(id, null);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getById(route.params.id);
  }

  assignResources(id: number, body: any){
    return this.http.post(`${this.defaultCrudUrls.basicUrl()}/${id}/assign-resources`, body)
      .toPromise()
      .then(item => item)
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

}

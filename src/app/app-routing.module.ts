import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticatedComponent} from "@g3/core/auth/authenticated.component";
import {AVAILABLE_ROLES} from "@g3/core/auth/permissions";
import {RoleGuard} from "@g3/core/auth/role.guard";
import {NotFoundComponent} from "@g3/core/not-found/not-found.component";
import {IntroComponent} from "@g3/core/intro/intro.component";

export const routes: Routes = [
  {path: '', redirectTo: 'intro', pathMatch: 'full'},
  {path: 'intro', component: IntroComponent},
  {
    path: 'parametrization',
    loadChildren: './parametrization/parametrization.module#ParametrizationModule',
    canActivate: [RoleGuard],
    data: {
      roles: [
        AVAILABLE_ROLES.coordinator
      ]
    }
  },
  {
    path: 'inventory',
    loadChildren: './inventory/inventory.module#InventoryModule',
    canActivate: [RoleGuard],
    data: {
      roles: [
        AVAILABLE_ROLES.coordinator,
        AVAILABLE_ROLES.auxiliar,
        AVAILABLE_ROLES.monitor
      ]
    }
  },
  {
    path: 'reservations',
    loadChildren: './reservations/reservations.module#ReservationsModule',
    canActivate: [RoleGuard],
    data: {
      roles: [
        AVAILABLE_ROLES.student,
        AVAILABLE_ROLES.monitor,
        AVAILABLE_ROLES.auxiliar,
        AVAILABLE_ROLES.coordinator,
        AVAILABLE_ROLES.functionary,
        AVAILABLE_ROLES.SENA,
        AVAILABLE_ROLES.teacher
      ]
    }
  },
  {
    path: 'reports',
    loadChildren: './reports/reports.module#ReportsModule',
    canActivate: [RoleGuard],
    data: {
      roles: [
        AVAILABLE_ROLES.auxiliar,
        AVAILABLE_ROLES.coordinator,
        AVAILABLE_ROLES.admin,
        AVAILABLE_ROLES.SENA
      ]
    }
  },
  {
    path: 'monitors',
    loadChildren: './monitors/monitors.module#MonitorsModule',
    canActivate: [RoleGuard],
    data: {
      roles: [
        AVAILABLE_ROLES.coordinator
      ]
    }
  },
  {
    path: 'error/:errorCode', component: NotFoundComponent
  },
  {path: 'authenticated', component: AuthenticatedComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

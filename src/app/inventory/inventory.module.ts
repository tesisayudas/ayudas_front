import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InventoryRoutingModule } from './inventory-routing.module';
import {ResourceTypesService} from "../parametrization/resource-types/resource-types.service";

@NgModule({
  imports: [
    CommonModule,
    InventoryRoutingModule
  ],
  declarations: [],
  providers: [ResourceTypesService]
})
export class InventoryModule { }

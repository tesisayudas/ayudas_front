import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'normal_resources', loadChildren: './normal-resources/normal-resources.module#NormalResourcesModule'},
  {path: 'extra_resources', loadChildren: './extra-resources/extra-resources.module#ExtraResourcesModule'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NormalResourcesRoutingModule} from './normal-resources-routing.module';
import {NormalResourcesListComponent} from './normal-resources-list/normal-resources-list.component';
import {NormalResourcesFormComponent} from './normal-resources-form/normal-resources-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CrudModule} from "@g3/crud/crud.module";
import {SoriModule} from "../../sori/sori.module";
import {ResourcesService} from "./resources.service";
import {UnitiesService} from "../../parametrization/unities/unities.service";
import {ResourceStatusesService} from "../../parametrization/resource-statuses/resource-statuses.service";
import {ResourceTypesService} from "../../parametrization/resource-types/resource-types.service";

@NgModule({
  imports: [
    CommonModule,
    NormalResourcesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [NormalResourcesListComponent, NormalResourcesFormComponent],
  providers: [ResourcesService, UnitiesService, ResourceStatusesService, ResourceTypesService]
})
export class NormalResourcesModule {
}

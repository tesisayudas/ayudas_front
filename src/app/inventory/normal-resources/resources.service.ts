import {Injectable} from '@angular/core';
import {BackItemCrudManager} from "app/crud/model/item-crud-manager";
import {HttpClient} from "@angular/common/http";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {URLS} from "../../sori/elements";
import {HttpUtils} from "@g3/crud/model/http.utils";

@Injectable()
export class ResourcesService extends BackItemCrudManager implements Resolve<any>{

  constructor(private http: HttpClient, private router: Router) {
    super(http, URLS.resources, router)
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.getById(route.params.id);
  }

  getFreeResources(body) {
    return this.http.post(`${this.defaultCrudUrls.basicUrl()}/free`, body)
      .toPromise()
      .then(response => response)
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

}

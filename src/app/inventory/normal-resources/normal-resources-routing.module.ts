import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NormalResourcesListComponent} from "./normal-resources-list/normal-resources-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";
import {NormalResourcesFormComponent} from "./normal-resources-form/normal-resources-form.component";
import {ResourcesService} from "./resources.service";
import {RoleGuard} from "@g3/core/auth/role.guard";

const routes: Routes = [
  {
    path: '',
    component: NormalResourcesListComponent,
    canActivate: [RoleGuard],
    data: {
      titleKey: 'navigation.resources',
      resource: ACCESS_RESOURCES.resource
    }
  },
  {
    path: 'create',
    component: NormalResourcesFormComponent,
    canActivate: [RoleGuard],
    data: {
      titleKey: 'navigation.resources',
      resource: ACCESS_RESOURCES.resource
    }
  },
  {
    path: ':id/edit',
    component: NormalResourcesFormComponent,
    canActivate: [RoleGuard],
    data: {
      titleKey: 'navigation.resources',
      resource: ACCESS_RESOURCES.resource
    },
    resolve: {
      resource: ResourcesService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NormalResourcesRoutingModule {
}

import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, ViewChildren} from '@angular/core';
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {FormBuilder, Validators} from "@angular/forms";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {ResourcesService} from "../resources.service";
import {UnitiesService} from "../../../parametrization/unities/unities.service";
import {ResourceStatusesService} from "../../../parametrization/resource-statuses/resource-statuses.service";
import {ResourceTypesService} from "../../../parametrization/resource-types/resource-types.service";
import {SelectControlComponent} from "../../../shared/form-control/form-control-select/select-control/select-control.component";
import {Subscription} from "rxjs/Subscription";
import {NavigationBackService} from "@g3/core/navigation-back.service";

@Component({
  selector: 'g3-normal-resources-form',
  templateUrl: './normal-resources-form.component.html',
  styles: []
})
export class NormalResourcesFormComponent implements OnDestroy, AfterViewInit {

  customForm: CustomForm;
  validator: CustomFormValidator;
  resourceTypes = [];
  unities = [];
  statuses = [];
  entity;
  classrooms = [];
  sub : Subscription;
  @ViewChildren(SelectControlComponent) selects: SelectControlComponent[];
  isUpdate = false;

  constructor(private fb: FormBuilder,
              private i18nService: I18nService,
              public route: ActivatedRoute,
              private router: Router,
              private resourcesService: ResourcesService,
              private resourceTypesService: ResourceTypesService,
              private unitiesService: UnitiesService,
              private statusesService: ResourceStatusesService,
              private snackbarService: SnackbarService,
              private navigationBackService: NavigationBackService,
              private cd: ChangeDetectorRef) {
    this.navigationBackService.urlBack = 'inventory/normal_resources';
    this.initForm();
    this.putResourceTypes();
    this.putUnities();
    this.putStatuses();
    this.sub = this.customForm.form.get('unity.id').valueChanges.subscribe(res => {
      !!res && this.unitiesService.getById(res).then(unity => {
        this.classrooms = unity.classrooms;
      });
    });
    this.validator = CustomFormUtils.addEditNoChangeValidator(this.customForm);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        type: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        status: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        unity: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        serial: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        activeCode: ['', Validators.compose([CustomValidators.requiredWithTrim])],
        internalNumber: ['', Validators.compose([CustomValidators.positiveNumber])],
        classroom: this.fb.group({
          id: ['']
        })
      })
      .handlerSaveResponse((response, action) => {
        CustomFormUtils.handlerSaveError(this.customForm, response, action, this.i18nService);
        if (!response || response < 400) {
          this.navigationBackService.goToBack();
        }
      })
      .prepareValue(value => {
        if(value.classroom && !value.classroom.id) delete value.classroom;
        return value;
      })
      .submiter(this.resourcesService)
      .build();
  }

  ngAfterViewInit(): void {
    const entity = this.route.snapshot.data.resource;
    const isEdit = this.router.url.includes('edit');
    setTimeout(() => {
      (!!entity && !!isEdit) && this.setEntity(entity);
    }, 500);
    this.cd.detectChanges();
  }

  back(){
    this.navigationBackService.goToBack();
  }

  setEntity(entity) {
    entity.classroom = entity.classroom ? entity.classroom : '';
    this.customForm.setValue(entity);
    this.isUpdate = true;
    this.validator.active();
  }

  private putResourceTypes() {
    this.resourceTypesService.getEnabledList().then(res => this.resourceTypes = res.filter(val => !val.extra));
  }

  private putUnities() {
    this.unitiesService.getEnabledList().then(res => this.unities = res);
  }

  private putStatuses() {
    this.statusesService.getEnabledList().then(res => this.statuses = res);
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }

  render(item){
    return `${item.internalCode}`;
  }
}

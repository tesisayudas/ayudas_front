import {Component, OnDestroy, OnInit} from '@angular/core';

import {ResourcesService} from "../resources.service";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {ActivatedRoute, Router} from "@angular/router";
import {map} from "rxjs/operators";
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'g3-normal-resources-list',
  template: `
    <g3-fab [routerLink]="'../create'"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)"
                     [itemList]="customList"
                     [totalItems]="totalItems"
                     [config]="tableConfig">
    </g3-table-custom>
  `,
  styles: []
})
export class NormalResourcesListComponent extends BasicItemListComponent<ResourcesService> implements OnInit, OnDestroy{

  subList: Subscription;
  customList = [];

  constructor(public service: ResourcesService,
              public paginator: PaginatorService,
              public router: Router,
              public route: ActivatedRoute) {
    super(service, paginator);
    this.goToEdit = (entity) => this.router.navigate([`../${entity.id}/edit`], {relativeTo: this.route});
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('resources.serial')
            .key('serial')
            .styleClass('mdl-data-table__cell--non-numeric')
            .showInMobile(true)
            .build()
        )
        .addColumn(
          new ColumnBuilder()
            .label('shared.type')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric')
            .showInMobile(true)
            .build()
        )
        .addColumn(
          new ColumnBuilder()
            .label('resources.status')
            .key('statusName')
            .styleClass('mdl-data-table__cell--non-numeric')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .hasCounter(false)
    .build();

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(null, null, null, null);
  }

  ngOnInit(): void {
    this.listenListChanges();
  }

  private listenListChanges() {
    this.subList = this.service.list$
      .pipe(
        map(res => res.items)
      ).subscribe(res => {
        this.customList = res.map(element => {
          element.name = element && element.type && element.type.name;
          element.statusName = element && element.status && element.status.name;
          return element;
        });
      })
  }

  ngOnDestroy(){
    this.subscription && this.subscription.unsubscribe();
    if (this.paginatorService) this.paginatorService.service = null;
    this.subList && this.subList.unsubscribe()
  }
}

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {ExtraResourcesService} from "../extra-resources.service";
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {ExtraResourcesFormComponent} from "../extra-resources-form/extra-resources-form.component";
import {Subscription} from "rxjs/Subscription";
import {map} from "rxjs/operators";

@Component({
  selector: 'g3-extra-resources-list',
  template: `
    <g3-fab (action)="openForm()"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)"
                     [itemList]="customList"
                     [totalItems]="totalItems"
                     [config]="tableConfig">
    </g3-table-custom>
    <g3-extra-resources-form></g3-extra-resources-form>
  `,
  styles: []
})
export class ExtraResourcesListComponent extends BasicItemListComponent<ExtraResourcesService> implements OnInit, OnDestroy {

  public customList: any[] = [];
  subList: Subscription;
  @ViewChild(ExtraResourcesFormComponent) form: ExtraResourcesFormComponent;

  constructor(public service: ExtraResourcesService,
              public paginator: PaginatorService) {
    super(service, paginator);
  }

  ngOnInit(): void {
    this.listenListChanges();
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.type')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric')
            .showInMobile(true)
            .build()
        )
        .addColumn(
          new ColumnBuilder()
            .label('extra_resources.total_quantity')
            .key('totalQuantity')
            .styleClass('mdl-data-table__cell--non-numeric')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .build();

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, null, null, null);
  }

  private listenListChanges() {
    this.subList = this.service.list$
      .pipe(
        map(res => res.items)
      ).subscribe(res => {
        this.customList = res.map(element => {
          element.name = element && element.resourceType && element.resourceType.name;
          return element;
        });
      })
  }

  ngOnDestroy(){
    this.subscription && this.subscription.unsubscribe();
    if (this.paginatorService) this.paginatorService.service = null;
    this.subList && this.subList.unsubscribe()
  }
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExtraResourcesListComponent} from "./extra-resources-list/extra-resources-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: ExtraResourcesListComponent,
    data: {
      titleKey: 'navigation.resources',
      resource: ACCESS_RESOURCES.resource
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtraResourcesRoutingModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtraResourcesRoutingModule } from './extra-resources-routing.module';
import { ExtraResourcesListComponent } from './extra-resources-list/extra-resources-list.component';
import { ExtraResourcesFormComponent } from './extra-resources-form/extra-resources-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CrudModule} from "@g3/crud/crud.module";
import {SoriModule} from "../../sori/sori.module";
import {ExtraResourcesService} from "./extra-resources.service";

@NgModule({
  imports: [
    CommonModule,
    ExtraResourcesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [ExtraResourcesListComponent, ExtraResourcesFormComponent],
  providers: [ExtraResourcesService]
})
export class ExtraResourcesModule { }

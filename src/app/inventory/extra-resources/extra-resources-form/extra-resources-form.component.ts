import {Component, OnInit} from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {FormBuilder, Validators} from "@angular/forms";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {ExtraResourcesService} from "../extra-resources.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {ResourceTypesService} from "../../../parametrization/resource-types/resource-types.service";

@Component({
  selector: 'g3-extra-resources-form',
  template: `
    <g3-dialog-form idDialog="extraResourceDialog" [form]="dialogForm" [validatorForm]="validator" idForm="requesterTypeForm">
      <form id="requesterTypeForm" [formGroup]="customForm.form">
        <!--totalQuantity-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input min="0" type="number" class="mdl-textfield__input" formControlName="quantity" id="quantity"/>
          <label class="mdl-textfield__label" for="quantity">{{'extra_resources.total_quantity' | i18n}}*</label>
        </div>

        <g3-select-control [formGroup]="customForm.form"
                           [results]="resourcesTypes"
                           [isRequired]="true"
                           [idControl]="'type.id'"
                           [labelName]="'extra_resources.resource_type'"></g3-select-control>
      </form>
    </g3-dialog-form>
  `,
  styles: []
})
export class ExtraResourcesFormComponent implements OnInit, ItemFormDialog {

  customForm: CustomForm;
  dialogForm: CustomFormDialog;
  validator: CustomFormValidator;
  resourcesTypes: any[] = [];

  constructor(private fb: FormBuilder,
              private service: ExtraResourcesService,
              private i18n: I18nService,
              private snack: SnackbarService,
              private resourceTypeService: ResourceTypesService) {
    this.initForm();
    this.initDialog();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  ngOnInit(): void {
    this.resourceTypeService.getEnabledList().then(res => {
      this.resourcesTypes = res.filter(val => val.extra);
    });
  }

  open() {
    this.dialogForm.open();
    const dialog = document.getElementById("extraResourceDialog");
    dialog.style.setProperty('overflow', 'visible');
  }

  setEntity(entity) {
    entity.type = entity.resourceType;
    entity.quantity = entity.totalQuantity;
    this.dialogForm.setValue(entity);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        type: this.fb.group({
          id: ['', Validators.compose([CustomValidators.requiredWithTrim])]
        }),
        quantity: ['', Validators.compose([CustomValidators.requiredWithTrim, CustomValidators.positiveNumber, CustomValidators.onlyInteger])]
      })
      .handlerSaveResponse((response, action)=> CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n, this.snack))
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }

  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('extra_resources.create')
      .updateTitleKey('extra_resources.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .build()
  }
}

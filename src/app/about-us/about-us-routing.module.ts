import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {AboutUsComponent} from "./about-us/about-us.component";

export const routes: Routes = [
  {
    path: '', component: AboutUsComponent, data: {titleKey: 'about_us_title'}
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AboutUsRoutingModule { }

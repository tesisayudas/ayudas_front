import {Component, ViewChild} from '@angular/core';
import {BasicItemListComponent, BasicItemListSetupData} from "@g3/crud/controller/item-list/basic-item-list.component";
import {PaginatorService} from "@g3/crud/model/item-crud-manager";
import {
  ColumnBuilder,
  GenericButton,
  GenericTableInputs,
  TableBuilder,
  TableColumnsBuilder,
  TableOptionsItemBuilder
} from "@g3/crud/view/table/generic-table";
import {MonitorService} from "../monitor.service";
import {MonitorFormComponent} from "../monitor-form/monitor-form.component";

@Component({
  selector: 'g3-monitor-list',
  template: `
    <g3-fab (action)="openForm()"></g3-fab>
    <g3-table-custom (onActionClick)="onActionClick($event)"
                     [itemList]="itemList"
                     [totalItems]="totalItems"
                     [config]="tableConfig">
    </g3-table-custom>
    <g3-monitor-form></g3-monitor-form>
  `,
  styles: []
})
export class MonitorListComponent extends BasicItemListComponent<MonitorService>{

  @ViewChild(MonitorFormComponent) form: MonitorFormComponent;

  constructor(public service: MonitorService, public paginator: PaginatorService) {
    super(service, paginator);
  }

  setUp(): BasicItemListSetupData {
    return new BasicItemListSetupData(this.form, null, null, null);
  }

  tableConfig: GenericTableInputs = new TableBuilder()
    .options(
      new TableOptionsItemBuilder()
        .add(GenericButton.edit())
        .build()
    )
    .columns(
      new TableColumnsBuilder()
        .addColumn(
          new ColumnBuilder()
            .label('shared.name')
            .key('name')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-8')
            .showInMobile(true)
            .build()
        )
        .addColumn(
          new ColumnBuilder()
            .label('monitors.user')
            .key('username')
            .styleClass('mdl-data-table__cell--non-numeric g3-flex-grow-8')
            .showInMobile(true)
            .build()
        )
        .build()
    )
    .build();

}

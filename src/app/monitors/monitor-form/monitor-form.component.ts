import {Component} from '@angular/core';
import {ItemFormDialog} from "@g3/crud/controller/controller-interfaces";
import {CustomDialogFormBuilder, CustomFormDialog} from "@g3/crud/controller/form/custom-dialog-form";
import {CustomForm, CustomFormBuilder} from "@g3/crud/controller/form/custom-form";
import {CustomFormValidator} from "@g3/crud/controller/form/custom-form-validator";
import {CustomFormUtils} from "@g3/crud/controller/form/custom-form-utils";
import {FormBuilder, Validators} from "@angular/forms";
import {CustomValidators} from "@g3/crud/controller/form/custom-validators";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {MonitorService} from "../monitor.service";

@Component({
  selector: 'g3-monitor-form',
  template: `
    <g3-dialog-form idDialog="requesterTypeDialog" [form]="dialogForm" [validatorForm]="validator" idForm="requesterTypeForm">
      <form id="requesterTypeForm" [formGroup]="customForm.form">
        <!--Name-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="name" id="name"/>
          <label class="mdl-textfield__label" for="name">{{'shared.name' | i18n}}*</label>
        </div>
        <!--Username-->
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" g3UpgradeElements>
          <input class="mdl-textfield__input" formControlName="username" id="username"/>
          <label class="mdl-textfield__label" for="name">{{'monitors.user' | i18n}}*</label>
        </div>
      </form>
    </g3-dialog-form>
  `,
  styles: []
})
export class MonitorFormComponent implements ItemFormDialog {

  dialogForm: CustomFormDialog;
  customForm: CustomForm;
  validator: CustomFormValidator;

  constructor(private fb: FormBuilder,
              private service: MonitorService,
              private i18n: I18nService,
              private snack: SnackbarService) {
    this.initForm();
    this.initDialog();
    this.validator = CustomFormUtils.addEditValidationToFormDialog(this.dialogForm).customFormValidator;
  }

  open() {
    this.dialogForm.open();
  }

  setEntity(entity) {
    this.dialogForm.setValue(entity);
  }

  private initForm() {
    this.customForm = new CustomFormBuilder(this.fb)
      .formStructure({
        id: [''],
        name: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])],
        username: ['', Validators.compose([CustomValidators.requiredWithTrim, Validators.maxLength(254)])]
      })
      .handlerSaveResponse((response, action)=> CustomFormUtils.handlerSaveError(this.dialogForm, response, action, this.i18n, this.snack))
      .prepareValue(value => value)
      .submiter(this.service)
      .build();
  }

  private initDialog() {
    this.dialogForm = new CustomDialogFormBuilder()
      .createTitleKey('monitors.create')
      .updateTitleKey('monitors.update')
      .customForm(this.customForm)
      .translator(this.i18n)
      .build()
  }

}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MonitorListComponent} from "./monitor-list/monitor-list.component";
import {ACCESS_RESOURCES} from "@g3/core/auth/permissions";

const routes: Routes = [
  {
    path: '',
    component: MonitorListComponent,
    data: {
      titleKey: 'navigation.monitors',
      resource: ACCESS_RESOURCES.resource
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitorsRoutingModule { }

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MonitorsRoutingModule} from './monitors-routing.module';
import {MonitorService} from "./monitor.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SoriModule} from "../sori/sori.module";
import {CrudModule} from "@g3/crud/crud.module";
import {MonitorListComponent} from "./monitor-list/monitor-list.component";
import {MonitorFormComponent} from "./monitor-form/monitor-form.component";

@NgModule({
  imports: [
    CommonModule,
    MonitorsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SoriModule,
    CrudModule
  ],
  declarations: [MonitorListComponent, MonitorFormComponent],
  providers: [MonitorService]
})
export class MonitorsModule { }

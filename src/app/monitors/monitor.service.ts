import { Injectable } from '@angular/core';
import {BackItemCrudManager} from "@g3/crud/model/item-crud-manager";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {URLS} from "../sori/elements";

@Injectable()
export class MonitorService extends BackItemCrudManager{

  constructor(private http: HttpClient, private router: Router) {
    super(http, URLS.monitors, router);
  }

}

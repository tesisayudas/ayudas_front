import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ConverterFinalString} from "@g3/crud/controller/converter-final-string";

@Component({
  selector: 'g3-select-dialog',
  templateUrl: './select-dialog.component.html',
  styleUrls: ['./select-dialog.component.css']
})
export class SelectDialogComponent implements OnInit, OnChanges {

  private static readonly defaultDialogId = "select-dialog-id";

  @Input() idDialog = SelectDialogComponent.defaultDialogId;
  @Input() titleKey;
  @Input('title') literalTitle;
  @Input() okBtnNameKey = 'shared.accept';
  @Input() cancelBtnNameKey = 'shared.cancel';
  @Output() selectAction = new EventEmitter();
  @Input() options: any[];
  @Input() display: string[] = ['name'];
  @Input() toStringCustom: (element: any) => string;

  finalTitle;

  okBtnText = 'shared.accept';
  cancelBtnText = 'shared.cancel';
  private converterFinalString: ConverterFinalString;
  selected;

  constructor(private i18n: I18nService) {
    this.i18n.translator$.subscribe(() => this.update());
    this.converterFinalString = new ConverterFinalString().priority('key').translator(this.i18n);

  }

  ngOnInit() {
  }

  update() {
    this.finalTitle = this.converterFinalString.key(this.titleKey).literal(this.literalTitle).getFinalString();
    this.okBtnText = this.i18n.getValue(this.okBtnNameKey);
    this.cancelBtnText = this.i18n.getValue(this.cancelBtnNameKey);
    this.selected = null;
  }

  _selectAction(){
    if(this.selected){
      this.selectAction.next({
        selected: this.selected
      });
    }
  }

  ngOnChanges() {
    this.update();
  }

  select(option) {
    this.selected = option;
  }
}

import {Component, Input} from '@angular/core';

@Component({
  selector: 'g3-disabled-chip',
  templateUrl: './disabled-chip.component.html',
  styleUrls: ['./disabled-chip.component.css']
})
export class DisabledChipComponent {

  @Input() disabled:boolean;
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PermissionService} from "@g3/core/auth/permission.service";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute} from "@angular/router";
import {AVAILABLE_SCOPES} from "@g3/core/auth/permissions";

@Component({
  selector: 'g3-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.css']
})
export class FabComponent implements OnInit {

  /**
   * attribute that represents the emitter event
   * @item {EventEmitter<any>}
   */
  @Output() action ?= new EventEmitter<any>();

  /**
   * attribute that represents the Id
   */
  @Input() id ?: any;

  public canShow$: Observable<boolean>;

  constructor(private permissionService: PermissionService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.canShow$ = this.permissionService.hasPermission(this.route, AVAILABLE_SCOPES.create);
  }

  /**
   * Method that emmit the event
   * @param event
   */
  fabAction(event){
    this.action.next({
      event: event
    });
  }
}


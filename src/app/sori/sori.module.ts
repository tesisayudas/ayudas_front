import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FabComponent } from './fab/fab.component';
import {SelectDialogComponent} from "./select-dialog/select-dialog.component";
import {SharedModule} from "../shared/shared.module";
import {DisabledChipComponent} from "./chips/disabled-chip/disabled-chip.component";

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [FabComponent, SelectDialogComponent, DisabledChipComponent],
  exports: [FabComponent, SelectDialogComponent, DisabledChipComponent]
})
export class SoriModule { }

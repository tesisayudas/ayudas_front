import {environment} from "../../environments/environment";

export const URLS = {
  resource_types: {route:'resource-types' , back: environment.ayudas.backBaseUrl},
  requester_types: {route: 'requester-types', back: environment.ayudas.backBaseUrl},
  unities: {route: 'unities', back: environment.ayudas.backBaseUrl},
  resources: {route: 'resources', back: environment.ayudas.backBaseUrl},
  extra_resources: {route: 'resources/extra', back: environment.ayudas.backBaseUrl},
  resource_statuses : {route: 'resource-statuses', back: environment.ayudas.backBaseUrl},
  request_reservation: {route: 'reservation-repeats', back: environment.ayudas.backBaseUrl},
  reservation: {route: 'reservations', back: environment.ayudas.backBaseUrl},
  reservation_types: {route: 'reservation-types', back: environment.ayudas.backBaseUrl},
  reservation_statuses: {route: 'reservation-statuses', back: environment.ayudas.backBaseUrl},
  reports: {route: 'reports', back: environment.ayudas.backBaseUrl},
  monitors: {route: 'monitors', back: environment.ayudas.backBaseUrl}
};

export const BREAKPOINT = {
  desktop: 840,
  tablet: 480
};

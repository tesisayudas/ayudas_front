export {}

declare global {
  interface String {
    capitalizeFirstLetter(): string;
    capitalizeWords(): string;
  }

  interface MaterialMenu {

  }
}

/**
 * Method that transform to upper case the first character of a string
 * @returns {string} the original string with the first character upper cased
 */
String.prototype.capitalizeFirstLetter = function (this: string) {
  if (this) {
    return this.charAt(0).toUpperCase() + this.substr(1, this.length).toLowerCase();
  }
  return '';
};

/**
 * Method that capitalize first character of each word separated by a whitespace in a string
 * @returns {string} the original string with the first character of each word upper cased
 */
String.prototype.capitalizeWords = function (this: string) {
  if(this) {
    let words = this.split(' ');
    return words.reduce((accumulator, currentWord) => {
      return accumulator + ' ' + currentWord.capitalizeFirstLetter();
    }, '');
  } else {
    return '';
  }
};

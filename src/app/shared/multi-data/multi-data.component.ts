import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'g3-multi-data',
  templateUrl: './multi-data.component.html',
})
export class MultiDataComponent implements OnInit {

  @Input() formGroup: FormGroup;
  @Input() idControl: string;
  @Input() labelName: string;
  @Input() isRequired: boolean;
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['']
    });
  }

  get control() { return this.formGroup.get(this.idControl) as FormArray;}

  save(val?: string) {
    const element = val ? val : this.form.get('name').value;
    if (this.control.value.findIndex(v => v.internalCode == element) == -1) {
      const newControl = new FormGroup({internalCode: new FormControl()});
      newControl.get('internalCode').setValue(element);
      this.control.push(newControl);
      this.form.reset();
    }
  }

  removeElement(i){
    this.control.removeAt(i);
  }

  patchValue(values:any[]){
    values.forEach(val => this.save(val.internalCode));
  }

  clear(){
    while(this.control.value.length > 0){
      this.control.removeAt(0);
    }
  }
}

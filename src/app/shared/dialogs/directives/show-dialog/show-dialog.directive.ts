import {Directive, Input, HostListener} from '@angular/core';
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";

/**
 * Show a dialog component by id of the dialog tag.
 */
@Directive({
  selector: '[g3ShowDialog]'
})
export class ShowDialogDirective {

  /**
   * The id of dialog tag.
   */
  idDialog;

  /**
   * Set the id of the dialog tag.
   * @param {string} idDialog - The id of dialog tag.
   */
  @Input()
  public set g3ShowDialog(idDialog: string) {
    this.idDialog = idDialog;
  }

  /**
   * Create a {@link ShowDialogDirective}.
   * @param {DialogService} dialog - The manager of the dialog components.
   */
  constructor(private dialog: DialogService) {
  }

  /**
   * Show a dialog component by id of the dialog tag.
   */
  @HostListener("click")
  onclick() {
    this.dialog.showDialogComponent(this.idDialog);
  }

}

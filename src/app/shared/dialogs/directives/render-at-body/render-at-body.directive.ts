import {Directive, TemplateRef, ViewContainerRef, Input, OnInit} from '@angular/core';
import {DialogsTemplatesToRenderService} from "@g3/core/dialogs/dialogs-templates-to-render/render-dialog.service";

/**
 * Render the dialog component at body.
 */
@Directive({
  selector: '[g3RenderAtBody]'
})
export class RenderAtBodyDirective implements OnInit {

  /**
   * The id of dialog tag.
   */
  idDialog;

  /**
   * Set the id of dialog tag.
   * @param {string} idDialog - The id of dialog tag.
   */
  @Input()
  public set g3RenderAtBody(idDialog: string) {
    this.idDialog = idDialog;
  }

  /**
   * Create a RenderAtBodyDirective.
   * @param {DialogsTemplatesToRenderService} dialogTemplatesToRender - The service that contain the dialogs component rendered at body.
   * @param {TemplateRef<Object>} template - The template of dialog component.
   * @param {ViewContainerRef} container - The container of the dialog component.
   */
  constructor(private dialogTemplatesToRender: DialogsTemplatesToRenderService, private template: TemplateRef<Object>, private container: ViewContainerRef) {
    this.container.clear();
  }

  /**
   * Add the dialog component at {@link DialogsTemplatesToRenderService}.
   */
  ngOnInit() {
    this.dialogTemplatesToRender.addDialogTemplate(this.idDialog, this.template);
  }

}

import {Directive, ElementRef} from '@angular/core';

/**
 * Variable of the polyfill to register the dialogs tag elements.
 */
declare var dialogPolyfill: any;

/**
 * Represent the polyfill for the dialogs tags, it allow that the dialogs work perfectly in all browsers.
 */
@Directive({
  selector: '[g3DialogPolyfill]'
})
export class DialogPolyfillDirective {

  /**
   * Create a polyfill for a dialog tag.
   * @param {ElementRef} element - The dialog tag element.
   */
  constructor(private element: ElementRef) {
    const dialogElement = this.element.nativeElement;
    !dialogElement.showModal && dialogPolyfill.registerDialog(dialogElement);
  }

}

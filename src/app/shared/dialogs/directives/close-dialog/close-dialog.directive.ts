import {Directive, HostListener, Input} from '@angular/core';
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";

/**
 * Close a dialog component by the id of its dialog tag.
 */
@Directive({
  selector: '[g3CloseDialog]'
})
export class CloseDialogDirective {

  /**
   * The id of the dialog tag.
   */
  idDialog;

  /**
   * Set the id of the dialog tag to close.
   * @param {string} idDialog - The id of dialog tag.
   */
  @Input()
  public set g3CloseDialog(idDialog: string) {
    this.idDialog = idDialog;
  };

  /**
   * Create a new CloseDialogDirective.
   * @param {DialogService} dialog - The manager of dialogs components.
   */
  constructor(private dialog: DialogService) {
  }

  /**
   * Close the dialog component at do click in the element that have this directive.
   */
  @HostListener("click")
  onClick() {
    this.dialog.closeDialogComponent(this.idDialog);
  }

}

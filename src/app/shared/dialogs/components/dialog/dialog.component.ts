import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {BodyContainerService} from "@g3/core/dialogs/body-container/body-container.service";

export const SMALL = 'small';
export const MEDIUM = 'medium';
export const LARGE = 'large';
/**
 * Represent a dialog (modal).
 */
@Component({
  selector: 'g3-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DialogComponent implements OnDestroy, OnInit {

  public static readonly default_dialog_id = 'default-dialog-id';

  /**
   * The id of dialog tag.
   * @item {string}
   */
  @Input() idDialog = DialogComponent.default_dialog_id;

  @Input('size') size;
  classSize;
  /**
   * It says if it has been clicked the dialog component.
   * @item {boolean}
   */
  clickHere = false;

  /**
   * Says if is open or close.
   * @item {EventEmitter<boolean>}
   */
  @Output() status = new EventEmitter<string>();

  /**
   * Create a new dialog component.
   * @param {DialogService} dialog - The dialog service to manage of the new dialog component.
   * @param {BodyContainerService} bodyContainer - The body container to remove the dialog component when it is destroyed.
   */
  constructor(private dialog: DialogService, private bodyContainer: BodyContainerService) {
    this.dialog.newDialogComponent(this);
  }

  /**
   * It says the it has been clicked over this dialog component.
   */
  clickOnDialog() {
    this.clickHere = true;
  }

  /**
   * Set the status to [closed]{@link DialogComponent#closed}.
   */
  ngOnInit() {
    this.changeStatus("closed");
    this.classSize = this.getClass();
  }

  /**
   * Remove of body container and manager of dialogs this dialog component.
   */
  ngOnDestroy() {
    this.bodyContainer.removeDialogById(this.idDialog);
    this.dialog.removeDialogComponent(this);
  }

  /**
   * Change the status (open/close).
   * @param {boolean} status - The new status.
   */
  changeStatus(status: string) {
    this.status.next(status);
  }

  /**
   * Open the dialog
   */
  open() {
    this.dialog.showDialogComponent(this.idDialog);
  }

  /**
   * Close the dialog
   */
  close() {
    this.dialog.closeDialogComponent(this.idDialog);
  }

  private getClass(): string {
    switch (this.size) {
      case SMALL:
        return "g3-dialog--" + SMALL;
      case MEDIUM:
        return "g3-dialog--" + MEDIUM;
      case LARGE:
        return "g3-dialog--" + LARGE;
      default:
        return "g3-dialog--" + SMALL;
    }
  }
}

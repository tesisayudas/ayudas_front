import {Component, ElementRef, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {I18nService} from "app/core/i18n/i18n.service";
import {combineLatest} from 'rxjs/observable/combineLatest'

@Component({
  selector: 'g3-multi-file-form',
  templateUrl: './multi-file-form.component.html',
})
export class MultiFileFormComponent implements OnInit {

  @Input() textField: string = 'shared.uploadFiles';
  @Input() validFormats: string[] = ['pdf', 'jpg', 'jpeg', 'iso', 'png'];
  @Input() required: boolean;
  @ViewChildren('fileUploader') filesUploader: QueryList<ElementRef>;

  loadingFile$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  files: File[] = [];
  noLoadedFiles: string[] = [];
  messageField: string;
  tooltipMessage: string;

  constructor(private i18nService: I18nService) {
    this.messageField = this.i18nService.getValue(this.textField);
    this.tooltipMessage = this.i18nService.getValue('shared.name');
  }

  ngOnInit() {
    this.loadingFile$.subscribe((val) => {
    });
  }

  public clearHTMLFiles(){
    this.filesUploader.forEach((ref:ElementRef) =>ref.nativeElement.value = null);
  }

  removeFile(event, index){
    this.files.splice(index, 1);
    this.clearHTMLFiles()
  }

  isValidExt(nameFile: string) {
    const availableTypes: string[] = this.validFormats;
    const extension = nameFile.split('.').pop().toLowerCase();
    return availableTypes.indexOf(extension) > -1;
  }

  onFileChange(event){
    if (event.target.files && event.target.files.length) {
      const files = event.target.files;
      this.loadFiles(files);
    }
  }

  loadFiles(files: any[]){
    this.noLoadedFiles = [];
    this.loadingFile$.next(true);
    let reader: FileReader[] = [];
    let index = 0;
    const listListeners: BehaviorSubject<any>[] = [];
    for(let file of files){
      const listener = new BehaviorSubject<any>(false);
      listListeners.push(listener);
      if(this.isValidExt(file.name)) {
        reader[index]= new FileReader();
        reader[index].readAsDataURL(file);
        reader[index].onloadend = () => {
          listener.next(true);
          if (this.files.findIndex(_file => _file.name === file.name) === -1){
            this.files.push(file);
          }
        };

      } else {
        listener.next(true);
        this.noLoadedFiles.push(file.name)
      }
    }
    combineLatest(listListeners).subscribe(res => {
      if(res.findIndex(val => val === false) === -1) this.loadingFile$.next(false)
    })
  }

  getError(){
    return this.i18nService.getValue('errors.noLoadedFile')
  }

}

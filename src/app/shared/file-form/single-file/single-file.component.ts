import {Component, ElementRef, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {upgradeElement} from "../../upgrades/upgrader";

@Component({
  selector: 'g3-single-file',
  templateUrl: './single-file.component.html',
})
export class SingleFileComponent implements OnInit {

  @Input() validFormats: string[] = ['pdf', 'jpg', 'jpeg', 'iso', 'png'];
  @Input() required: boolean = false;
  @Input() form: FormGroup;
  @Input() isUpdate: boolean = false;
  @ViewChildren('fileInput') filesInputs: QueryList<ElementRef>;
  file: File;
  loadingFile$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  ngOnInit() {
    this.form.addControl('documentUrl', new FormControl('', Validators.compose([ this.required ? Validators.required : null])));
  }

  removeFile() {
    this.file = null;
    this.form.get('documentUrl').patchValue(null);
    if (this.filesInputs) this.filesInputs.forEach(element => element.nativeElement.value = null);

  }

  isValidExt(nameFile: string) {
    const availableTypes: string[] = this.validFormats;
    const extension = nameFile.split('.').pop().toLowerCase();
    return availableTypes.indexOf(extension) > -1;
  }

  onFileChange(event) {
    this.loadingFile$.next(true);
    let reader: FileReader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      this.form.get('documentUrl').markAsDirty();
      this.form.get('documentUrl').patchValue(file.name);
      if (this.isValidExt(file.name)) {
        this.file = file;
        upgradeElement(document.getElementById('uploadFile'));
        reader.onloadend = () => {
          this.loadingFile$.next(false);
        };
      } else {
        this.loadingFile$.next(true);
        this.form.get('documentUrl').setErrors({noValidExt: true})
      }
    }
  }
}

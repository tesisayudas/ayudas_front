export const DOT_COLORS = {
  yellow: "mdl-color-text--yellow-600",
  gray: "gray",
  orange: "mdl-color-text--yellow-900",
  red: "mdl-color-text--red-300",
};

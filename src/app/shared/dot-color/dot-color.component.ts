import {Component, Input} from '@angular/core';

@Component({
  selector: 'g3-dot-color',
  template: `
    <span [id]="id" class="icon">
      <i class="material-icons mdi mdi-checkbox-blank-circle g3-simple-dot {{color}}"></i>
    </span>
    <span g3Upgrade
          class="mdl-tooltip mdl-tooltip--right"
          [attr.for]="id">
      {{ msg }}
    </span>

  `,
  styleUrls: ['./dot-color.component.scss']
})
export class DotColorComponent {
  @Input() color: string;
  @Input() id: string;
  @Input() msg: string;
}

import {Component, Input, OnInit} from '@angular/core';
import {DOT_COLORS} from "../dot-color/dot.colors";
import * as moment from 'moment';
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";
import {I18nService} from "@g3/core/i18n/i18n.service";

@Component({
  selector: 'g3-semaphore',
  templateUrl: './semaphore.component.html',
  styleUrls: ['./semaphore.component.scss']
})
export class SemaphoreComponent implements OnInit {
  @Input() endDate;
  @Input() expectedEndDate;
  @Input() id: string;
  @Input() range: [number, number, number] = [1, 5, 15];
  public tooltipMessage: string;
  public color: string;
  public days: string;
  public amountDays: number;
  constructor(private i18n: I18nService) {
  }

  ngOnInit() {
    this.getColor();
  }

  getColor(): void {
    const expectedEnd = moment(this.expectedEndDate, BASIC_DATE_FORMAT);
    const difference = expectedEnd.diff(moment(), 'days') + 1;
    if (this.endDate) {
      this.tooltipMessage = 'semaphore.close';
      this.color = DOT_COLORS.gray;
      return
    } else if (difference < this.range[0]) {
      this.color = DOT_COLORS.red;
      this.tooltipMessage = 'semaphore.last_day';
    } else if (difference < this.range[1]) {
      this.color = DOT_COLORS.orange;
      this.tooltipMessage = 'semaphore.missing_days';
      this.days = 'semaphore.days';
      this.amountDays = difference;
    } else if (difference < this.range[2]) {
      this.color = DOT_COLORS.yellow;
      this.tooltipMessage = 'semaphore.missing_days';
      this.days = 'semaphore.days';
      this.amountDays = difference;
    } else {
      this.tooltipMessage = 'semaphore.open';
    }
  }

  public getMsg(): string {
    let msg: string = this.i18n.getValue(this.tooltipMessage);
    if (this.days && this.amountDays) {
      msg += this.amountDays;
      msg += this.i18n.getValue(this.days);
    }
    return msg
  }
}

import {Component, ContentChild, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'g3-responsive-wrapper',
  templateUrl: './responsive-wraper.component.html',
  styleUrls: ['./responsive-wraper.component.css']
})
export class ResponsiveWrapperComponent implements OnInit {

  isLarge: number;
  @ContentChild("mobile") mobileView;
  @ContentChild("tablet") tabletView;
  @ContentChild("desktop") desktopView;
  public desktop: number = 1;
  public tablet: number = 2;
  public mobile: number = 3;

  constructor() {
    this.resize();
  }

  ngOnInit() {

  }

  @HostListener('window:resize')
  resize() {
    if (window.innerWidth >= 840) {
      this.isLarge = this.desktop;
    } else if (window.innerWidth >= 480 && window.innerWidth <= 839) {
      this.isLarge = this.tablet;
    } else {
      this.isLarge = this.mobile;
    }
  }

}

import {animate, state, style, transition, trigger} from "@angular/animations";

export const shrinkOut = trigger('shrinkOut', [
  state('in', style({})),
  transition('* => void', [
    style({height: '!', opacity: 1}),
    animate("350ms ease-in-out", style({height: 0, opacity: 0}))
  ]),
  transition('void => *', [
    style({height: 0, opacity: 0}),
    animate("350ms ease-in-out", style({height: '*', opacity: 1}))
  ])
]);

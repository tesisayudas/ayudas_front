import {Component, Input} from '@angular/core';
import {LoadingComponent} from '../loading/loading.component';

/**
 * Component of loading with spinner.
 */
@Component({
  selector: 'g3-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent extends LoadingComponent {

  /**
   * The state of multicolor for the spinner.
   * @item {boolean}
   */
  multiColor = true;

  /**
   * Set if use or not the single color fo the spinner.
   * @param {boolean} value
   */
  @Input()
  public set singleColor(value: boolean) {
    this.multiColor = value;
  }

  /**
   * Give the class of the spinner depend of the multicolor state.
   * @returns {string}
   */
  getClass() {
    return "mdl-spinner mdl-js-spinner is-active " + (!this.multiColor ? "mdl-spinner--single-color" : "");
  }

}

import {Component, HostListener, ViewChild} from '@angular/core';
import {LoadingComponent} from '../loading/loading.component';

/**
 * Component of loading with progress bar.
 */
@Component({
  selector: 'g3-progress-bar-indeterminate',
  templateUrl: './progress-bar-indeterminate.component.html',
  styleUrls: ['./progress-bar-indeterminate.component.css']
})
export class ProgressBarIndeterminateComponent extends LoadingComponent {

}

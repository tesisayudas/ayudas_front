import {AfterViewInit, Component, Input, ViewChild} from '@angular/core';

/**
 * Object of MDL to generate MDL elements dynamically.
 */
declare var componentHandler: any;

/**
 * Represent the component of loading.
 */
@Component({
  selector: 'g3-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements AfterViewInit {

  /**
   * The state of the loading.
   * @item {boolean}
   */
  @Input() loading = true;

  /**
   * The element view of loading.
   */
  @ViewChild('loadingElement') loadingElement;

  /**
   * If exist the element view of loading it does a upgrade, so the MDL class works correctly.
   */
  ngAfterViewInit() {
    this.loadingElement && componentHandler.upgradeElement(this.loadingElement.nativeElement);
  }

}

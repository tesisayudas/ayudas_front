import {Directive, HostListener, Input} from '@angular/core';
import {SnackbarService} from '../../core/snackbar/snackbar.service';

/**
 * Directive to show the toast component.
 */
@Directive({
  selector: '[g3Toast]'
})
export class ToastDirective {

  /**
   * The data that contain the toast component.
   * @item {{message: string}}
   * @private
   */
  private _data: any = {message: "Default message!"};

  /**
   * Set the data that contain the toast component.
   * @param data - A string or object with a property message.
   */
  @Input('g3Toast')
  public set data(data: any) {
    this._data = SnackbarService.structureData(data);
  }

  /**
   * Create a new {@link ToastDirective}.
   * @param {SnackbarService} snackbar - The that contain the logic to structure and show the toast component.
   */
  constructor(private snackbar: SnackbarService) {
  }

  /**
   * Handle the click event, it shows the toast component.
   */
  @HostListener("click")
  onclick() {
    this.snackbar.showToast(this._data);
  }

}

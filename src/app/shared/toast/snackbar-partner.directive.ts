import {AfterViewInit, Directive, ElementRef, OnDestroy} from '@angular/core';
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {SnackbarPartner} from "./snackbar-partner";

@Directive({
  selector: '[g3SnackbarPartner]'
})
export class SnackbarPartnerDirective implements AfterViewInit, OnDestroy, SnackbarPartner {

  constructor(private element: ElementRef, private snackbar: SnackbarService) {
  }

  ngAfterViewInit() {
    this.snackbar.addPartner(this);
  }

  ngOnDestroy() {
    this.snackbar.removePartner(this);
  }

  update(snackbar: any, data: any): void {
    if(window.innerWidth <= 479) {
      this.element.nativeElement.classList.add("mdl-snackbar--active");
      this.element.nativeElement.style.transform = `translate(0px, -${snackbar.offsetHeight}px)`;
      setTimeout(() => {
        this.element.nativeElement.style.transform = "translate(0px, 0px)";
      }, data.timeout);
    }
  }

}

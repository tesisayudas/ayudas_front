import {AfterViewInit, Directive, ElementRef, Host, Injector, Input, Optional} from '@angular/core';
import {FormControl, FormGroupDirective, FormGroupName} from "@angular/forms";
import {findParent, G3FormControl} from "../form-control";
import {upgradeSelects} from "../../upgrades/upgrader";
import {generateIdWithPrefix} from "@g3/crud/controller/generator-id-with-prefix";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ControlValidator} from "../form-control-name/form-control-name.directive";
import {distinctUntilChanged} from "rxjs/operators";

/**
 * Integrator between getmdl-select and reactive forms.
 */
@Directive({
  selector: '[g3FormControlSelect]'
})
export class FormControlSelectDirective extends G3FormControl implements AfterViewInit/*, DoCheck*/ {
  constructor(
    public _element: ElementRef,
    public injector: Injector,
    @Host() public form: FormGroupDirective,
    @Optional() @Host() public formGroup: FormGroupName
  ){ super() }
  /**
   * The name of the form control.
   */
  @Input() g3FormControlSelect;

  /**
   * The element with the class getmdl-select (the select).
   */
  parentGetMdlSelect;

  /**
   * The cached value of the select (reactive form value).
   */
  cachedValue;

  /**
   * yhe objects in the menu
   */
  cachedLis = [];

  selectedLi;

  idSelect;

  /**
   * Give the parent element with the class getmdl-select (give the select).
   * @param element - The child element
   * @returns {any} - The select or (undefined | null)
   */
  getParentGetMdlSelect(element: any): any {
    return findParent(element, (element) => element.classList.contains("getmdl-select"));
  }

  /**
   * Give the form control.
   * @returns {FormControl}
   */
  public get control(): FormControl | null {
    return super.getControl(this.g3FormControlSelect);
  }

  /**
   * Register the manager of behavior for the select (select -> form reactive).
   */
  ngAfterViewInit() {

    //Find the parent (getmdl-select) of the element
    this.element.nativeElement.autocomplete = "off";
    this.parentGetMdlSelect = this.getParentGetMdlSelect(this.element.nativeElement);

    if (this.parentGetMdlSelect) {
      this.idSelect = this.parentGetMdlSelect.id;
      if(!this.idSelect) {
        this.idSelect = generateIdWithPrefix("getmdl-select-");
        this.parentGetMdlSelect.id = this.idSelect;
      }

      this.cachedLisNoDynamic();

      this.addListenerToCurrentLis();

      this.upgradeGetmdlSelect();

      new MutationObserver((mutation) => {
        this.updateCachedLis(mutation);
        if(this.cachedLis) {
          this.addListenerToCurrentLis();
        }
      }).observe(this.parentGetMdlSelect, {
        childList: true,
        subtree: true
      });

      if(this.control.value) {
        this.check();
      }
    }

    this.control.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      this.check();
    });

    new ControlValidator(this.control, this.parentGetMdlSelect, this.injector.get(I18nService));
  }

  /**
   * Add the elements in the menu ( UL TAG) before that change
   */
  cachedLisNoDynamic() {
    [].forEach.call(this.parentGetMdlSelect.children, (child) => {
      if (child.tagName == 'UL') {
        this.cachedLis = child.children;
      }
    });
  }

  /**
   * update the cached list when DOM change
   */
  updateCachedLis(mutation) {
    [].forEach.call(mutation, (nodeList) => {
      const nativeElement = nodeList.addedNodes[0];
      if(nativeElement && nativeElement.tagName == 'UL') {
        this.cachedLis = nativeElement.children;
      }
    });
  }

  /**
   * Update the Select
   */
  upgradeGetmdlSelect() {
    upgradeSelects('#'+this.idSelect);
  }

  /**
   * add the Listener event to the li in UL
   */
  addListenerToCurrentLis() {
    if(this.control instanceof FormControl) {
      [].forEach.call(this.cachedLis, (li) => {
        if (li.tagName == 'LI') {
          this.addListenerToLi(li);
        }
      });
    }
  }

  /**
   * Add the listener event to LI
   */
  addListenerToLi(li) {
    if(!li.dataset.listener) {
      li.dataset.listener = true;
      li.addEventListener("click", () => {
        //when clicked set the formControl value to the val of the li
          this.control.setValue(li.dataset.val || '');
          this.control.markAsDirty();
      });
    }
  }

  /**
   * Manage the behavior for the select (form reactive -> select).
   */
  check() {
    if (this.control && this.control.value !== this.cachedValue) {
      this.cachedValue = this.control.value;
      this.update();
    }
  }

  clearSelectedLi() {
    if(this.selectedLi) {
      delete this.selectedLi.dataset.selected;
      this.selectedLi.classList.remove('selected');
      this.selectedLi = null;
    }
  }

  updateHeight() {
    setTimeout(() => {
      let element = this.getParentGetMdlSelect(this.element.nativeElement).querySelector(".mdl-js-menu");
      let container = this.getParentGetMdlSelect(this.element.nativeElement).querySelector(".mdl-menu__container");
      if (container.classList.contains('is-visible')) {
        element['MaterialMenu'].show();
      }
    });
  }

  update() {

    if(this.control instanceof FormControl){
      if(this.cachedLis){
        [].forEach.call(this.cachedLis, (li) => {
          if (li.dataset.val == this.cachedValue) {
            this.clearSelectedLi();
            li.dataset.selected=true;
            this.selectedLi = li;
          }
        });
    }

      if (!this.cachedValue) {
        this.element.nativeElement.value = "";
        this.clearSelectedLi();
      }

      this.upgradeGetmdlSelect();
      this.parentGetMdlSelect.MaterialTextfield.updateClasses_();
    }
  }

}

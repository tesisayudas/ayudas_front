import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {ItemSearchableModel} from "@g3/crud/model/model-interfaces";

@Component({
  selector: 'g3-select-control',
  templateUrl: './select-control.component.html',
  styles: ['.mdl-textfield{width: 100%}']
})
export class SelectControlComponent implements OnInit {

  @Input() service: ItemSearchableModel;
  @Input() results: { items: any[] } | any[];
  @Input() formGroup: FormGroup;
  @Input() idControl: string;
  @Input() labelName: string;
  @Input() render: (item) => string = (item) => item.name;
  @Input() property: string = 'id';
  @Input() titleBox: string;
  @Input() isRequired: boolean = false;
  isMulti: boolean = false;

  constructor() {
  }

  ngOnInit() {
    if (this.control instanceof FormArray) {
      this.isMulti = true;
    }
  }

  removeElement(i) {
    this.control.removeAt(i);
  }

  addElement(element) {
    if (this.control.value.findIndex(v => v.id == element.id) == -1) {
      const newControl = new FormGroup({id: new FormControl(), name: new FormControl()});
      newControl.get('id').setValue(element.id);
      newControl.get('name').setValue(element.name);
      this.control.push(newControl);
    }
  }

  get control() { return this.formGroup.get(this.idControl) as FormArray;}

  clear(){
    while(this.control.value.length > 0){
      this.control.removeAt(0);
    }
  }

  patchValue(values: {id, name}[]) {
    values.forEach(val => this.addElement(val));
  }

  clearData(){
    this.formGroup.get(this.idControl).reset();
  }
}

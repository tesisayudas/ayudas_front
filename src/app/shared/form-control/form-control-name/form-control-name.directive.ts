import {AfterViewInit, Directive, ElementRef, Host, Injector, Input, Optional} from '@angular/core';
import {G3FormControl} from "../form-control";
import {upgradeElement} from "../../upgrades/upgrader";
import {FormControl, FormGroupDirective, FormGroupName} from "@angular/forms";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {getValidationErrorMessage} from "@g3/crud/controller/form/custom-validators";

@Directive({
  selector: '[formControlName]'
})
export class FormControlNameDirective extends G3FormControl implements AfterViewInit {

  /**
   * The name of the control in the formGroup.
   */
  @Input() formControlName;

  parentMdlJs;

  control: FormControl;

  constructor(
    public _element: ElementRef,
    public injector: Injector,
    @Host() public form: FormGroupDirective,
    @Optional() @Host() public formGroup: FormGroupName
  ){ super() }

  options: [{ clazz: string, update: (value) => void }] = [
    {
      clazz: "mdl-js-textfield",
      update: (value) => {
        this.parentMdlJs.MaterialTextfield && this.parentMdlJs.MaterialTextfield.checkDirty() &&  upgradeElement(this.element);
      }
    },
    {
      clazz: "mdl-js-checkbox",
      update: (value) => {
        if (this.parentMdlJs.MaterialCheckbox) {
          if (value) {
            this.parentMdlJs.MaterialCheckbox.check();
          } else {
            this.parentMdlJs.MaterialCheckbox.uncheck();
          }
        }
      }
    },
    {
      clazz: "mdl-js-radio",
      update: (value) => {
        if (this.parentMdlJs.MaterialRadio) {
          if (value && this.element.nativeElement.value == value) {
            this.parentMdlJs.MaterialRadio.check();
          } else {
            this.parentMdlJs.MaterialRadio.uncheck();
          }
        }
      }
    }
  ];

  ngAfterViewInit() {
    this.parentMdlJs = this.element.nativeElement.parentElement;
    this.control = this.getControl(this.formControlName);
    if (this.parentMdlJs) {
      let clazz;
      for (let i = 0; i < this.options.length; i++) {
        clazz = this.options[i].clazz;
        if (this.parentMdlJs.classList.contains(clazz)) {
          this.control.valueChanges.subscribe((value) => this.options[i].update(value));
          this.options[i].update(this.control.value);
          break;
        }
      }
      clazz == 'mdl-js-textfield' && new ControlValidator(this.control, this.parentMdlJs, this.injector.get(I18nService));
    }
  }

}

export class ControlValidator {

  private textFieldSpan;
  private textFieldInput;

  constructor(private control: FormControl,
              private parentMdlJs,
              private translate: {getValue: (string) => string}) {
    this.manageValidationErrors();
  }

  manageValidationErrors() {
    this.textFieldInput = this.parentMdlJs.querySelector('input');
    if (!this.textFieldInput) {
      this.textFieldInput = this.parentMdlJs.querySelector('textarea');
    }
    this.textFieldSpan = this.parentMdlJs.querySelector('span') || this.addSpan();
    this.control.statusChanges.subscribe(() => {
      if (this.control.invalid && this.control.dirty) {
        const errorName = Object.keys(this.control.errors)[0];
        const data = this.control.errors[errorName];
        this.reportErrorToUser(errorName, data);
      } else {
        this.reportSuccessToUser();
      }
    });
  }

  addSpan(): any {
    const newSpan = document.createElement('span');
    newSpan.classList.add("mdl-textfield__error");
    upgradeElement(newSpan);
    this.parentMdlJs.appendChild(newSpan);
    return newSpan;
  }

  reportErrorToUser(errorName, data) {
    this.textFieldSpan.innerHTML = getValidationErrorMessage(errorName, this.translate, data);
    this.textFieldInput && this.textFieldInput.setCustomValidity(' :( ');
    this.parentMdlJs.MaterialTextfield && this.parentMdlJs.MaterialTextfield.checkValidity();
  }

  reportSuccessToUser() {
    this.textFieldSpan.innerHTML = '';
    this.textFieldInput && this.textFieldInput.setCustomValidity('');
    this.parentMdlJs.MaterialTextfield && this.parentMdlJs.MaterialTextfield.checkValidity();
  }
}

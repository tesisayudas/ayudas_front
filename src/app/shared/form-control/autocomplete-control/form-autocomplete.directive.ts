import {AfterViewInit, Directive, HostListener, Input, OnDestroy} from '@angular/core';
import {FormControlSelectDirective} from "../form-control-select/form-control-select.directive";
import {ItemSearchableModel} from "@g3/crud/model/model-interfaces";
import {Subscription} from "rxjs/Subscription";

const BackspaceCode = 8;

/**
 * Form autocomplete-control directive
 * Add autocomplete-control behavior to an form control select
 */
@Directive({
  selector: '[g3FormAutocomplete]'
})
export class FormAutocompleteDirective implements AfterViewInit, OnDestroy {


  @Input() canAdd: boolean = false;

  /**
   * Object which holds the array of items to display
   */
  @Input() results: { items: any[] };
  /**
   * Service responsible to search the items
   */
  @Input() searchService: ItemSearchableModel;
  /**
   * Subscription to the observable of the search
   */
  subscription: Subscription;
  /**
   * The timeout of the last term entered
   */
  timeout = null;

  constructor(private formControlSelect: FormControlSelectDirective) {
    if (!this.formControlSelect) {
      console.warn('You must decorate the element as form control select');
    }
  }

  /**
   * Method which listen to keydown event
   * When the backspace is down if there is a selected li then the input is cleaned totally
   * If there is a selected li and any other key is down the key is ignored
   * @param event
   */
  @HostListener('keydown', ['$event'])
  keyDown(event) {
    const keyCode = event.keyCode;
    const element = this.formControlSelect.element.nativeElement;
    let container = this.formControlSelect.getParentGetMdlSelect(element).querySelector(".mdl-menu__container");
    if (container.classList.contains('g3-display__none'))
      container.classList.remove('g3-display__none');
    if (this.formControlSelect.selectedLi) {
      if (keyCode == BackspaceCode) {
        clearTimeout(this.timeout);
        this.formControlSelect.clearSelectedLi();
        this.formControlSelect.control.setValue('');
        this.formControlSelect.cachedValue = "";
        this.formControlSelect.element.nativeElement.value = "";
        this.searchService.nextTerm('');
      } else {
        event.preventDefault();
        event.stopPropagation();
      }
    }
  }

  /**
   * Method which listen to a key up event
   * 400 ms after a key is pressed up it put a next term in the search service subject
   * @param event
   */
  @HostListener('keyup', ['$event'])
  filterTerm(event) {
    const term = event.target.value;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => this.searchService.nextTerm(term), 400);

  }


  /**
   * Method which listen to focus out event
   * If no select an item clean the input
   */
  @HostListener('focusout')
  clearLi() {
    if (!this.canAdd) {
      setTimeout(() => {
        if (!this.formControlSelect.selectedLi) {
          this.formControlSelect.parentGetMdlSelect.classList.remove('is-dirty');
          this.formControlSelect.element.nativeElement.value = "";
          this.searchService.nextTerm('');
          this.formControlSelect.clearSelectedLi();
          this.formControlSelect.upgradeGetmdlSelect();
        } else {
          // this.searchService.nextTerm(this.formControlSelect.element.nativeElement.value);
        }
      }, 200)
    }
  }

  /**
   * Method which listen to focus event
   * Move the cursor to the end of the input
   * @param event
   */
  @HostListener('focus', ['$event'])
  focus(event) {
    const element = this.formControlSelect.element.nativeElement;
    if (this.canAdd) {
      let container = this.formControlSelect.getParentGetMdlSelect(element).querySelector(".mdl-menu__container");
      if (!element.value) {
        container.classList.add('g3-display__none');
      }
    }
    this.searchService.nextTerm(" ");
    setTimeout(() => {
      event.target.scrollLeft = event.target.scrollWidth;
    }, 5);
  }

  /**
   * After view init it subscribes to a search observable given a subject
   * Update the item and update the height of the select when a new results are given
   */
  ngAfterViewInit() {
    this.subscription = this.searchService.search(this.searchService.searchSubject().asObservable()).subscribe((results:any) => {
      if(results.items.length > 0){
        this.results.items = results.items;
      } else {
        this.results.items = [];
      }
      this.formControlSelect.updateHeight();
    });
  }

  ngOnDestroy(): void {
    this.subscription && this.subscription.unsubscribe();
  }
}

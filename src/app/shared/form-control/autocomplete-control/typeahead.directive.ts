import {
  AfterViewInit,
  Directive,
  ElementRef,
  Host,
  HostListener,
  Injector,
  Input,
  OnDestroy,
  Optional
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ItemSearchableModel} from "@g3/crud/model/model-interfaces";
import {G3FormControl} from "../form-control";
import {FormControl, FormGroupDirective, FormGroupName} from "@angular/forms";
import {upgradeElement} from "../../upgrades/upgrader";

@Directive({
  selector: '[g3Typeahead]'
})
export class TypeaheadDirective extends G3FormControl implements AfterViewInit, OnDestroy {
  constructor(
    public _element: ElementRef,
    public injector: Injector,
    @Host() public form: FormGroupDirective,
    @Optional() @Host() public formGroup: FormGroupName
  ){ super() }
  @Input() canAdd: boolean = false;

  @Input() g3Typeahead;

  /**
   * Object which holds the array of items to display
   */
  @Input() results: { items: any[] };
  /**
   * Service responsible to search the items
   */
  @Input() searchService: ItemSearchableModel;
  /**
   * Subscription to the observable of the search
   */
  subscription: Subscription;
  /**
   * The timeout of the last term entered
   */
  timeout = null;

  parent;

  container;

  cachedLis;

  /**
   * Give the form control.
   * @returns {FormControl}
   */
  public get control(): FormControl | null {
    return super.getControl(this.g3Typeahead);
  }

  //
  //
  // /**
  //  * Method which listen to focus out event
  //  * If no select an item clean the input
  //  */
  // @HostListener('focusout')
  // clearLi() {
  //   if (!this.canAdd) {
  //     setTimeout(() => {
  //       if (!this.formControlSelect.selectedLi) {
  //         this.formControlSelect.parentGetMdlSelect.classList.remove('is-dirty');
  //         this.formControlSelect.element.nativeElement.value = "";
  //         this.searchService.nextTerm('');
  //         this.formControlSelect.clearSelectedLi();
  //         this.formControlSelect.upgradeGetmdlSelect();
  //       } else {
  //         this.searchService.nextTerm(this.formControlSelect.element.nativeElement.value);
  //       }
  //     }, 200)
  //   }
  // }

  /**
   * Method which listen to a key up event
   * 400 ms after a key is pressed up it put a next term in the search service subject
   * @param event
   */
  @HostListener('keyup', ['$event'])
  filterTerm(event) {

    const term = event.target.value;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      let container = this.parent.querySelector(".mdl-menu__container");
      if (container.classList.contains('g3-display__none'))
        container.classList.remove('g3-display__none');
      upgradeElement(container);
      this.searchService.nextTerm(term);
    }, 400);

  }

  //
  /**
   * Method which listen to focus event
   * Move the cursor to the end of the input
   * @param event
   */
  @HostListener('focus', ['$event'])
  focus(event) {
    let container = this.parent.querySelector(".mdl-menu__container");
    if (!this.element.nativeElement.value.trim()) {
      container.classList.add('g3-display__none');
    }
    setTimeout(() => {
      event.target.scrollLeft = event.target.scrollWidth;
    }, 5);
  }

  //
  /**
   * After view init it subscribes to a search observable given a subject
   * Update the item and update the height of the select when a new results are given
   */
  ngAfterViewInit() {
    this.parent = this.element.nativeElement.parentNode;
    this.subscription = this.searchService.search(this.searchService.searchSubject().asObservable()).subscribe(results => {
      this.results.items = results;
      this.metodo();
      this.updateHeight();
    });
  }

  updateHeight() {
    setTimeout(() => {
      let element = this.parent.querySelector(".mdl-js-menu");
      let container = this.parent.querySelector(".mdl-menu__container");
      if (container.classList.contains('is-visible')) {
        element['MaterialMenu'].show();
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription && this.subscription.unsubscribe();
  }

  metodo() {
    new MutationObserver(() => {
      this.updateList();
      if (this.cachedLis) {
        this.addListenerToCurrentLis();
      }
    }).observe(this.parent, {
      childList: true,
      subtree: true
    });
  }

  /**
   * add the Listener event to the li in UL
   */
  addListenerToCurrentLis() {
    [].forEach.call(this.cachedLis, (li) => {
      if (li.tagName == 'LI') {
        this.addListenerToLi(li);
      }
    });
  }

  /**
   * Add the listener event to LI
   */
  addListenerToLi(li) {
    if (!li.dataset.listener) {
      li.dataset.listener = true;
      li.addEventListener("click", () => {
        //when clicked set the formControl value to the val of the li
        if (!!this.control) {
          this.element.nativeElement.value = li.dataset.val;
          this.control.setValue(li.dataset.val || '');
        } else {
          console.log('Worning: must be add a control');
        }
      });
    }
  }

  updateList() {
    const ul = this.parent.querySelector('.mdl-menu');
    if (ul && ul.tagName == 'UL') {
      this.cachedLis = ul.children;
    }
  }
}

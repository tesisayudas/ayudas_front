import {ElementRef, Host, Injector, Optional} from "@angular/core";
import {FormControl, FormGroupDirective, FormGroupName} from "@angular/forms";

/**
 * Search recursively the parentElement of a element by predicate function.
 * @param element - The child element.
 * @param {(element) => boolean} predicate - The predicate.
 * @returns {any} - The parent or null.
 */
export function findParent(element: any, predicate: (element) => boolean): any | null{
  if(!element.parentElement){
    return null;
  }
  if(predicate(element.parentElement)) {
    return element.parentElement;
  }
  return findParent(element.parentElement, predicate);
}

export abstract class G3FormControl {

  public abstract form: FormGroupDirective;
  public abstract formGroup: FormGroupName;
  public abstract _element: ElementRef;
  public abstract  injector: Injector;

  get element(): ElementRef {
    return this._element;
  }

  /**
   * Give the form control.
   * @returns {FormControl}
   */
  getControl(controlName): FormControl | null {
    let formControl = null;
    if (this.formGroup) {
      formControl = this.formGroup.control.get(controlName);
    } else if (!this.formGroup && this.form) {
      formControl = this.form.form.get(controlName);
    } else {
      throw Error("G3FormControl isn't inside of a formGroup directive");
    }
    return formControl;
  }

}

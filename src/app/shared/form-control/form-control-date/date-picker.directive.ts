import {Directive, ElementRef, HostListener} from '@angular/core';
import * as MdDateTimePicker from 'md-date-time-picker';
import * as moment from 'moment';
import {DefaultDate} from "./date-picker";
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";

@Directive({
  selector: '[g3DatePicker]'
})
export class DatePickerDirective {

  value;

  dialog;

  currentFormat: string = BASIC_DATE_FORMAT;

  object;
  public objectDate: DefaultDate | null = null;
  constructor(public element: ElementRef) {
    if (this.objectDate == null) {
      this.object = new DefaultDate();
    } else {
      this.object = this.objectDate;
    }
    if (this.element.nativeElement.value) {
      this.object.init = moment(this.element.nativeElement.value, this.currentFormat);
    }
    this.initDialog(this.object.returnObject());
    this.subscribe(this.element);
    this.getValueInFormat();
  }

  @HostListener('click')
  click() {
    this.dialog.toggle();
  }

  initDialog(object) {
    this.dialog = new MdDateTimePicker.default(object);
  }

  getValue() {
    return this.value;
  }

  getValueInFormat(format?: string) {
    this.addOnOkListener(() => {
      this.value = this.dialog.time.format(format || this.currentFormat).toString();
      this.element.nativeElement.value = this.value;
    });
  }

  addOnOkListener(fn: (value: string) => void) {
    this.element.nativeElement.addEventListener('onOk', () => {
      fn(this.dialog.time.format(this.currentFormat).toString());
    });
  }

  removeOnOkListerner(fn: (value: string) => void) {
    this.element.nativeElement.removeEventListener('onOk', () => {
      fn(this.dialog.time.format(this.currentFormat).toString());
    })
  }

  setCurrentFormat(format: string) {
    this.currentFormat = format;
  }

  setObject(object) {
    this.object = object;
  }

  private subscribe(element) {
    this.dialog.trigger = element.nativeElement;
  }
}

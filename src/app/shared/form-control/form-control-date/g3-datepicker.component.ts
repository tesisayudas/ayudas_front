import {Component, forwardRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from "@angular/core";
import {ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {DateAdapter, MatDatepickerInputEvent} from "@angular/material";
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";
import * as moment from 'moment';
@Component({
  selector: 'g3-custom-date-picker',
  template: `
    <mat-form-field class="g3-full-width g3-cursor-pointer">
      <input readonly
             matInput
             class="g3-cursor-pointer"
             [placeholder]="title"
             [min]="minValue"
             [matDatepicker]="picker"
             [value]="dateValue"
             (dateInput)="addEvent('input', $event)"
             (click)="picker.open()">
      <button mat-button *ngIf="dateValue.isValid()" matSuffix mat-icon-button aria-label="Clear"
              (click)="dateValue=null">
        <mat-icon>close</mat-icon>
      </button>
      <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
      <mat-datepicker [touchUi]="modal" #picker></mat-datepicker>
      <mat-hint *ngIf="getErrorKey() as key">{{('errors.'+key) | i18n}}</mat-hint>
    </mat-form-field>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => G3DatepickerComponent),
      multi: true
    }
  ]
})
export class G3DatepickerComponent implements ControlValueAccessor, OnChanges, OnInit, OnDestroy {

  @Input() _dateValue;
  @Input() title: string;
  @Input() min: string;
  @Input() modal: boolean = true;
  @Input() formControlName: string;
  @Input() form: FormGroup;
  public minValue: Date;
  private sub: Subscription;
  public propagateChange = (_: any) => {
  };


  constructor(private i18nService: I18nService,
              private adapter: DateAdapter<any>) { }

  ngOnInit() {
    this.sub = this.i18nService.translator$.subscribe((res: any) => {
      const abreviation = res.i18n.abbreviationLanguage;
      this.adapter.setLocale(abreviation);
    });
  }

  ngOnDestroy() {
    if (this.sub) this.sub.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    const {min} = changes;
    if (min) {
      this.minValue = moment(min.currentValue, BASIC_DATE_FORMAT).add(1, 'day').toDate();
      this.cleanInvalitMin(min.currentValue);
    }
  }

  getErrorKey(): string {
    if (this.form &&
      this.form.get(this.formControlName) &&
      this.form.get(this.formControlName).dirty &&
      this.form.get(this.formControlName).invalid &&
      this.form.get(this.formControlName).errors){
      return Object.keys(this.form.get(this.formControlName).errors)[0]
    }
    return ''
  }

  public cleanInvalitMin(minDate) {
    if (this._dateValue) {
      const diference = moment(minDate, BASIC_DATE_FORMAT).add(1, 'day').diff(moment(this._dateValue, BASIC_DATE_FORMAT), 'days');
      if (diference > 0) {
        this.dateValue = null;
      }
    }
  }

  get dateValue() {
    return moment(this._dateValue, BASIC_DATE_FORMAT);
  }

  set dateValue(val) {
    this._dateValue = moment(val).format(BASIC_DATE_FORMAT);
    const form = this.form && this.form.get(this.formControlName)
    if (form) form.markAsTouched()
    if (this._dateValue === "Invalid date") {
      if (form) form.markAsUntouched()
      this.propagateChange(null);
    } else {
      this.propagateChange(this._dateValue);

    }
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.dateValue = moment(event.value, BASIC_DATE_FORMAT);
  }

  writeValue(value: any) {
    if (value) {
      this.dateValue = moment(value, BASIC_DATE_FORMAT);
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }
}

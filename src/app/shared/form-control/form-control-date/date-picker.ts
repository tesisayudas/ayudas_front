import * as moment from 'moment';
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";


export class DefaultDate {

  constructor(locale: string = 'es') {
    this._language = locale;
    this._type = 'date';
    this._future = moment().add(50, 'years').locale(this._language);
    this._past = moment().subtract(50, 'years').locale(this._language);
    this._init = moment().locale(this._language);
  }

  private _language: string;

  get language() {
    return this._language;
  }

  set language(value: string) {
    this._language = value;
  }

  private _currentDate: string = BASIC_DATE_FORMAT;

  get currentDate(): string {
    return this._currentDate;
  }

  set currentDate(value: string) {
    this._currentDate = value;
  }

  private _type: string;

  set type(value: string) {
    this._type = value;
  }

  private _future: any;

  set future(value: any) {
    this._future = moment(value, this._currentDate).locale(this._language);
  }

  private _past: any;

  get type(): string {
    return this._type;
  }

  get future(): any {
    return this._future;
  }

  get past(): any {
    return this._past;
  }

  set past(value: any) {
    this._past = moment(value, this._currentDate).locale(this._language);
  }

  get init(): any {
    return this._init;
  }

  private _init: any;

  set init(value: any) {
    this._init = moment(value, this._currentDate).locale(this._language);
  }

  public returnObject(): object {
    return {type: this._type, future: this._future, past: this._past, init: this._init}
  }

}

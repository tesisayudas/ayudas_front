import {AfterViewInit, Directive, ElementRef, Host, HostListener, Injector, Input, Optional} from '@angular/core';
import {DatePickerDirective} from "./date-picker.directive";
import {G3FormControl} from "../form-control";
import {FormControl, FormGroupDirective, FormGroupName} from "@angular/forms";
import {DefaultDate} from "./date-picker";
import * as moment from 'moment';
import {ControlValidator} from "../form-control-name/form-control-name.directive";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";
import {distinctUntilChanged} from "rxjs/operators";

@Directive({
  selector: '[g3FormControlDate]'
})
export class FormControlDateDirective extends G3FormControl implements AfterViewInit {
  constructor(
    public _element: ElementRef,
    public injector: Injector,
    @Host() public form: FormGroupDirective,
    @Optional() @Host() public formGroup: FormGroupName
  ){ super() }
  @Input() g3FormControlDate;

  @Input() pastDate;

  datePicker: DatePickerDirective;

  control: FormControl;

  pastControl;

  parentMdlJs;

  translate: I18nService;

  suscrito = false;

  initialDatePicker: DefaultDate;

  @Input() initialDate: string;

  ngAfterViewInit() {
    this.translate = this.injector.get(I18nService);
    this.initialDatePicker = new DefaultDate();
    if (this.initialDate != null) {
      this.initialDatePicker.past = moment(this.initialDate, BASIC_DATE_FORMAT);
    }
    this.translate.metadataLanguage$.subscribe((language) => {
      this.initialDatePicker.language = language.abbreviation;
      this.initDialog();
    });
    this.element.nativeElement.readOnly = true;
    this.control = this.getControl(this.g3FormControlDate);
    this.pastControl = this.getControl(this.pastDate);
    this.parentMdlJs = this.element.nativeElement.parentElement;
    this.initDialog();
    this.control.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe((val) => {
      if (val != null) {
        this.initialDatePicker.init = val;
        this.element.nativeElement.value = val;
        this.initDialog();
      } else {
        this.element.nativeElement.value = val;
        this.initDialog();
      }
    });
    if (this.pastControl) {
      this.pastControl.valueChanges.subscribe((val) => {

        if (this.initialDatePicker.init.diff(moment(val, BASIC_DATE_FORMAT), 'days') < 1) {
          this.initialDatePicker.init = moment(val, BASIC_DATE_FORMAT).add(1, 'days').format(BASIC_DATE_FORMAT).toString();
          this.element.nativeElement.value = null;
          if (this.control.value) {
            this.control.reset();
          }
        }
        this.initialDatePicker.past = moment(val, BASIC_DATE_FORMAT).add(1, 'days').format(BASIC_DATE_FORMAT).toString();
        this.initDialog();
      });
    }
    new ControlValidator(this.control, this.parentMdlJs, this.injector.get(I18nService));
  }

  initDialog() {
    this.datePicker = new DatePickerDirective(this.element);
    this.subscribeListener();
    this.parentMdlJs.MaterialTextfield && this.parentMdlJs.MaterialTextfield.checkDirty();
  }

  unsubscribeListener() {
    this.datePicker.removeOnOkListerner(() => {
      this.suscrito = false;
    });
  }

  subscribeListener() {
      this.datePicker.addOnOkListener((value) => {
        this.suscrito = true;
        this.control.setValue(value);
      });
    this.parentMdlJs.MaterialTextfield && this.parentMdlJs.MaterialTextfield.checkDirty();
  }

  @HostListener('click')
  click() {
    this.datePicker.dialog.toggle();
  }
}

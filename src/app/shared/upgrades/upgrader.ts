/**
 * Object of MDL to generate MDL elements dynamically.
 */
declare var componentHandler: any;

/**
 * Object of getmdl-select (select for MDL of third) to generation dynamical.
 */
declare var getmdlSelect: any;

/**
 * Upgrade a element and its children.
 * @param element - NativeElement to upgrade.
 */
export function upgradeElementWithChildren(element: any) {
  componentHandler.upgradeElements(element);
}

/**
 * Upgrade to a MDL element.
 * @param element
 */
export function upgradeElement(element: any) {
  componentHandler.upgradeElement(element);
}

/**
 * Init for all select of getmdl-select.
 */
export function upgradeSelects(cssSelector = '.getmdl-select'): void {
  getmdlSelect.init(cssSelector);
}

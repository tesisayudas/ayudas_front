import {AfterViewInit, Directive, ElementRef, NgZone} from '@angular/core';
import {upgradeElementWithChildren} from "../upgrader";

/**
 * Upgrade elements created dynamically.
 */
@Directive({
  selector: '[g3UpgradeElements]'
})
export class UpgradeElementsDirective implements AfterViewInit {

  /**
   * Upgrade a elements and its children.
   * @param {ElementRef} element
   */
  constructor(private element: ElementRef, private ngZone: NgZone) {
  }

  /**
   * Upgrade the element and its children.
   */
  ngAfterViewInit() {
    if(this.element) {
      this.ngZone.runOutsideAngular(() => {
        upgradeElementWithChildren(this.element.nativeElement);
      });
    }
  }

}

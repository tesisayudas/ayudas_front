import {AfterViewInit, Directive, ElementRef, NgZone} from '@angular/core';
import {upgradeElement} from "../upgrader";

/**
 * Upgrade the MDL element created dynamically.
 */
@Directive({
  selector: '[g3Upgrade]'
})
export class UpgradeDirective implements AfterViewInit {

  /**
   * Create a new MDL element dynamically.
   * @param {ElementRef} element
   */
  constructor(private element: ElementRef, private ngZone: NgZone) { }

  /**
   * Upgrade to the element.
   */
  ngAfterViewInit() {
    if(this.element){
      this.ngZone.runOutsideAngular(() =>
        upgradeElement(this.element.nativeElement)
      );
    }
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {I18nPipe} from './i18n/pipes/i18n.pipe';
import {I18nDirective} from './i18n/directives/i18n.directive';
import {LoadingComponent} from './loading/loading/loading.component';
import {ProgressBarIndeterminateComponent} from './loading/progress-bar-indeterminate/progress-bar-indeterminate.component';
import {SpinnerComponent} from './loading/spinner/spinner.component';
import {ToastDirective} from './toast/toast.directive';
import {DialogPolyfillDirective} from './dialogs/directives/dialog-polyfill/dialog-polyfill.directive';
import {DialogComponent} from "./dialogs/components/dialog/dialog.component";
import {CloseDialogDirective} from "./dialogs/directives/close-dialog/close-dialog.directive";
import {RenderAtBodyDirective} from "./dialogs/directives/render-at-body/render-at-body.directive";
import {ShowDialogDirective} from "./dialogs/directives/show-dialog/show-dialog.directive";
import {UpgradeDirective} from './upgrades/upgrade/upgrade.directive';
import {UpgradeElementsDirective} from './upgrades/upgradeElements/upgrade-elements.directive';
import {FormControlNameDirective} from './form-control/form-control-name/form-control-name.directive';
import {SnackbarPartnerDirective} from './toast/snackbar-partner.directive';
import {FormControlSelectDirective} from './form-control/form-control-select/form-control-select.directive';
import {I18nVarPipe} from './i18n/pipes/i18n-var.pipe';
import {FormControlDateDirective} from './form-control/form-control-date/form-control-date.directive';
import {DatePickerDirective} from './form-control/form-control-date/date-picker.directive';
import {ResponsiveWrapperComponent} from "./responsive-wraper/responsive-wrapper.component";
import {FixedMenuDirective} from './fixed-menu.directive';
import {FormAutocompleteDirective} from "./form-control/autocomplete-control/form-autocomplete.directive";
import {DotColorComponent} from './dot-color/dot-color.component';
import {TypeaheadDirective} from './form-control/autocomplete-control/typeahead.directive';
import {SemaphoreComponent} from "./semaphore/semaphore.component";
import {SingleFileComponent} from './file-form/single-file/single-file.component';
import {MultiFileFormComponent} from "./file-form/multi-file/multi-file-form.component";
import {FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MAT_DATE_FORMATS, MatNativeDateModule} from "@angular/material/core";
import {MatButtonModule, MatIconModule, MatInputModule} from "@angular/material";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";
import {G3DatepickerComponent} from './form-control/form-control-date/g3-datepicker.component';
import {SelectControlComponent} from './form-control/form-control-select/select-control/select-control.component';
import {SafeHtmlPipe} from './safe-html.pipe';
import { MultiDataComponent } from './multi-data/multi-data.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'll',
  },
  display: {
    dateInput: BASIC_DATE_FORMAT,
    monthYearLabel: 'll',
    dateA11yLabel: BASIC_DATE_FORMAT,
    monthYearA11yLabel: BASIC_DATE_FORMAT,
  },
};
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatMomentDateModule,
    MatIconModule,
    MatButtonModule,
  ],
  declarations: [
    I18nPipe,
    I18nDirective,
    LoadingComponent,
    ProgressBarIndeterminateComponent,
    SpinnerComponent,
    ToastDirective,
    DialogComponent,
    CloseDialogDirective,
    ShowDialogDirective,
    DialogPolyfillDirective,
    RenderAtBodyDirective,
    UpgradeDirective,
    UpgradeElementsDirective,
    FormControlNameDirective,
    SnackbarPartnerDirective,
    FormControlSelectDirective,
    I18nVarPipe,
    FormControlDateDirective,
    DatePickerDirective,
    ResponsiveWrapperComponent,
    FixedMenuDirective,
    FormAutocompleteDirective,
    DotColorComponent,
    TypeaheadDirective,
    SemaphoreComponent,
    MultiFileFormComponent,
    SingleFileComponent,
    G3DatepickerComponent,
    SelectControlComponent,
    SafeHtmlPipe,
    MultiDataComponent,
  ],
  exports: [
    I18nPipe,
    I18nDirective,
    ProgressBarIndeterminateComponent,
    SpinnerComponent,
    ToastDirective,
    DialogComponent,
    CloseDialogDirective,
    ShowDialogDirective,
    UpgradeDirective,
    UpgradeElementsDirective,
    FormControlNameDirective,
    SnackbarPartnerDirective,
    FormControlSelectDirective,
    FormControlDateDirective,
    DatePickerDirective,
    I18nVarPipe,
    ResponsiveWrapperComponent,
    FixedMenuDirective,
    FormAutocompleteDirective,
    DotColorComponent,
    TypeaheadDirective,
    SemaphoreComponent,
    MultiFileFormComponent,
    SingleFileComponent,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatMomentDateModule,
    MatIconModule,
    MatButtonModule,
    G3DatepickerComponent,
    SelectControlComponent,
    SafeHtmlPipe,
    MultiDataComponent,
  ],
  providers:[
    FormGroupDirective,
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class SharedModule {
}

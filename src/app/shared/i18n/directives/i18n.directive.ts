import {Directive, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {I18nService} from '../../../core/i18n/i18n.service';

/**
 * Convert the key in its value depend of current language.
 */
@Directive({
  selector: '[g3I18n]'
})
export class I18nDirective implements OnInit, OnDestroy {

  /**
   * The key to transform.
   */
  key: string;

  /**
   * The subscription to the translator.
   */
  translatorSubscription: Subscription;

  /**
   * Create a new {@link I18nDirective}.
   * @param {ElementRef} element - The element that contain the nativeElement.
   * @param {I18nService} i18n
   */
  constructor(private element: ElementRef, private i18n: I18nService) {
  }

  /**
   * Getting the key and it convert in its value depend of current language.
   */
  ngOnInit() {
    this.key = this.getContent();
    this.translatorSubscription = this.i18n.translator$.subscribe(translator => {
      const value = translator.getValue(this.key);
      this.updateContent(value);
    });
    this.i18n.update();
  }

  /**
   * Set the value of the innerHTML of the element.
   * @param {string} value
   */
  updateContent(value: string) {
    this.element.nativeElement.innerHTML = value;
  }

  /**
   * Give the innerHTML of the element.
   * @returns {string} - The innerHTML of element.
   */
  getContent(): string {
    return this.element.nativeElement.innerHTML;
  }

  /**
   * Cancel the subscription of the translator.
   */
  ngOnDestroy() {
    this.translatorSubscription.unsubscribe();
  }

}

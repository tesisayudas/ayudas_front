import {Pipe, PipeTransform} from '@angular/core';
import {I18nService} from '../../../core/i18n/i18n.service';

/**
 * Transform a key to one value depend of current language.
 */
@Pipe({
  name: 'i18n',
  pure: false
})
export class I18nPipe implements PipeTransform {

  /**
   * The last value.
   */
  cachedValue: string;

  /**
   * The last abbreviation language.
   */
  cachedAbbreviationLanguage: string;

  /**
   * Create a new {@link I18nPipe}.
   * @param {I18nService} i18n
   */
  constructor(protected i18n: I18nService) {
  }

  /**
   * If has been changed the language to get newly the value fo the key.
   * @param {string} key
   * @returns {string} - The value of key depend of current language.
   */
  transform(key: string): string {

    if (this.i18n.getAbbreviationCurrentLanguage() != this.cachedAbbreviationLanguage) {
      this.cachedAbbreviationLanguage = this.i18n.getAbbreviationCurrentLanguage();
      this.cachedValue = this.i18n.getValue(key);
    }

    return this.cachedValue;
  }

}

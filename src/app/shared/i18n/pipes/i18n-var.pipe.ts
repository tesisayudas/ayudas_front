import { Pipe, PipeTransform } from '@angular/core';
import {I18nPipe} from "./i18n.pipe";

@Pipe({
  name: 'i18nVar'
})
export class I18nVarPipe extends I18nPipe implements PipeTransform {

  /**
   * The last key
   */
  cachedKey;

  /**
   * If has been changed the language or the key to get newly the value fo the key.
   * @param {string} key
   * @returns {string} - The value of key depend of current language.
   */
  transform(key: string): string {

    if(this.cachedKey != key) {
      this.cachedKey = key;
      this.cachedValue = this.i18n.getValue(key);
    }

    return super.transform(this.cachedKey);
  }

}

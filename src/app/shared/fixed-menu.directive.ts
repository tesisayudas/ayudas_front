import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[g3FixedMenu]'
})
export class FixedMenuDirective {

  @Input() g3FixedMenu;
  @Input() container;

  @HostListener('click', ['$event'])
  click(event) {
    setTimeout(() => {
      this.setPosition(this.g3FixedMenu, this.container, this.g3FixedMenu.parentNode, event)
    });
  }

  setPosition(element_, forElement_, container_, event: MouseEvent){
    if (element_ && forElement_) {
      const rect = forElement_.getBoundingClientRect();
      const forRect = forElement_.parentElement.getBoundingClientRect();
      if (element_.classList.contains(CssClasses_.UNALIGNED)) {
      } else if (element_.classList.contains(CssClasses_.BOTTOM_RIGHT)) {
        // Position below the "for" element, aligned to its right.
        container_.style.right =  window.innerWidth - rect.right + 'px';
        container_.style.top = rect.top - forElement_.scrollTop + rect.height+ 'px';
      } else if (element_.classList.contains(CssClasses_.TOP_LEFT)) {
        // Position above the "for" element, aligned to its left.
        container_.style.left = forElement_.offsetLeft-container_.offsetWidth + 'px';
        container_.style.bottom = ((window.innerHeight - event.screenY)+forRect.bottom - rect.top) + 'px';
      } else if (element_.classList.contains(CssClasses_.TOP_RIGHT)) {
        // Position above the "for" element, aligned to its right.
        container_.style.right =  window.innerWidth - rect.right + 'px';
        container_.style.top = rect.top - forElement_.scrollTop -  container_.getBoundingClientRect().height+ 'px';
      } else {
        // Default: position below the "for" element, aligned to its left.
        container_.style.left = forElement_.offsetLeft + 'px';
        container_.style.top = forElement_.offsetTop + forElement_.offsetHeight + 'px';
      }
    }
  }


}

export const CssClasses_ = {
  CONTAINER: 'mdl-menu__container',
  OUTLINE: 'mdl-menu__outline',
  ITEM: 'mdl-menu__item',
  ITEM_RIPPLE_CONTAINER: 'mdl-menu__item-ripple-container',
  RIPPLE_EFFECT: 'mdl-js-ripple-effect',
  RIPPLE_IGNORE_EVENTS: 'mdl-js-ripple-effect--ignore-events',
  RIPPLE: 'mdl-ripple',
  // Statuses
  IS_UPGRADED: 'is-upgraded',
  IS_VISIBLE: 'is-visible',
  IS_ANIMATING: 'is-animating',
  // Alignment options
  BOTTOM_LEFT: 'mdl-menu--bottom-left',
  // This is the default.
  BOTTOM_RIGHT: 'mdl-menu--bottom-right',
  TOP_LEFT: 'mdl-menu--top-left',
  TOP_RIGHT: 'mdl-menu--top-right',
  UNALIGNED: 'mdl-menu--unaligned'
};

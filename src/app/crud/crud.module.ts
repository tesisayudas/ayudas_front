import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ConfirmationDialogComponent} from "./view/confirmation-dialog/confirmation-dialog.component";
import {PaginationItemComponent} from "./view/table/pagination-item/pagination-item.component";
import {IconComponent} from "./view/icons/icon/icon.component";
import {
  CloseIconComponent, CustomIconComponent,
  DeleteIconComponent,
  EditIconComponent,
  EyeIconComponent
} from "./view/icons/icons.component";
import {SwitchComponent} from "./view/switch/switch.component";
import {DialogFormComponent, FormDialogComponent} from './view/form-dialog/form-dialog.component';
import {ChangeStatusComponent} from './view/change-status/change-status.component';
import {MessageDialogComponent} from "./view/message-dialog/message-dialog.component";
import {RowMenuActionComponent} from './view/table/row-menu-actions/row-menu-action/row-menu-action.component';
import {RowMenuSwitchComponent} from './view/table/row-menu-actions/row-menu-switch/row-menu-switch.component';
import {RowMenuComponent} from "./view/table/row-menu/row-menu.component";
import {MobileTableComponent} from "./view/table/mobile-table/mobile-table.component";
import {ScrollableTableDirective} from "./view/table/scrollable-table.directive";
import {TableContainerComponent} from './view/table/table-container/table-container.component';
import {DesktopTableComponent} from './view/table/desktop-table/desktop-table.component';
import {DetailsDialogComponent} from './view/details-dialog/details-dialog.component';
import {DeleteDialogComponent} from './view/delete-dialog/delete-dialog.component';
import {ErrorMessageDialogComponent} from './view/error-message-dialog/error-message-dialog.component';
import {ChangeStateWithRelationComponent} from "./view/change-state-with-relation/change-state-with-relation.component";
import {TableComponent} from "./view/table/generic-table/table.component";
import {TableHeaderComponent} from "./view/table/generic-table/table-header.component";
import {TableRowComponent} from "./view/table/generic-table/table-row.component";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ConfirmationDialogComponent,
    PaginationItemComponent,
    IconComponent,
    EyeIconComponent,
    EditIconComponent,
    CloseIconComponent,
    DeleteIconComponent,
    SwitchComponent,
    FormDialogComponent,
    ChangeStatusComponent,
    MessageDialogComponent,
    RowMenuActionComponent,
    RowMenuSwitchComponent,
    RowMenuComponent,
    MobileTableComponent,
    ScrollableTableDirective,
    TableContainerComponent,
    DesktopTableComponent,
    DetailsDialogComponent,
    DeleteDialogComponent,
    ErrorMessageDialogComponent,
    ChangeStateWithRelationComponent,
    DialogFormComponent,
    TableComponent,
    TableHeaderComponent,
    TableRowComponent,
    CustomIconComponent
  ],
  exports: [
    SharedModule,
    PaginationItemComponent,
    EyeIconComponent,
    CloseIconComponent,
    EditIconComponent,
    DeleteIconComponent,
    SwitchComponent,
    ReactiveFormsModule,
    FormsModule,
    FormDialogComponent,
    ConfirmationDialogComponent,
    ChangeStatusComponent,
    MessageDialogComponent,
    RowMenuActionComponent,
    RowMenuSwitchComponent,
    RowMenuComponent,
    MobileTableComponent,
    ScrollableTableDirective,
    TableContainerComponent,
    DesktopTableComponent,
    DetailsDialogComponent,
    DeleteDialogComponent,
    ErrorMessageDialogComponent,
    ChangeStateWithRelationComponent,
    DialogFormComponent,
    TableComponent,
    TableHeaderComponent,
    TableRowComponent,
    CustomIconComponent
  ]
})
export class CrudModule {
}

import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ConverterFinalString} from "../../controller/converter-final-string";

@Component({
  selector: 'g3-error-message-dialog',
  templateUrl: './error-message-dialog.component.html',
})
export class ErrorMessageDialogComponent implements OnInit, OnChanges {

  private static readonly errorMessage = "error-message-dialog-id";

  @Input() idDialog = ErrorMessageDialogComponent.errorMessage;

  @Input() titleKey;
  @Input() messageKey;
  @Input() okBtnNameKey = 'shared.accept';

  @Input() literalTitle;
  @Input() literalMessage;

  finalTitle;
  finalMessage;

  okBtnName = 'shared.accept';

  @ViewChild('body') body;

  converterFinalString;

  constructor(private i18n: I18nService, private dialogService: DialogService) {
    this.i18n.translator$.subscribe(() => this.update());
    this.converterFinalString = new ConverterFinalString().priority('key').translator(this.i18n);
  }

  update() {
    this.finalTitle = this.converterFinalString.key(this.titleKey).literal(this.literalTitle).getFinalString();
    this.finalMessage = this.converterFinalString.key(this.messageKey).literal(this.literalMessage).getFinalString();
    this.okBtnName = this.i18n.getValue(this.okBtnNameKey);
  }

  ngOnInit() {
    if (this.idDialog == ErrorMessageDialogComponent.errorMessage) {
      console.warn('You should to assign a idDialog to ConfirmationDialog');
    }
  }

  ngOnChanges() {
    this.update();
  }

  open() {
    this.dialogService.showDialogComponent(this.idDialog);
  }

}

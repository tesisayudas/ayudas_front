import {Component, Input, ViewChild} from '@angular/core';
import {ChangeStatusComponent} from "../change-status/change-status.component";
import {ErrorMessageDialogComponent} from "../error-message-dialog/error-message-dialog.component";

@Component({
  selector: 'g3-change-state-with-relation',
  templateUrl: './change-state-with-relation.component.html',
})
export class ChangeStateWithRelationComponent extends ChangeStatusComponent {

  @Input() errorMessage: string;

  @ViewChild(ErrorMessageDialogComponent) errorDialog: ErrorMessageDialogComponent;

  change() {
    this.service.enable(this.entity.id, !this.entity.enabled)
      .then((response) => {
        if (response == 204) {
          this.manageResponse(!this.entity.enabled);
        } else if (response.status == 400) {
          this.errorDialog.open();
        }
      })
      .catch((err) => console.log("error: ", err));
    this.dialogService.closeDialogComponent(this.idDialog);
  }
}

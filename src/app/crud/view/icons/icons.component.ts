import {Component, Input} from '@angular/core';
import {IconComponent} from "./icon/icon.component";

@Component({
  selector: 'g3-eye-icon',
  template: `
    <g3-icon
      iconFont="eye"
      tooltipMessageKey="shared.show"
    >
    </g3-icon>
  `,
  styles: []
})
export class EyeIconComponent extends IconComponent {}


@Component({
  selector: 'g3-edit-icon',
  template: `
    <g3-icon
      [isDisabled]="isDisabled"
      iconFont="pencil"
      tooltipMessageKey="shared.edit"
    >
    </g3-icon>
  `,
  styles: []
})
export class EditIconComponent extends IconComponent { }

@Component({
  selector: 'g3-delete-icon',
  template: `
    <g3-icon class="g3-button--delete"
             iconFont="delete-forever"
             tooltipMessageKey="shared.delete"
    >
    </g3-icon>`
})
export class DeleteIconComponent extends IconComponent {
}


@Component({
  selector: 'g3-close-icon',
  template: `
    <g3-icon
      [isDisabled]="isDisabled"
      class="g3-button--delete"
      iconFont="calendar-remove"
      tooltipMessageKey="shared.close"
    >
    </g3-icon>
  `,
  styles: [""]
})
export class CloseIconComponent extends IconComponent {
}


@Component({
  selector: 'g3-custom-icon',
  template: `
    <g3-icon
      [iconFont]="icon"
      [isDisabled]="isDisabled"
      [tooltipMessageKey]="isDisabled ? messageOnDisabled : message">
    </g3-icon>
  `,
  styles: []
})
export class CustomIconComponent extends IconComponent {
  @Input() icon;
  @Input() message;
  @Input() messageOnDisabled;
  @Input() isDisabled;
}

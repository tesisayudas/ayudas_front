import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ConverterFinalString} from "../../../controller/converter-final-string";
import {generateIdWithPrefix} from "../../../controller/generator-id-with-prefix";

@Component({
  selector: 'g3-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit, OnChanges {

  @Input() public isDisabled: boolean = false;

  @Input() id;

  @Input() tooltipMessageKey = "";

  @Input() tooltipMessage;

  @Input() iconFont = "insert_emoticon";

  @Output() click = new EventEmitter();

  iconId;

  finalTooltipMessage;

  converterFinalString: ConverterFinalString;

  constructor(private i18n: I18nService) {
    this.i18n.translator$.subscribe(() => this.update());
    this.converterFinalString = new ConverterFinalString().priority('key').translator(this.i18n);
  }

  ngOnInit() {
    this.iconId = generateIdWithPrefix("g3-icon-id-");
    this.update();
  }

  update() {
    this.finalTooltipMessage = this.converterFinalString.key(this.tooltipMessageKey).literal(this.tooltipMessage).getFinalString();
  }

  ngOnChanges() {
    this.update();
  }

  onclick() {
    this.click.next();
  }

}

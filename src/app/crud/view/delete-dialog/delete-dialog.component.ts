import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ItemDeletableModel} from "../../model/model-interfaces";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {ItemConfirmationDelete} from "../../controller/controller-interfaces";

@Component({
  selector: 'g3-delete-dialog',
  templateUrl: './delete-dialog.component.html'
})
export class DeleteDialogComponent implements ItemConfirmationDelete {

  @Input() idDialog: string;

  @Input() deleteMessageKey: string = 'shared.delete_confirmation';

  @Input() service: ItemDeletableModel;

  @Output() click = new EventEmitter<any>();

  entity;

  constructor(protected dialogService: DialogService,
              protected i18Service: I18nService,
              protected toastService: SnackbarService) {
  }

  deleteConfirm(entity) {
    this.entity = entity;
    this.dialogService.showDialogComponent(this.idDialog);
  }

  closeDialog() {
    this.dialogService.closeDialogComponent(this.idDialog);
  }

  delete() {
    if (!!this.service) {
      this.service.delete(this.entity.id);
      this.closeDialog();
      this.toastService.showToast(this.i18Service.getValue('shared.deleted_ok'));
    } else {
      this.click.next({entity: this.entity});
    }
  }

  getMessage() {
    return this.i18Service.getValue(this.deleteMessageKey);
  }
}

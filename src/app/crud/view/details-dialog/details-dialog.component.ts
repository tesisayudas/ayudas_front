import {Component, Input} from '@angular/core';
import {ItemDetailsDialog} from "../../controller/controller-interfaces";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";

@Component({
  selector: 'g3-details-dialog',
  templateUrl: './details-dialog.component.html',
  styleUrls: ['./details-dialog.component.css']
})
export class DetailsDialogComponent implements ItemDetailsDialog {

  entity;

  @Input() title = null;

  @Input() attrTitle = null;

  @Input() idDialog = 'dialogShow';

  constructor(private dialogService: DialogService) {
  }

  open() {
    this.dialogService.showDialogComponent(this.idDialog);
  }

  setEntity(entity) {
    this.entity = entity;
  }

}

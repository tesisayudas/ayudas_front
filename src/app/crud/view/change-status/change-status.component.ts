import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ItemStateChanger} from "../../controller/controller-interfaces";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {BackItemCrudManager} from "../../model/item-crud-manager";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";

@Component({
  selector: 'g3-change-status',
  templateUrl: './change-status.component.html'
})
export class ChangeStatusComponent implements ItemStateChanger {

  @Input() idDialog: string;

  @Input() enabledMessageKey: string = 'shared.enable_confirmation';

  @Input() disabledMessageKey: string = 'shared.disable_confirmation';

  @Input() service: BackItemCrudManager;

  @Output() onChangeData: EventEmitter<any> = new EventEmitter<any>();

  message;

  label;

  entity;

  constructor(protected dialogService: DialogService,
              protected i18Service: I18nService,
              protected toastService: SnackbarService) {
  }

  changeState(event, labelState, entity) {
    this.entity = entity;
    if (labelState) {
      this.label = labelState;
      event.preventDefault();
    }
    this.message = (this.entity.enabled) ? this.i18Service.getValue(this.disabledMessageKey) : this.i18Service.getValue(this.enabledMessageKey);
    this.dialogService.showDialogComponent(this.idDialog);
  }

  change() {
    this.service.enable(this.entity.id, !this.entity.enabled)
      .then(() => this.manageResponse(!this.entity.enabled))
      .catch((err) => console.log("error: ", err));
    this.dialogService.closeDialogComponent(this.idDialog);
  }

  private updateLabel(enabled: boolean) {
    if (enabled) {
      this.label.classList.add('is-checked');
    } else {
      this.label.classList.remove('is-checked');
    }
  }


  manageResponse(enabled: boolean) {
    if (this.label) {
      this.updateLabel(enabled);
    }
    this.entity.enabled = enabled;
    this.toastService.showToast(this.i18Service.getValue('shared.status_changed'))
  }

}

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild,} from '@angular/core';
import {I18nService} from "@g3/core/i18n/i18n.service";
import {ConverterFinalString} from "../../controller/converter-final-string";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";

@Component({
  selector: 'g3-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html'
})
export class ConfirmationDialogComponent implements OnInit, OnChanges {

  private static readonly defaultDialogId = "confirmation--dialog-id";

  @Input() idDialog = ConfirmationDialogComponent.defaultDialogId;

  @Input() titleKey;
  @Input() messageKey;
  @Input() okBtnNameKey = 'shared.accept';
  @Input() cancelBtnNameKey = 'shared.cancel';

  @Output() ok = new EventEmitter();

  @Input('title') literalTitle;
  @Input('message') literalMessage;

  finalTitle;
  finalMessage;

  okBtnName = 'shared.accept';
  cancelBtnName = 'shared.cancel';

  @ViewChild('body') body;

  converterFinalString;

  constructor(private i18n: I18nService, private dialogService: DialogService) {
    this.i18n.translator$.subscribe(() => this.update());
    this.converterFinalString = new ConverterFinalString().priority('key').translator(this.i18n);
  }

  confirmed() {
    this.ok.next();
    this.closeConfirmationDialog();
  }

  update() {
    this.finalTitle = this.converterFinalString.key(this.titleKey).literal(this.literalTitle).getFinalString();
    this.finalMessage = this.converterFinalString.key(this.messageKey).literal(this.literalMessage).getFinalString();
    this.okBtnName = this.i18n.getValue(this.okBtnNameKey);
    this.cancelBtnName = this.i18n.getValue(this.cancelBtnNameKey);
  }

  ngOnInit() {
    if(this.idDialog == ConfirmationDialogComponent.defaultDialogId) {
      console.warn('You should to assign a idDialog to ConfirmationDialog');
    }
  }

  ngOnChanges() {
    this.update();
  }

  closeConfirmationDialog(){
    this.dialogService.closeDialogComponent(this.idDialog);
  }

}

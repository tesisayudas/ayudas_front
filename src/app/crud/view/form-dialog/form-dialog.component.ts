import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {DialogComponent} from "../../../shared/dialogs/components/dialog/dialog.component";
import {CustomFormDialog} from "../../controller/form/custom-dialog-form";

@Component({
  selector: 'g3-form-dialog',
  templateUrl: './form-dialog.component.html'
})
export class FormDialogComponent implements AfterViewInit{

  @ViewChild(DialogComponent) dialogCmp: DialogComponent;

  @Input() idDialog = "form-dialog-component-default-id";
  @Input() title = 'default-title';
  @Input() disabled: () => boolean | boolean;

  @Output() save = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() availableDialog = new EventEmitter<DialogComponent>();

  @Input() size = 'SMALL';
  @Input() idForm = 'form';

  public save_() {
    this.save.next();
  }

  public close_() {
    this.close.next();
  }

  ngAfterViewInit() {
    this.availableDialog.next(this.dialogCmp);
  }
}

@Component({
  selector: 'g3-dialog-form',
  template: `
    <g3-dialog [idDialog]="idDialog" [size]="size">
      <header>
        <h4 class="mdl-dialog__title">{{form.title}}</h4>
      </header>
      <body>
      <div class="mdl-dialog__content">

        <ng-content></ng-content>

      </div>
      </body>

      <footer g3UpgradeElements>
        <div class="mdl-dialog__actions">
          <button [attr.form]="idForm" class="mdl-button mdl-js-button mdl-js-ripple-effect"
                  [disabled]="validatorForm.invalid"
                  (click)="save()" g3I18n>shared.save
          </button>
          <button class="mdl-button mdl-js-button mdl-js-ripple-effect" (click)="close()" g3I18n>shared.cancel</button>
        </div>
      </footer>
    </g3-dialog>`
})
export class DialogFormComponent implements AfterViewInit{

  @ViewChild(DialogComponent) dialogCmp: DialogComponent;

  @Input() idDialog = "form-dialog-component-default-id";
  @Input() form: CustomFormDialog;
  @Input() validatorForm;
  @Input() idForm = "idForm";
  @Input() size;

  public save() {
    this.form.customForm.save();
  }

  public close() {
    this.form.close();
  }

  ngAfterViewInit() {
    this.form.dialogComponent = this.dialogCmp;
  }
}

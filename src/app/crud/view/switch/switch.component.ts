import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges, OnChanges} from '@angular/core';
import {generateIdWithPrefix} from "../../controller/generator-id-with-prefix";
import {I18nService} from "@g3/core/i18n/i18n.service";

@Component({
  selector: 'g3-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})
export class SwitchComponent implements OnInit, OnChanges {

  @Input() id;

  @Input() checked = false;

  @Output() change = new EventEmitter<any>();

  @Input() disabled = false;

  @Input() tooltipMsgKey = 'shared.enable_or_disable';

  tooltipMsg;

  switchId;

  constructor(private i18n: I18nService){
    this.i18n.translator$.subscribe(() => this.updateTooltipMsg());
    this.updateTooltipMsg();
  }

  updateTooltipMsg() {
    this.tooltipMsg = this.i18n.getValue(this.tooltipMsgKey);
  }

  onClick(event, label){
    this.change.next({event: event, label: label});
  }

  ngOnInit() {
    this.switchId = generateIdWithPrefix("g3-switch-id-");
  }

  ngOnChanges(changes: SimpleChanges) {
    for(let propName in changes) {
      if(propName == 'tooltipMsgKey') {
        this.updateTooltipMsg();
      }
    }
  }

}

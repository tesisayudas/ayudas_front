import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'g3-row-menu-switch',
  templateUrl: './row-menu-switch.component.html',
  styleUrls: ['./row-menu-switch.component.css']
})
export class RowMenuSwitchComponent implements OnInit {

  @Input() text: string;

  @Output() change = new EventEmitter();

  @Input() icon: string = null;

  constructor() {
  }

  ngOnInit() {
  }

  onClick(event) {
    this.change.next({
      event,
      label: null
    });
  }

}

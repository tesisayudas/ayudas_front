import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'g3-row-menu-action',
  templateUrl: './row-menu-action.component.html',
  styleUrls: ['./row-menu-action.component.css']
})
export class RowMenuActionComponent implements OnInit {

  @Input() text: string;

  @Output() rowClick = new EventEmitter();
  @Input() isDisabled: boolean = false;
  @Input() messageOnDisabled: string;
  @Input() icon: string = null;

  constructor() {
  }

  ngOnInit() {
  }

  onClick() {
    this.rowClick.next();
  }

}

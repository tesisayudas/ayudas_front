import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'g3-mobile-table',
  templateUrl: './mobile-table.component.html',
  styleUrls: ['../../../styles/list.scss']
})
export class MobileTableComponent implements OnInit, AfterViewInit {

  @Input() header: string;

  @ViewChild("table") table: ElementRef;

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.table.nativeElement.querySelector("tbody").addEventListener('scroll', () => {
      this.onScroll();
    })
  }

  onScroll() {
    let menu = this.table.nativeElement.querySelector(".is-visible > .mdl-menu");
    menu && menu["MaterialMenu"].hide();
  }
}

import {Component, Input, ViewChild} from '@angular/core';
import {PaginationItemComponent} from "../pagination-item/pagination-item.component";

@Component({
  selector: 'g3-table-container',
  templateUrl: './table-container.component.html',
  styleUrls: ['../../../styles/list.scss']
})
export class TableContainerComponent {

  @Input() service;

  @Input() totalItems;

  @ViewChild(PaginationItemComponent) paginationItemComponent: PaginationItemComponent;

  constructor() {
  }

  public get pagination() {
    return this.paginationItemComponent;
  }

}

import {Component, DoCheck, ElementRef, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'g3-row-menu',
  templateUrl: './row-menu.component.html',
  styleUrls: ['./row-menu.component.css']
})
export class RowMenuComponent implements DoCheck {
  @Input() id: string;
  @Input() enabled: boolean;
  @Input() device: string = '';
  @Input() canShow: boolean = true;
  @Input() i;
  @Input() totalItems: number;
  @ViewChild("menu") menu: ElementRef;
  @ViewChild('container') container: ElementRef;

  ngDoCheck(){
    const nativeElement: Element  = this.container && this.container.nativeElement;
    if (!nativeElement) return;
    const modals: NodeListOf<Element> = nativeElement.getElementsByClassName('mdl-menu__container is-upgraded');
    const noVisibleModals = Array.from(modals).filter((el: Element) => !el.className.includes('is-visible'));
    noVisibleModals.forEach((el: any) => {
      el.style.top = 0
    });
  }
}

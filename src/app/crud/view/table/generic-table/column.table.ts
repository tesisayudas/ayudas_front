import {Observable} from "rxjs";
import {HostListener} from "@angular/core";
import {BREAKPOINT} from "../../../../sori/elements";
import {PermissionService} from "@g3/core/auth/permission.service";
import {ActivatedRoute} from "@angular/router";
import {combineLatest} from "rxjs/observable/combineLatest";
import {map} from "rxjs/operators";
import {ITableEnable, ITableOptionItem} from "@g3/crud/view/table/generic-table/models";

export abstract class ColumnTable {

  public windowWidth: number = window.innerWidth;
  public breakPointTablet = BREAKPOINT.tablet;
  abstract permissionService: PermissionService;
  abstract route: ActivatedRoute;

  constructor() {
  }

  /**
   * Detect window size changes and set the value to windowWidth variable.
   * @param event
   */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = event.target.innerWidth
  }

  /**
   * Check if is mobile screen
   * @return {boolean}
   */
  public isMobile(): boolean {
    return this.windowWidth < this.breakPointTablet
  }

  public hasPermission(option: ITableOptionItem | ITableEnable): Observable<boolean> {
    const resource = option.resourceKey;
    const scopes = option.scopeKey;
    const permission = (scope: string) => resource ? this.permissionService.hasResourceWithScope(resource, scope) : this.permissionService.hasPermission(this.route, scope);
    if (Array.isArray(scopes)) return combineLatest(
      scopes.map(scope => permission(scope))
    ).pipe(map((results: boolean[]) => results.indexOf(false) === -1));
    else if (!!scopes) return  permission(scopes);
    return new Observable((observer) => observer.next(true));
  }

  public hasRolePermission(roles: string[]): Observable<boolean> {
    if (roles && Array.isArray(roles)) return this.permissionService.hasRolAccess(...roles);
    return new Observable((observer) => observer.next(true));
  }
}

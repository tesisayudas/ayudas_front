import {ITableOptions} from "./models";

export class TableOptionsBuilder {
  private options: ITableOptions = {};
  private currentOption: string;

  /**
   * Add an option which allow the table component present the available actions.
   * @param {string} option The available options are store in the availableTableOptions const.
   * @return {TableOptionsBuilder}
   */
  public add(option: string): TableOptionsBuilder {
    if (!availableTableOptions[option]) throw new Error(`TableComponent has not ${option} available.`)
    this.currentOption = option;
    this.options[this.currentOption] = {};
    this.key(option);
    this.label(`shared.${option}`);
    return this;
  }

  /**
   * Send a label to the current option. This is used to present a tooltip in the components
   * @param {string} label Key which will be translated by the platform.
   * @return {TableOptionsBuilder}
   */
  public label(label: string): TableOptionsBuilder {
    this.options[this.currentOption].label = label;
    return this;
  }

  /**
   * Used in switch components which has two states and need an alternative to tootlip message;
   * @param {string} label Key which will be translated by the platform.
   * @return {TableOptionsBuilder}
   */
  public alternativeLabel(label: string): TableOptionsBuilder {
    this.options[this.currentOption].alternativeLabel = label;
    return this;
  }

  /**
   * Return an object with the specified actions.
   * @return {ITableOptions}
   */
  public build(): ITableOptions {
    return this.options;
  }

  /**
   * Add a key which is included in the event emited when some available action is clicked;
   * @param {string} key
   * @return {TableOptionsBuilder}
   */
  public key(key: string): TableOptionsBuilder {
    this.options[this.currentOption].key = key;
    return this;
  }

}

export const availableTableOptions = {
  edit: 'edit',
  view: 'view',
  enabled: 'enabled',
};



import {TableEnable} from "./models";

export class TableEnableBuilder {
  private data: TableEnable;

  public key(key: string): TableEnableBuilder {
    this.data.key = key;
    return this;
  }

  public label(label: string): TableEnableBuilder {
    this.data.label = label;
    return this;
  }

  public labelOnDisabled(labelOnDisabled: string): TableEnableBuilder {
    this.data.labelOnDisabled = labelOnDisabled;
    return this;
  }

  public labelOnEnabled(labelOnEnabled: string): TableEnableBuilder {
    this.data.labelOnEnabled = labelOnEnabled;
    return this;
  }

  public scopoeKey(scope: string): TableEnableBuilder {
    this.data.scopeKey = scope;
    return this;
  }

  constructor() {
    this.data = new TableEnable();
  }

  public build(): TableEnable {
    return this.data;
  }
}

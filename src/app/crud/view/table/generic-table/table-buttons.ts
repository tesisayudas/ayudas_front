import {TableOptionItemBuilder} from "./table-option-item-builder";
import {ITableOptionItem, TableEnable} from "./models";
import {AVAILABLE_SCOPES} from "@g3/core/auth/permissions";
import {TableEnableBuilder} from "./table-enable-builder";

export class GenericButton {
  public static View(): ITableOptionItem {
    return new TableOptionItemBuilder()
      .key('view')
      .label('shared.show')
      .icon('eye')
      .scopeKey(AVAILABLE_SCOPES.show)
      .build()
  }

  public static edit(): ITableOptionItem {
    return new TableOptionItemBuilder()
      .key('edit')
      .label('shared.edit')
      .icon('pencil')
      .scopeKey(AVAILABLE_SCOPES.update, AVAILABLE_SCOPES.show)
      .build()
  }

  public static delete(): ITableOptionItem {
    return new TableOptionItemBuilder()
      .key('remove')
      .label('shared.remove')
      .icon('close-circle')
      .scopeKey(AVAILABLE_SCOPES.delete)
      .build()
  }

  public static changeStaus(): TableEnable {
    return  new TableEnableBuilder().build()
  }
}

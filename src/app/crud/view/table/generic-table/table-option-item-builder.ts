import {ITableOptionItem} from "./models";
import {TableOptionItem} from "./models";

export class TableOptionItemBuilder {

  private tableOptionItem: ITableOptionItem;

  constructor() {
    this.tableOptionItem = new TableOptionItem();
  }

  key(key: string): TableOptionItemBuilder {
    this.tableOptionItem.key = key;
    return this;
  }

  label(label: string): TableOptionItemBuilder {
    this.tableOptionItem.label = label;
    return this
  }

  canShow(canShow: (entity: any) => boolean): TableOptionItemBuilder {
    this.tableOptionItem.canShow = canShow;
    return this;
  }

  isDisabled(isDisabled: (entity: any) => boolean, labelOnDisabled: string): TableOptionItemBuilder {
    this.tableOptionItem.isDisabled = isDisabled;
    this.tableOptionItem.labelOnDisabled = labelOnDisabled;
    return this;
  }

  icon(icon: string): TableOptionItemBuilder {
    this.tableOptionItem.icon = icon;
    return this;
  }

  scopeKey(...scopeKey: string[]): TableOptionItemBuilder {
    this.tableOptionItem.scopeKey = scopeKey;
    return this;
  }

  resourceKey(resource: string): TableOptionItemBuilder {
    this.tableOptionItem.resourceKey = resource;
    return this;
  }

  build(): ITableOptionItem {
    return this.tableOptionItem;
  }

}

export class TableOptionsItemBuilder {
  private tableOptionsItems: ITableOptionItem[];

  constructor() {
    this.tableOptionsItems = [];
  }

  add(item: ITableOptionItem): TableOptionsItemBuilder {
    this.tableOptionsItems.unshift(item);
    return this
  }

  build(): ITableOptionItem[] {
    return this.tableOptionsItems;
  }
}

import {Type} from "@angular/core";
import {AVAILABLE_SCOPES} from "@g3/core/auth/permissions";
import {Observable} from "rxjs";
import {PermissionService} from "@g3/core/auth/permission.service";
import {ActivatedRoute} from "@angular/router";
import {ColumnTable} from "@g3/crud/view/table/generic-table/column.table";

export interface ITableOptions {
  edit?: ITableOption;
  view?: ITableOption;
  enabled?: ITableOption;
}

export interface ITableOption{
  key: string;
  label: string;
  alternativeLabel: string;
  canShow?: (entity: any) => boolean;
}

export interface ITableColumn{
  label: string;
  key: string;
  class: string;
  semaphore?: ISemaphore,
  showInMobile?: boolean;
  hasDot?: boolean;
  onlyRoles?: string[];
  colorKey: string;
  showAlways: boolean;
}

export class TableColumn implements ITableColumn {
  public class: string;
  public hasDot: boolean;
  public key: string;
  public label: string;
  public onlyRoles: string[];
  public semaphore: ISemaphore;
  public showInMobile: boolean;
  private permissionService: PermissionService;
  public hasRolePermission: Observable<boolean>;
  public showAlways: boolean;
  public colorKey: string;

  constructor(data: ITableColumn, permissionService) {
    this.class = data.class;
    this.hasDot = data.hasDot;
    this.key = data.key;
    this.label = data.label;
    this.onlyRoles = data.onlyRoles;
    this.semaphore = data.semaphore;
    this.showInMobile = data.showInMobile;
    this.showAlways = data.showAlways;
    this.colorKey = data.colorKey;
    this.permissionService = permissionService;
    this.hasRolePermission = this._hasRolePermission()
  }

  private _hasRolePermission(): Observable<boolean> {
    if (this.onlyRoles && Array.isArray(this.onlyRoles)) return this.permissionService.hasRolAccess(...this.onlyRoles);
    return new Observable((observer) => observer.next(true));
  }
}

export interface ITableFilter {
  component: Type<any>,
  roles?: string[];
}

export interface IOnActionClick {
  key: string;
  entity: any;
  event: any;
}

export interface ISemaphore {
  rules: [number, number, number];
  keys : {
    endDate: any;
    expectedEndDate: any;
  };
  id: string;
}

export interface ITableEnable {
  key: string;
  labelOnEnabled: string;
  labelOnDisabled: string;
  label: string;
  scopeKey: string;
  resourceKey: string;
}

export interface ITableOptionItem {
  key: string;
  label: string;
  labelOnDisabled: string;
  icon: string;
  scopeKey: string[];
  canShow: (entity: any) => boolean;
  isDisabled: (entity: any) => boolean;
  resourceKey: string;
}

export class GenericTableInputs {

  public static readonly WIDTH = {
    small: 90,
    big: 100
  };

  public filter: ITableFilter;
  public columns: ITableColumn[];
  public enable: ITableEnable;
  public options: ITableOptionItem[];
  public hasCounter: boolean;
  public width: number;

  constructor() {
    this.hasCounter = true;
    this.filter = null;
    this.columns = [];
    this.options = [];
    this.enable = null;
    this.width = GenericTableInputs.WIDTH.small;
  }
}

export class TableOptionItem implements ITableOptionItem {
  canShow: (entity: any) => boolean = () => true;
  isDisabled: (entity: any) => boolean = () => false;
  key: string = 'no-specified';
  label: string = 'no-specified';
  labelOnDisabled: string = 'no-specified';
  icon: string = 'bookmark';
  scopeKey: string[] = null;
  resourceKey: string = null;
}

export class OptionHandler extends ColumnTable{

  public canShow: boolean;
  public isDisabled: boolean;
  public _hasPermission: Observable<boolean>;
  constructor(
    public permissionService: PermissionService,
    public route: ActivatedRoute,
    public tableOption: TableOptionItem,
    private entity: any) {
    super();
    this.canShow = tableOption.canShow(entity);
    this._hasPermission = this.hasPermission(this.tableOption);
    this.isDisabled = this.tableOption.isDisabled(entity);
  }
}

export class TableEnable implements ITableEnable {
  key: string = 'enabled';
  scopeKey: string = AVAILABLE_SCOPES.changeStatus;
  resourceKey: string;
  label: string = 'shared.change_status';
  labelOnDisabled: string = 'shared.disable';
  labelOnEnabled: string = 'shared.enable';
}


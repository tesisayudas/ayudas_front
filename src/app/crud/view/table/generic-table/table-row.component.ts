import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from "@angular/core";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {GenericTableInputs, IOnActionClick, ITableColumn, ITableOptionItem, OptionHandler, TableColumn} from "./models";
import {ActivatedRoute} from "@angular/router";
import {PermissionService} from "@g3/core/auth/permission.service";
import {Observable} from "rxjs/Observable";
import {combineLatest} from "rxjs/observable/combineLatest";
import {map} from "rxjs/operators";
import {ColumnTable} from "@g3/crud/view/table/generic-table/column.table";

@Component({
  selector: 'g3-table-row',
  template: `
    <div class="g3-table--row">
      <!--Index-->
      <div *ngIf="!isMobile() && config?.hasCounter" class="g3-flex-grow--0">{{index}}</div>

      <!-- Data to show in table-->
      <ng-container *ngIf="_columns">
        <ng-container *ngFor="let columnData of _columns">
          <div *ngIf="(!(isMobile() && !columnData.showInMobile) && (columnData.hasRolePermission | async))" [class]="columnData.class">
            <ng-container *ngIf="columnData?.hasDot && !columnData?.semaphore">
              <i [ngStyle]="{'color': columnData.colorKey ? row[columnData.colorKey] : ((row.enabled === false) ? 'grey' : '')}"
                    *ngIf="columnData.showAlways || (isMobile() && columnData.showInMobile)"
                    class="material-icons mdi mdi-checkbox-blank-circle g3-simple-dot"></i>
            </ng-container>
            <ng-container *ngIf="!columnData?.hasDot && columnData?.semaphore">
              <g3-semaphore [endDate]="row[columnData.semaphore.keys.endDate]"
                            [id]="columnData.semaphore.id + row.id"
                            [range]="columnData.semaphore.rules"
                            [expectedEndDate]="row[columnData.semaphore.keys.expectedEndDate]"></g3-semaphore>
            </ng-container>
  
            <div *ngIf="row[columnData.key]?.languageKey; else simpleText">
              <p>{{row[columnData.key].languageKey | i18n}}</p>
            </div>
            <ng-template #simpleText>
              <p>{{getTextToShow(row[columnData.key])}}</p>
            </ng-template>
          </div>
        </ng-container>
        
      </ng-container>

      <!-- Desktop actions -->
      <ng-container *ngIf="!isMobile(); else mobile">
        <ng-container *ngIf="config?.enable">
          <div *ngIf="config?.enable && (hasPermission(config.enable) | async);" class="g3-flex--align-center">
            <g3-switch [checked]="row[config.enable.key]"
                       (change)="emitAction(row, config.enable.key, $event)"></g3-switch>
          </div>
        </ng-container>
        <div *ngIf="canShowTableOptions$ | async" class="g3-flex--align-center">
          <ng-container *ngFor="let option of _options">
            <g3-custom-icon [icon]="option.tableOption.icon"
                            *ngIf="option.canShow && (option._hasPermission | async)"
                            [isDisabled]="option.isDisabled"
                            (click)="emitAction(row, option.tableOption.key)"
                            [messageOnDisabled]="option.tableOption.labelOnDisabled"
                            [message]="option.tableOption.label"></g3-custom-icon>
          </ng-container>
        </div>
      </ng-container>

      <!-- Mobile actions -->
      <ng-template #mobile>
        <g3-row-menu [totalItems]="totalItems" *ngIf="showOptionsToggle$ | async" [canShow]="showOptionsMenuIcon | async" [i]="indexTable" [id]="row.id" g3UpgradeElements>

          <!-- Available options -->
          
          <ng-container *ngFor="let option of _options">
            <g3-row-menu-action [text]="option.tableOption.label"
                                [isDisabled]="option.isDisabled"
                                [messageOnDisabled]="option.tableOption.labelOnDisabled"
                                [icon]="option.tableOption.icon"
                                class="item-option-table"
                                *ngIf="option.canShow && (option._hasPermission | async)"
                                (click)="emitAction(row, option.tableOption.key)"></g3-row-menu-action>
          </ng-container>

          <!-- Enable config -->
          <ng-container *ngIf="config?.enable && (canShowEnable | async)">
            <g3-row-menu-switch (change)="emitAction(row, config.enable.key, $event)"
                                class="item-option-table"
                                *ngIf="!row.enabled"
                                [text]="config.enable.labelOnEnabled"></g3-row-menu-switch>
            <g3-row-menu-switch (change)="emitAction(row, config.enable.key, $event)"
                                class="item-option-table"
                                *ngIf="row.enabled"
                                [text]="config.enable.labelOnDisabled"></g3-row-menu-switch>
          </ng-container>

        </g3-row-menu>
      </ng-template>
    </div>
  `,
  styles: [`
    p {
      margin-bottom: 0;
      line-height: 17px !important;
      word-break: break-word
    }

    @media screen and (max-width: 479px) {
      p {
        font-size: 0.75rem;
      }
    }

    @media screen and (min-width: 480px) and (max-width: 940px) {
      p {
        font-size: 0.79rem;
      }
    }`]
})
export class TableRowComponent extends ColumnTable implements OnInit, OnChanges {
  @Input() index: number | string;
  @Input() indexTable: number | string;
  @Input() row: any;
  @Output() onActionClick: EventEmitter<IOnActionClick> = new EventEmitter<IOnActionClick>();
  @Input() config: GenericTableInputs;
  @Input() totalItems: number;
  public columns: TableColumn[];
  public _options;
  public _columns;
  public canShowEnable: Observable<boolean>;
  public showOptionsToggle$;
  public canShowTableOptions$;

  constructor(private translator: I18nService,
              public route: ActivatedRoute,
              public permissionService: PermissionService
  ) {
    super()
  }

  ngOnChanges(changes: SimpleChanges) {
    const {config, row} = changes;
    const _enable  = config && config.currentValue.enable;
    const _options: ITableOptionItem[] = config && config.currentValue.options;
    const _columns: ITableColumn[] = config && config.currentValue.columns;
    const _row = row && row.currentValue;

    if (_columns){
      this._columns = _columns.map(_column => new TableColumn(_column, this.permissionService))
    }

    if (_options){
      this._options = _options.map(option => new OptionHandler(this.permissionService,this.route,option,_row))
    }

    if (_enable) {
      this.canShowEnable = this.hasPermission(_enable);
    }
  }

  public getTextToShow(data): string {
    const type = typeof data;
    switch (type) {
      case 'boolean':
        return this.translator.getValue(data ? 'shared.yes' : 'shared.not');
      case 'object':
        if (Array.isArray(data)) return data.length.toString();
        return data;
      default:
        return data;
    }
  }

  ngOnInit() {
    this.canShowTableOptions$ = this.getCanShowOptions();
    const canShowToggle: Observable<boolean>[] = this.config.options.map(option => this.hasPermission(option));
    if (this.config.enable) canShowToggle.push(this.hasPermission(this.config.enable));
    this.showOptionsToggle$ = combineLatest(canShowToggle).pipe(map((listPermissions: boolean[]) => listPermissions.indexOf(true) > -1))
  }

  private getCanShowOptions(): Observable<boolean> {
    return combineLatest(this.config.options.map(option => this.hasPermission(option)))
      .pipe(map((listPermissions: boolean[]) => listPermissions.indexOf(true) > -1))
  }

  /**
   * Notifies the parent component about changes.
   * @param entity Item who is selected.
   * @param {string} key Reference to the acction to execute.
   * @param event Data from the event emited.
   * @public
   */
  public emitAction(entity: any, key: string, event: any = null) {
    this.onActionClick.emit({key, entity, event})
  }

  get showOptionsMenuIcon(): Observable<boolean> {
    return combineLatest(this.config.options.map(option => this.hasPermission(option)))
      .pipe(
        map(res =>
          res.map((item, i) => item ? this.config.options[i] : null)
            .filter(item => item)
        ),
        map(options =>
          options.map(option => option.canShow(this.row)).indexOf(true) > -1)
      );
  }

}

import {
  AfterViewInit,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {GenericTableInputs, IOnActionClick, ITableFilter} from "./models";
import {PaginationItemComponent} from "../pagination-item/pagination-item.component";
import {distinctUntilChanged, filter, map} from "rxjs/operators";
import {fromEvent} from "rxjs/observable/fromEvent";
import {Subscription} from "rxjs";
import {PermissionService} from "@g3/core/auth/permission.service";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'g3-table-custom',
  styleUrls: ['../../../styles/list.scss'],
  template: `
    <div class="g3-table-container">
      <div class="filter w-{{config.width}} w-100--tablet g3-margin-auto" *ngIf="(filter || config?.filter) && (canShowFilter)">
        <div class="filter__toggle">
          <button id="filter-btn"
                  class="mdl-card-menu mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect   g3-expandable__label"
                  (click)="onToggleFilter()">
            <i class="material-icons material-icons mdi mdi-filter"></i>
          </button>
          <span class="mdl-tooltip mdl-tooltip--left g3-capitalize" [attr.for]="'filter-btn'" g3UpgradeElements>
            {{'filter.action' | i18n}}
          </span>
        </div>
        <div class="filter__form">
          <ng-template #filterContainer></ng-template>
        </div>
      </div>
      <div
        class="animated fadeIn g3-margin-auto mdl-shadow--2dp g3-fab-partner g3-container__overflow-visible w-{{config?.width}} w-100--tablet">
        <div
          class="g3-scrollable-block g3-mdl-card__no-space g3-container__overflow-visible w-100">
          <div class="g3-table fixed-header w-100">
            <g3-table-header [config]="config"></g3-table-header>
            <div class="g3-table--body">
              <g3-table-row *ngFor="let row of itemList; let i=index" (onActionClick)="emitAction($event)"
                            [config]="config"
                            [totalItems]="itemList.length"
                            [index]="getIndex(i)"
                            [indexTable]="i"
                            [row]="row"></g3-table-row>
            </div>
            <g3-pagination-item [totalRegisters]="totalItems"></g3-pagination-item>
          </div>
        </div>
      </div>
    </div>
  `
})
export class TableComponent implements AfterViewInit, OnDestroy {
  @Input() filter: ITableFilter;
  @Input() itemList: any[];
  @Input() totalItems;
  @Input() service;
  @Output() onActionClick: EventEmitter<IOnActionClick> = new EventEmitter<IOnActionClick>();
  @ViewChild("filterContainer", {read: ViewContainerRef}) filterContainer;
  @ViewChild(PaginationItemComponent) paginationItemComponent: PaginationItemComponent;
  @Input() config: GenericTableInputs;
  public filterRef: ComponentRef<any>;
  private subScroll: Subscription;
  private subFilter: Subscription;
  public canShowFilter: boolean = true;

  constructor(private resolver: ComponentFactoryResolver,
              private permissionService: PermissionService
  ) {
  }

  ngAfterViewInit() {
    setTimeout(() => this.createFilter(), 0);
    const table: any = window.document.querySelector(".g3-table--body");
    this.subScroll = fromEvent(table, 'scroll')
      .pipe(
        map(() => table.querySelector(".is-visible > .mdl-menu")),
        filter(menu => menu && menu["MaterialMenu"] !== null),
        distinctUntilChanged()
      )
      .subscribe(menu => menu["MaterialMenu"].hide())
  }

  /**
   * Check if filter input has been send and create the object to filter config.
   *
   * @private
   * @return void
   */
  private createFilter(): void {
    let show$: Observable<boolean> =  (this.config.filter && this.config.filter.roles) ? this.permissionService.hasRolAccess(...this.config.filter.roles) : Observable.create(obs => obs.next(true));
    this.subFilter = show$.subscribe(res => {
      this.canShowFilter = res;
      if (this.config && this.config.filter && res) {
        const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.config.filter.component);
        this.filterRef = this.filterContainer && this.filterContainer.createComponent(factory);
      }
    });
  }

  /**
   * Send an event to the parent component about a entity request.
   * @param {IOnActionClick} event
   * @return void
   */
  public emitAction(event: IOnActionClick): void {
    this.onActionClick.emit(event)
  }

  /**
   * Calc index to the item list according to the current page.
   * @param {number} i
   * @return {number}
   * @public
   */
  public getIndex(i: number): number {
    return (i + (this.paginationItemComponent.currentPage * this.paginationItemComponent.resultsPerPage) + 1)
  }

  /**
   * Handle button behavior to display or not the filter form.
   */
  public onToggleFilter(): void {
    if (this.filterRef)
      this.filterRef.instance.canShow = !this.filterRef.instance.canShow;
  }

  ngOnDestroy() {
    if (this.subScroll) this.subScroll.unsubscribe();
    if (this.subFilter) this.subFilter.unsubscribe();
  }

}

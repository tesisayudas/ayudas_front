import {ISemaphore, ITableColumn} from "./models";

export class TableColumnsBuilder {
  private _columns: ITableColumn [] = [];

  public addColumn(column: ITableColumn) {
    this._columns.push(column);
    return this;
  }

  public build():ITableColumn[] {
    return this._columns;
  }
}

export class ColumnBuilder {
  private column: any = {};

  constructor(){
    this.column.class = "";
  }

  public label (label: string): ColumnBuilder {
    this.column.label = label;
    return this;
  }

  public key (key: string): ColumnBuilder {
    this.column.key = key;
    return this;
  }

  public styleClass (styleclass: string): ColumnBuilder {
    this.column.class += " " + styleclass;
    return this;
  }

  public semaphore (semaphore: ISemaphore): ColumnBuilder {
    this.column.semaphore = semaphore;
    return this;
  }

  public showInMobile (showInMobile: boolean = false): ColumnBuilder {
    this.column.showInMobile = showInMobile;
    return this;
  }

  public hasDot (hasDot: boolean = false): ColumnBuilder {
    this.column.hasDot = hasDot;
    return this;
  }

  public onlyRoles(roles: string[]): ColumnBuilder {
    this.column.onlyRoles = roles;
    return this;
  }

  public addColorKey(colorKey: string): ColumnBuilder {
    this.column.colorKey = colorKey;
    return this;
  }

  public showAlways(showAlways: boolean): ColumnBuilder {
    this.column.showAlways = showAlways;
    return this;
  }

  public build (): ITableColumn {
    return this.column;
  }
}

export class SemaphoreBuilder {
  private semaphore: any = {};

  public rules(red: number, orange: number, yellow: number): SemaphoreBuilder {
    this.semaphore.rules = [red, orange, yellow];
    return this;
  }

  public keys(endDate, expectedEndDate): SemaphoreBuilder {
    this.semaphore.keys = {endDate, expectedEndDate};
    return this;
  }

  public id(id: string): SemaphoreBuilder {
    this.semaphore.id = id;
    return this;
  }

  public builder(): ISemaphore {
    return this.semaphore;
  }
}

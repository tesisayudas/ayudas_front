import {GenericTableInputs, ITableColumn, ITableEnable, ITableFilter, ITableOptionItem} from "./models";

export class TableBuilder {

  private inputs: GenericTableInputs;

  constructor() {
    this.inputs = new GenericTableInputs();
  }

  width(width: number): TableBuilder {
    this.inputs.width = width;
    return this;
  }

  filter(filter: ITableFilter): TableBuilder {
    this.inputs.filter = filter;
    return this;
  }

  columns(columns: ITableColumn[]): TableBuilder {
    this.inputs.columns = columns;
    return this;
  }

  enable(enable: ITableEnable): TableBuilder {
    this.inputs.enable = enable;
    return this;
  }

  options(options: ITableOptionItem[]): TableBuilder {
    this.inputs.options = options;
    return this
  }

  hasCounter(hasCounter: boolean): TableBuilder {
    this.inputs.hasCounter = hasCounter;
    return this;
  }

  build(): GenericTableInputs {
    return this.inputs;
  }

}


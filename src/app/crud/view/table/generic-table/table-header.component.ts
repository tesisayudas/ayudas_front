import {Component, Input, OnInit} from "@angular/core";
import {GenericTableInputs} from "./models";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {combineLatest} from "rxjs/observable/combineLatest";
import {PermissionService} from "@g3/core/auth/permission.service";
import {ActivatedRoute} from "@angular/router";
import {ColumnTable} from "@g3/crud/view/table/generic-table/column.table";

@Component({
  selector: 'g3-table-header',
  styleUrls: ['../../../styles/list.scss'],
  template: `    
    <div class="g3-table--row g3-table--header">
      <div *ngIf="!isMobile() && config?.hasCounter" class="g3-flex-grow--0">#</div>
      <ng-container *ngFor="let column of config.columns" >
        <div *ngIf="(!(isMobile() && !column.showInMobile) && (hasRolePermission(column.onlyRoles) | async))" [class]="column.class">{{column.label | i18n}}</div>
      </ng-container>
      <div *ngIf="config?.enable && !isMobile() && (hasPermission(config.enable) | async)" class="g3-flex--align-center">{{config?.enable.label | i18n}}</div>
      <div *ngIf="config?.options.length > 0 && (canShowOptionLabel$ | async)"
           class="g3-flex--align-center">{{isMobile() ? '':'shared.options' | i18n}}</div>
    </div>
  `
})
export class TableHeaderComponent extends ColumnTable implements OnInit {

  @Input() config: GenericTableInputs;
  public canShowOptionLabel$: Observable<boolean>;


  constructor(public permissionService: PermissionService, public route: ActivatedRoute) { super() }

  ngOnInit(): void {
    this.canShowOptionLabel$ = combineLatest(this.config.options.map(option => this.hasPermission(option)))
      .pipe(map((listPermissions: boolean[]) => listPermissions.indexOf(true) > -1))
  }
}



import {Component, Input, OnInit} from '@angular/core';
import {PaginatorService} from "@g3/crud/model/item-crud-manager";

@Component({
  selector: 'g3-pagination-item',
  templateUrl: './pagination-item.component.html',
  styleUrls: ['./pagination-item.component.css']
})
export class PaginationItemComponent implements OnInit {
  @Input() paginatorId = 'paginatorId';
  @Input() resultsPerPage = 10;

  @Input() optionsResultsPerPage = [
    10,
    20,
    50,
    100
  ];

  currentPage = 0;

  @Input() totalRegisters = 10;

  @Input() paginator: ItemPaginator;

  constructor(private _paginator: PaginatorService) {
  }

  ngOnInit() {
    this.setResultsPerPage(10);
  }

  getStartPag() {
    const tmpStart = this.currentPage * this.resultsPerPage + 1;
    return tmpStart <= this.totalRegisters ? tmpStart : this.totalRegisters;
  }

  getEndPag() {
    const tmpEnd = (this.currentPage * this.resultsPerPage) + this.resultsPerPage;
    return tmpEnd <= this.totalRegisters ? tmpEnd : this.totalRegisters;
  }

  setResultsPerPage(option) {
    this.resultsPerPage = option;
    this._paginator.resultPerPage = this.resultsPerPage;
    this.setPage(0);
  }

  beforePage() {
    this.setPageAndUpdateList(this.currentPage - 1);
  }

  afterPage() {
    this.setPageAndUpdateList(this.currentPage + 1);
  }

  getLastPage() {
    let div = this.totalRegisters / this.resultsPerPage;
    if (div / 10 > 0) {
      div = Math.ceil(div);
    }
    return div - 1;
  }

  setPage(page) {
    this.currentPage = page;
    this._paginator.page = page;
  }

  setPageAndUpdateList(page) {
    this.setPage(page);
    this._paginator.updateList();
  }

  setResultsPerPageAndUpdateList(resultsPerPage) {
    this.setResultsPerPage(resultsPerPage);
    this._paginator.updateList();
  }
}

export interface ItemPaginator {
  setResultPerPage(resultPerPage: number);

  getResultPerPage(): number;

  setPage(page: number);

  getPage(): number;

  updateList(): void;
}

import {AfterViewInit, Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[g3ScrollableTable]'
})
export class ScrollableTableDirective implements AfterViewInit {

  private tableBody;

  constructor(private element: ElementRef) {
  }

  @HostListener('window:resize')
  public resize() {
    setTimeout(() => {
      const head = this.element.nativeElement.children[0];
      const firstRow = this.tableBody.querySelector("tr");
      if (firstRow) {
        const bodyCells = firstRow.children;
        let index = 0;
        for (let column of head.children[0].children) {
          let headWidth = column.getBoundingClientRect().width;
          let bodyWidth = bodyCells[index].getBoundingClientRect().width;
          if (headWidth > bodyWidth) {
            bodyCells[index].style.width = headWidth + 'px';
          } else {
            column.style.width = bodyWidth + 'px';
          }
          if (bodyCells[index].getBoundingClientRect().width != column.getBoundingClientRect().width) {
            column.style.width = bodyWidth + 'px';
          }
          if (this.tableBody.scrollHeight > this.tableBody.clientHeight && index == head.children[0].children.length - 1) {
            bodyCells[index].style.width = (bodyCells[index].getBoundingClientRect().width - 16) + 'px';
          }
          index++;
        }
      }
    });
  }

  ngAfterViewInit(): void {
    this.tableBody = this.element.nativeElement.children[1];
    this.resize();
    const config = {childList: true};
    new MutationObserver(() => this.resize())
      .observe(this.tableBody, config);
  }
}

import {Injectable} from '@angular/core';
import {ItemStateChanger} from "./controller-interfaces";
import {DialogService} from "@g3/core/dialogs/dialog/dialog.service";
import {BackItemCrudManager} from "../model/item-crud-manager";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";

@Injectable()
export class DialogStateChangerService implements ItemStateChanger {

  idDialog;

  label;

  message: string;

  private entity;

  constructor(private dialogService: DialogService,
              private toastService: SnackbarService,
              private i18Service: I18nService) {
  }

  private _service: BackItemCrudManager;

  set service(value: BackItemCrudManager) {
    this._service = value;
  }

  changeState(event, labelState, entity) {
    this.entity = entity;
    this.label = labelState;
    event.preventDefault();
    this.message = (this.entity.enabled) ? 'shared.disable_confirmation' : 'shared.enable_confirmation';
    this.dialogService.showDialogComponent(this.idDialog);
  }

  change() {
    this._service.enable(this.entity.id, !this.entity.enabled)
      .then((response) => {
        this.updateLabel(!this.entity.enabled);
        this.entity.enabled = !this.entity.enabled;
        this.toastService.showToast(this.i18Service.getValue('shared.status_changed'))
      })
      .catch((err) => console.log("error: ", err));
    this.dialogService.closeDialogComponent(this.idDialog);
  }

  private updateLabel(enabled: boolean) {
    if (enabled) {
      this.label.classList.add('is-checked');
    } else {
      this.label.classList.remove('is-checked');
    }
  }
}

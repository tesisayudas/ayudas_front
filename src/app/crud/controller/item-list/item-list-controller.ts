import {ItemDetailsDialog, ItemFormDialog, ItemStateChanger} from "../controller-interfaces";
import {ItemListModel} from "../../model/model-interfaces";

export class ItemListController {

  itemList = [];

  form: ItemFormDialog;

  stateChanger: ItemStateChanger;

  details: ItemDetailsDialog;

  constructor(private itemListModel: ItemListModel) {
    this.itemListModel.list$ && this.itemListModel.list$.subscribe(list => {
      this.itemList = list;
    });
    this.itemListModel.updateList();
  }

  changeState(data, entity) {
    this.stateChanger && this.stateChanger.changeState(data.event, data.label, entity);
  }

  showDetails(entity) {
    if(this.details) {
      this.details.setEntity(entity);
      this.details.open();
    }
  }

  edit(entity) {
    if(this.form) {
      this.form.setEntity(entity);
      this.openForm();
    }
  }

  openForm() {
    this.form && this.form.open();
  }
}

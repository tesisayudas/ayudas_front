import {AfterViewInit, OnDestroy} from '@angular/core';
import {ItemConfirmationDelete, ItemDetailsDialog, ItemFormDialog, ItemStateChanger} from "../controller-interfaces";
import {Subscription} from "rxjs/Subscription";
import {GenericTableInputs, IOnActionClick} from "@g3/crud/view/table/generic-table";
import {BackItemCrudManager, PaginatorService} from "@g3/crud/model/item-crud-manager";

export class BasicItemListSetupData {
  form: ItemFormDialog;
  details: ItemDetailsDialog;
  stateChanger: ItemStateChanger;
  deleteItem: ItemConfirmationDelete;

  constructor(form: ItemFormDialog, details: ItemDetailsDialog, stateChanger: ItemStateChanger, deleteItem?: ItemConfirmationDelete) {
    this.form = form;
    this.details = details;
    this.stateChanger = stateChanger;
    this.deleteItem = deleteItem;
  }
}

export abstract class BasicItemListComponent<T extends BackItemCrudManager> implements AfterViewInit, OnDestroy{

  public itemList = [];
  public form: ItemFormDialog;
  public stateChanger: ItemStateChanger;
  public details: ItemDetailsDialog;
  public deleteItem: ItemConfirmationDelete;
  public totalItems: number;
  public subscription: Subscription;
  public goToDetails: (data?: any) => any;
  public goToEdit: (data?: any) => any;
  abstract tableConfig: GenericTableInputs;
  abstract setUp(): BasicItemListSetupData;

  constructor(private itemListModel: T, public paginatorService: PaginatorService) {
    this.paginatorService.service = this.itemListModel;
    if (this.itemListModel.list$) {
      this.subscription = this.itemListModel.list$.subscribe(list => {
        this.totalItems = list.totalItems;
        this.itemList = list.items;
        this.itemListModel.actualTotalItems = this.totalItems;
      });
    }
  }

  ngAfterViewInit() {
    this.init();
    this.itemListModel.updateList();
  }

  init() {
    const setupData = this.setUp();
    if(setupData) {
      this.form = setupData.form;
      this.details = setupData.details;
      this.stateChanger = setupData.stateChanger;
      this.deleteItem = setupData.deleteItem;
    }
  }

  changeState(data, entity) {
    return this.stateChanger && this.stateChanger.changeState(data.event, data.label, entity);
  }

  showDetails(entity) {
    if(this.details) {
      this.paginatorService.service.getById(entity.id).then(res => {
        this.details.setEntity(res);
        this.details.open();
      });
    } else {
      this.goToDetails(entity);
    }
  }

  edit(entity) {
    if (this.form) {
      this.paginatorService.service.getById(entity.id).then(res => {
        this.form.setEntity(res);
        this.openForm();
      });
    } else {
      this.goToEdit(entity);
    }
  }

  delete(entity) {
    this.deleteItem && this.deleteItem.deleteConfirm(entity);
  }

  openForm() {
    this.form && this.form.open();
  }

  onActionClick(event: IOnActionClick): void {
    switch (event.key) {
      case 'edit':
        return this.edit(event.entity);
      case 'enabled':
        return this.changeState(event.event, event.entity);
      case 'view':
        return this.showDetails(event.entity);
      default:
        break;
    }
  }

  responseDatachanged(event) {
    const i = this.itemList.findIndex(res => res.id === event.id);
    if (i > -1) {
      this.itemList[i] = event;
      if(this.itemList[i].hasOwnProperty('closed')) this.itemList[i].enabled = !this.itemList[i].closed
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    if (this.paginatorService) this.paginatorService.service = null;
  }

}

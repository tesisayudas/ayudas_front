import {Injectable} from '@angular/core';
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {I18nService} from "@g3/core/i18n/i18n.service";
import {environment} from '../../../environments/environment';

@Injectable()
export class UserReporterService {

  private static readonly baseMsgKey = 'notification_right';

  private arrayActions: ItemAction[] = [
    new ItemAction('create'),
    new ItemAction('update'),
    new ItemAction('enable'),
    new ItemAction('disable')
  ];

  constructor(private snackbar: SnackbarService, private i18n: I18nService) {
  }

  private showMsgToUser(msgKey: string) {
    const message = this.i18n.getValue(msgKey);
    message && this.snackbar.showToast(message);
  }

  report(response: any, action: string | ItemAction) {
    if (response && action) {
      const todo: { find?: () => ItemAction, buildMsgKey: (ia) => string } = action instanceof ItemAction ?
        {buildMsgKey: ia => ia.msgKey} :
        {
          find: () => this.arrayActions.find(ia => ia.action == action),
          buildMsgKey: ia => UserReporterService.baseMsgKey + '.' + ia.msgKey
        };
      const finalAction: ItemAction = todo.find ? todo.find() : action as ItemAction;
      this.showMsgToUser(todo.buildMsgKey(finalAction));
      !environment.production && console.log(`UserReporterService:
        action = ${finalAction.action}
        response = ${JSON.stringify(response)}
      `);
    }
  }

}

export class ItemAction {
  action: string;
  msgKey: string;

  constructor(action: string, msgKey?: string) {
    this.action = action;
    this.msgKey = msgKey || action;
  }
}

import {AbstractControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import * as moment from 'moment';
import {BASIC_DATE_FORMAT} from "@g3/core/date-utils.service";

export const REGEX = {
  ONLY_LETTERS: '^[^!#\"\\\\$%&@=?¿¡*+~{}_:;.,0-9]+$',
  LETTERS_UTF8: '^[a-zA-ZñÑ]+',
  DOUBLE_NUMBER: '-?[0-9]+(\\.[0-9]+)?',
  INTEGER_NUMBER: '[0-9]+',
  URL: '^(((http)|(https)):\\/\\/)?[A-z0-9\\-]+\\.[A-z0-9\\-]+(\\.[A-z0-9\\-]+)*(\\/.*)?$',
  EMAIL: '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$',
  VALIDTEXT: '.*[A-Za-z_Ññ\\.]{3,}.*',
};

export const NUMBER = {
  maxNum: 2147483647
};

export abstract class CustomValidators {

  static validText(control: AbstractControl): ValidationErrors | null {
    const validator = Validators.pattern(REGEX.VALIDTEXT);
    return validator(control) ? {invalidText: true} : null;
  }

  static onlyInteger(control: AbstractControl): ValidationErrors | null {
    if (Number.isInteger(control.value)) return null;
    return { onlyInteger: true}
  }

  static requiredWithTrim(control: AbstractControl): ValidationErrors | null {
    if (!control.value || !control.value.toString().trim()) {
      return {requiredWithTrim: true}
    }
    return null;
  }

  static trim(control: AbstractControl): ValidationErrors | null {
    if (control.value && control.value.length > 0) {
      const trim = control.value.trim();
      return !trim ? {requiredWithTrim: true} : null;
    }
    return null;
  }

  static validId(control: AbstractControl): ValidationErrors | null {
    if(control.value && (control.value+"").startsWith("0")) {
      return {validId: true}
    }
    return null;
  }

  static positiveNumber(control: AbstractControl): ValidationErrors | null {
    const value: string = control && control.value ? control.value.toString() : '0';
    const numValidatorFn = Validators.pattern('[0-9]*(\\.[0-9]+)?');
    return ((Number(value) <= 0 && numValidatorFn(control))) ? {positiveNumber: true}: null;
  }

  static onlyLetters(control: AbstractControl): ValidationErrors | null {
    const validator = Validators.pattern('^[^!#\\"\\\\\\\\$%&@=?¿¡*+~{}_:;.,0-9]+$');
    return validator(control) ? {onlyLetters: true}: null;
  }

  static validEmail(control: AbstractControl): ValidationErrors | null {
    const validator = Validators.pattern(REGEX.EMAIL);
    return validator(control) ? {email: true} : null;
  }

  static validURL(control: AbstractControl): ValidationErrors | null {
    const validator = Validators.pattern(REGEX.URL);
    return validator(control) ? {url: true} : null;
  }

  static afterDate(dateBeforeControl: string, dateAfterControl): ValidationErrors | null {
    return (group: FormGroup) => {
      if (group.controls[dateBeforeControl].value && group.controls[dateAfterControl].value) {
        let afterControl = group.controls[dateAfterControl];
        let dateBefore = moment(group.controls[dateBeforeControl].value, BASIC_DATE_FORMAT);
        let dateAfter = moment(afterControl.value, BASIC_DATE_FORMAT);
        if (dateBefore && dateAfter && dateAfter.diff(dateBefore, 'days') < 1) {
          afterControl.setErrors({afterDate: true});
          return {afterDate: true};
        }
      }
      return null;
    };
  }

  static date(control: AbstractControl): ValidationErrors | null {
    const validator = Validators.pattern('^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$');
    return validator(control) ? {date: true} : null;
  }

  static endGreaterThanStartTimeAndAfterNow(fg: FormGroup) {
    const startControl = fg.get('startTime');
    const endControl = fg.get('endTime');
    const start = startControl.value;
    const end = endControl.value;
    const startDateStr = fg.parent && fg.parent.get('startDate').value;

    if (startDateStr && moment(`${startDateStr} ${start}`, "DD/MM/YYYY hh:mm").diff(moment(), "minute") <= 0) {
      startControl.markAsDirty();
      startControl.setErrors({beforeNow: true});
    } else {
      startControl.setErrors(null);
    } 
    
    if (moment(start, "hh:mm").diff(moment(end, "hh:mm"), "minute") >= 0) {
      endControl.markAsDirty();
      endControl.setErrors({afterTime: true});
    } else {
      endControl.setErrors(null);
    }
    return null;
  }
}
const contextErrorMsg = 'errors.';
const defaultErrorMsg = contextErrorMsg+'default';
const customErrorMessages: {errorName: string, getErrorMessage: (translate: {getValue: (string) => string}, data: any) => string}[] = [
  // here the more specific error message, for example:
  //{errorName: 'maxLength', getErrorMessage: (t, d) => `${t.getValue(contextErrorMsg+'maxLength'}: ${d.requiredMaxLength}`}
];
const errMsgRegister: {errorName: string, custom?: boolean}[] = [];
Object.keys(CustomValidators).forEach((k: string) => {
  errMsgRegister.push({errorName: k})
});

const customErrMsgRegister: {errorName: string, custom?: boolean}[] = customErrorMessages.map((err: any) => {
  err.custom = true;
  return err;
});

const validationErrors: {errorName: string, custom?: boolean}[] = errMsgRegister.map(err => {
  const customErr = customErrMsgRegister.find(e => e.errorName == err.errorName);
  return customErr || err;
}).concat(customErrMsgRegister.filter(err => {
  if(!errMsgRegister.find(e => e.errorName == err.errorName)){
    return err;
  }
}));

export function getValidationErrorMessage(errorName: string, translate: {getValue: (string) => string}, data: any) {
  const err = validationErrors.find(ve => ve.errorName == errorName);
  if (!err) {
    const messageError = translate.getValue(contextErrorMsg + errorName);
    return messageError || translate.getValue(defaultErrorMsg);
  }
  if (!err.custom) return translate.getValue(contextErrorMsg + err.errorName);
  return (err as any).getErrorMessage(translate, data);
}

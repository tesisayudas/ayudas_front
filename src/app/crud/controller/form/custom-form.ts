import {FormBuilder, FormGroup} from "@angular/forms";
import {ItemCrudManager} from "../../model/model-interfaces";

export class CustomForm {

  form: FormGroup;

  reflreshList: boolean =  true;

  submiter: ItemCrudManager;

  formStructure: { controlsConfig: { [p: string]: any }, extra: { [key: string]: any; } } | { [p: string]: any };

  prepareValue: (value) => any;

  updatePredicate: (value) => boolean = (value) => value.id;

  handlerSaveResponse: (response, action: 'create' | 'update' | null) => void;

  private initialFormState;

  constructor(private formBuilder: FormBuilder) {
  }

  initForm() {
    if (this.formStructure.hasOwnProperty('controlsConfig')) {
      const formConfiguration = this.formStructure as { controlsConfig: { [p: string]: any }, extra: { [key: string]: any; } };
      this.form = this.formBuilder.group(formConfiguration.controlsConfig, formConfiguration.extra);
    } else {
      this.form = this.formBuilder.group(this.formStructure);
    }
    this.initialFormState = this.form.value;
  }

  setValue(value) {
    this.form.patchValue(value);
  }

  save: () => void = () => {
    if (this.form.valid) {
      const value = this.prepareValue(this.form.value);
      if(!this.isEmpty(value)){
        if (this.updatePredicate(value)) {
          this.update(value);
        }
        else {
          this.create(value);
        }
      }
    }
  };

  update: (entity) => void = (entity) => {
    this.submiter.update(entity, this.reflreshList).then(response => this.handlerSaveResponse(response, 'update'));
  };

  create: (entity) => void = (entity) => {
    this.submiter.create(entity, this.reflreshList).then(response => this.handlerSaveResponse(response, 'create'));
  };

  reset() {
    this.form && this.form.reset();
  }

  isEmpty(object: Object) {
    const keys = Object.keys(object);
    for(let prop of keys){
      if(typeof object[prop] === "object" && object[prop] != null){
        let empty = this.isEmpty(object[prop]);
        if(!empty) return false;
      } else if(!!object[prop]){
        return false;
      }
    }
    return true;
  }

}

export class CustomFormBuilder {

  private customForm: CustomForm;

  constructor(formBuilder: FormBuilder) {
    this.customForm = new CustomForm(formBuilder);
  }

  refreshList(refreshList: boolean): CustomFormBuilder{
    this.customForm.reflreshList = refreshList;
    return this;
  }

  submiter(submiter: ItemCrudManager): CustomFormBuilder {
    this.customForm.submiter = submiter;
    return this;
  }

  formStructure(formStructure: { controlsConfig: { [p: string]: any }, extra: { [key: string]: any; } } | { [p: string]: any }): CustomFormBuilder {
    this.customForm.formStructure = formStructure;
    return this;
  }

  prepareValue(prepareValue: (value) => any): CustomFormBuilder {
    this.customForm.prepareValue = prepareValue;
    return this;
  }

  updatePredicate(updatePredicate: (value) => boolean): CustomFormBuilder {
    this.customForm.updatePredicate = updatePredicate;
    return this;
  }

  // handlerErrorResponse(handlerErrorResponse: (error) => void): CustomFormBuilder {
  //   this.customForm.handlerErrorResponse = handlerErrorResponse;
  // }

  handlerSaveResponse(handlerSaveResponse: (response, action: 'create' | 'update') => void): CustomFormBuilder {
    this.customForm.handlerSaveResponse = handlerSaveResponse;
    return this;
  }

  build(): CustomForm {
    this.customForm.initForm();
    return this.customForm;
  }

  create(saveFn: (entity) => void): CustomFormBuilder {
    this.customForm.create = saveFn;
    return this;
  }

  update(saveFn: (entity) => void): CustomFormBuilder {
    this.customForm.update = saveFn;
    return this;
  }

}

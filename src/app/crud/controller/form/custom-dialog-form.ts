import {CustomForm} from "./custom-form";
import {DialogComponent} from "../../../shared/dialogs/components/dialog/dialog.component";
import {Subscription} from "rxjs/Subscription";
import {I18nService} from "@g3/core/i18n/i18n.service";

export class CustomFormDialog {
  updateTitleKey;
  createTitleKey;
  customForm: CustomForm;

  private translator: I18nService;

  public set i18n(i18n: I18nService) {
    this.translator = i18n;
    i18n.translator$.subscribe(() => this.updateTitleFn());
  }

  dialogSubscription: Subscription;

  private dialogComponent_: DialogComponent;

  public set dialogComponent(dialogComponent: DialogComponent) {
    this.dialogComponent_ = dialogComponent;
    if(dialogComponent) {
      if(this.dialogSubscription) {
        this.dialogSubscription.unsubscribe();
      }
      this.dialogSubscription = dialogComponent.status.subscribe(state => {
        if (state=='closed') {
          this.handlerCloseDialog();
        }
      });
    }
  }

  private title_;
  public get title() {
    return this.title_;
  }

  constructor() {}

  updateTitleFn: () => void = () => {
    if (this.customForm && this.translator) {
      const titleKey = this.customForm.updatePredicate(this.customForm.form.value) ? this.updateTitleKey : this.createTitleKey;
      if(titleKey) this.title_ = this.translator.getValue(titleKey);
    }
  };

  handlerCloseDialog: () => void = () => {
    if(this.customForm) {
      this.customForm.reset();
    }
    this.updateTitleFn();
  };

  setValue: (value) => void = (value) => {
    if(this.customForm && this.customForm.form){
      this.customForm.setValue(value);
      this.updateTitleFn();
    }
  };

  open() {
    this.dialogComponent_.open();
  }

  close(): void {
    this.dialogComponent_.close();
  }
}

export class CustomDialogFormBuilder {

  private customDialogForm: CustomFormDialog;

  constructor() {
    this.customDialogForm = new CustomFormDialog();
  }

  private returnThis(): CustomDialogFormBuilder {
    return this;
  }

  updateTitleKey(updateTitleKey: string) {
    this.customDialogForm.updateTitleKey = updateTitleKey;
    return this.returnThis();
  }

  createTitleKey(createTitleKey: string) {
    this.customDialogForm.createTitleKey = createTitleKey;
    return this.returnThis();
  }

  customForm(customForm: CustomForm) {
    this.customDialogForm.customForm = customForm;
    return this.returnThis();
  }

  translator(translator: I18nService) {
    this.customDialogForm.i18n = translator;
    return this.returnThis();
  }

  updateTitleFn(updateTitleFn: () => void) {
    this.customDialogForm.updateTitleFn = updateTitleFn;
    return this.returnThis();
  }

  handlerCloseDialog(handlerCloseDialog: () => void) {
    this.customDialogForm.handlerCloseDialog = handlerCloseDialog;
    return this.returnThis();
  }

  setValue(setValue: (value) => void) {
    this.customDialogForm.setValue = setValue;
    return this.returnThis();
  }

  build(): CustomFormDialog {
    this.customDialogForm.updateTitleFn();
    return this.customDialogForm;
  }

}

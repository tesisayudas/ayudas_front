import {FormGroup} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";

export class CustomFormValidator {

  private customComparatorWithInitialState: CustomComparatorWithInitialState;

  private formSubscription: Subscription;

  private valid_ = false;

  private _updateCaseSensitive: boolean = false;


  get updateCaseSensitive(): boolean {
    return this._updateCaseSensitive;
  }

  set updateCaseSensitive(value: boolean) {
    this._updateCaseSensitive = value;
  }

  public get valid() {
    if (this.form_ && !this.formSubscription) {
      return this.form_.valid;
    }
    return this.valid_;
  }

  private invalid_ = !this.valid;

  public get invalid() {
    if (this.form_ && !this.formSubscription) {
      return this.form_.invalid;
    }
    return this.invalid_;
  }

  private form_: FormGroup;

  public setForm(form: FormGroup) {
    this.form_ = form;
  }

  private setInitialState(initialState) {
    const objectToCompare = new ObjectToCompare(initialState, true, true);
    this.customComparatorWithInitialState = new CustomComparatorWithInitialState(objectToCompare);
  }

  public active() {
    this.formSubscription = this.form_.valueChanges.subscribe((value) => {
      this.validate(value)
    });
    this.setInitialState(this.form_.value);
    this.validate(this.form_.value);
  }

  public inactive() {
    if (this.formSubscription) {
      this.formSubscription.unsubscribe();
      this.formSubscription = null;
    }
  }

  public get initialStatePrepared() {
    return this.customComparatorWithInitialState.initialState;
  }

  private cachedValued_;

  public get cachedValue() {
    return this.cachedValued_;
  }

  public validate(currentValue) {
    this.cachedValued_ = currentValue;
    this.valid_ = this.form_.valid && !this.customComparatorWithInitialState.equalsLikeStringTo(new ObjectToCompare(currentValue, true, true), this._updateCaseSensitive);
    this.invalid_ = !this.valid;
  }
}

export class CustomComparatorWithInitialState {
  private initialState_: ObjectToCompare;

  public get initialState() {
    return this.initialState_;
  }

  private customComparator: CustomObjectComparator;

  constructor(initialState: ObjectToCompare) {
    this.initialState_ = initialState;
    this.customComparator = new CustomObjectComparator();

    this.customComparator.prepareObjectToCompare(this.initialState_);
  }

  public equalsLikeStringTo(currentState: ObjectToCompare, caseSensitive: boolean = true): boolean {
    return this.customComparator.equalsLikeString(this.initialState_, currentState, caseSensitive);
  }
}

export class CustomObjectComparator {

  private objectTransformer: ObjectTransformer = new ObjectTransformer();

  public equalsLikeString(a: ObjectToCompare, b: ObjectToCompare, caseSensitive: boolean = true): boolean {
    this.prepareObjectToCompare(a);
    this.prepareObjectToCompare(b);

    let strA = JSON.stringify(a.object);
    let strB = JSON.stringify(b.object);

    if(caseSensitive) {
      strA = strA.toLowerCase();
      strB = strB.toLowerCase();
    }

    return strA == strB;
  }

  public prepareObjectToCompare(objectToCompare: ObjectToCompare) {
    if (!objectToCompare.isPrepared()) {
      objectToCompare.toJson && this.cloneAttributeObject(objectToCompare);
      if (objectToCompare.attributesToString) {
        this.objectTransformer.travelAttributes(objectToCompare.object, (element) => (element + "").trim());
        this.objectTransformer.travelAttributes(objectToCompare.object, (element) => (element + "").trim());
      } else {
        this.objectTransformer.trimObject(objectToCompare.object);
      }
      objectToCompare.setPrepared(true);
    }
  }

  public cloneAttributeObject(element: { object: Object }) {
    element.object = this.clone(element.object);
  }

  public clone(object): any {
    return JSON.parse(JSON.stringify(object));
  }

  public equalAInB(a, b) {
    const aKeys = Object.keys(a);
    for (let i = 0; i < aKeys.length; i++) {
      const valueA = a[aKeys[i]];
      const valueB = b[aKeys[i]];
      if (valueA instanceof Array) {
        for (let j = 0; j < valueA.length; j++) {
          this.equalAInB(valueA[j], valueB[j]);
        }
      } else if (valueA instanceof Object) {
        this.equalAInB(valueA, valueB);
      } else {
        if(valueA != valueB) return false;
      }
    }
    return true;
  }

}

export class ObjectTransformer {

  public toUpperCaseObject(object) {
    this.travelAttributes(object, element => {
      if (typeof element == 'string' || element instanceof String) {
        return element.toString().toUpperCase();
      }
      return element;
    });
  }

  public trimObject(object) {
    this.travelAttributes(object, (element) => element.trim());
  }

  public attributesToString(object: Object): any {
    this.travelAttributes(object, (element) => element + "");
  }

  public travelAttributes(object: Object, action: (element) => any): any {
    for (let key in object) {
      if (object[key] instanceof Array)
        object[key].forEach(o => this.travelAttributes(o, action));
      else if (object[key] instanceof Object)
        this.travelAttributes(object[key], action);
      else {
        object[key] = action(object[key]);
      }
    }
    return object;
  }
}

export class ObjectToCompare {
  object: Object;
  attributesToString: boolean;
  toJson: boolean;
  private prepared: boolean;

  constructor(object: Object, attributesToString: boolean = true, toJson: boolean = true) {
    this.object = object;
    this.attributesToString = attributesToString;
    this.toJson = toJson;
    this.prepared = false;
  }

  public setPrepared(prepared: boolean) {
    this.prepared = prepared;
  }

  public isPrepared(): boolean {
    return this.prepared;
  }
}

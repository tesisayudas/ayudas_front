import {CustomForm} from "./custom-form";
import {CustomFormValidator} from "./custom-form-validator";
import {CustomFormDialog} from "./custom-dialog-form";
import {isNumeric} from "rxjs/util/isNumeric";
import {AbstractControl, FormArray, FormControl, FormGroup} from "@angular/forms";
import {DefaultDate} from "../../../shared/form-control/form-control-date/date-picker";
import * as moment from 'moment';
import {I18nService} from "@g3/core/i18n/i18n.service";
import {SnackbarService} from "@g3/core/snackbar/snackbar.service";
import {ErrorMessageDialogComponent} from "../../view/error-message-dialog/error-message-dialog.component";

export class CustomFormUtils {

  static addEditNoChangeValidator(customForm: CustomForm): CustomFormValidator {
    const customFormValidator: CustomFormValidator = new CustomFormValidator();
    customFormValidator.setForm(customForm.form);
    const defaultSave = customForm.save;
    customForm.save = () => customFormValidator.valid && defaultSave();
    return customFormValidator;
  }

  static addEditNoChangeValidatorInDialog(customFormDialog: CustomFormDialog, customFormValidator: CustomFormValidator) {
    const defaultHandlerCloseDialog = customFormDialog.handlerCloseDialog;
    customFormDialog.handlerCloseDialog = () => {
      defaultHandlerCloseDialog();
      customFormValidator.inactive();
    };
    const defaultSetValue = customFormDialog.setValue;
    customFormDialog.setValue = (value) => {
      defaultSetValue(value);
      customFormValidator.active();
    };
  }

  static addEditValidationToFormDialog(customFormDialog: CustomFormDialog): { customFormDialog: CustomFormDialog, customFormValidator: CustomFormValidator } {
    const customFormValidator: CustomFormValidator = CustomFormUtils.addEditNoChangeValidator(customFormDialog.customForm);
    CustomFormUtils.addEditNoChangeValidatorInDialog(customFormDialog, customFormValidator);
    return {customFormDialog: customFormDialog, customFormValidator: customFormValidator};
  }

  static handlerSaveError(form: any, response, action: 'create' | 'update', i18n: I18nService, snackService?: SnackbarService, messagedialog?: ErrorMessageDialogComponent) {
    const isCustomForm = form instanceof CustomForm;
    const _form = isCustomForm ? form.form : form.customForm.form;
    if (!isNumeric(response)) {
      if (response != null && !response.id) {
        if (!isCustomForm && messagedialog) {
          form.close();
          messagedialog.open();
        }
        else {
          if (typeof response.error === 'string') response.error = JSON.parse(response.error);
          return this.handlerFormErrors(_form, response, i18n);
        }
      } else {
        if (!isCustomForm) form.close();
        if(i18n && snackService) this.showToastConfirmation(action, i18n, snackService);
      }
    }
  }

  static handlerFormErrors(form, response, i18n: I18nService) {
    let nonError = true;
    const errors = response.error && response.error.errors && Array.isArray(response.error.errors) ?
      response.error.errors.reduce((acc, cur, i)=> {
        acc[cur] = response.error.description;
        return acc;
      }, {}) : response.error;
    for (let er in errors) {
      if (response.status == 400) {
        if (form.get(er)) {
          form.get(er).setErrors({required: true});
          nonError = false;
        }else if(er.includes('[')){
          const subString = er.split('[');
          const pos = subString[1].split(']')[0];
          const errorControl = subString[1].split('.')[1];
          const control = form.get(subString[0]);
          const errorName = errors[er][0].toString();
          if(control && control.controls && errorControl){
            if (((control.controls as FormArray)[pos]) instanceof FormGroup){
              const haveErrorName = !!errorName ? i18n.getValue(`errors.${errorName}`): null;
              if(haveErrorName){
                const err = {};
                err[errorName] = true;
                ((control.controls as FormArray)[pos] as FormGroup).get(errorControl).setErrors(err);
              }else{
                ((control.controls as FormArray)[pos] as FormGroup).get(errorControl).setErrors({required:true});
              }
            } else {
              (control.controls as FormArray)[pos].setErrors({required: true});
            }
          }
          nonError = false;
        }
      } else if (response.status == 409) {
        if (typeof er === 'string' && form.get(er) instanceof FormControl) {
          form.get(er).setErrors({exist: true});
          nonError = false;
        }
      }
    }
    return nonError;
  }

  static showToastConfirmation(action: 'create' | 'update' | 'deleted' | 'filter' | 'reset', i18n: I18nService, snack: SnackbarService) {
    switch (action) {
      case 'create':
        return snack.showToast(i18n.getValue('shared.create_success'));
      case 'update':
        return snack.showToast(i18n.getValue('shared.update_success'));
      case 'deleted' :
        return snack.showToast(i18n.getValue('shared.deleted_success'));
      case 'filter':
        return snack.showToast(i18n.getValue('shared.filter_success'));
      case 'reset' :
        return snack.showToast(i18n.getValue('shared.reset_success'));
      default:
        break;
    }
  }

  static removeEmptyData(object: Object): Object {
    for (let attr in object) {
      if (object[attr] == null || object[attr] == "") {
        delete object[attr];
      }
    }
    return object;
  }

  static isPastDate(date: string): boolean {
    const newDate = moment(date, 'DD/MM/YYYY');
    const today = moment();
    return (today.diff(newDate, 'days') > 0);
  }


  static isFutureDate(date: string): boolean {
    const newDate = moment(date, 'DD/MM/YYYY');
    const now = moment();
    const nowString = now.date() + '/'+ (now.month()+1) +'/'+now.year();
    const today = moment(nowString, 'DD/MM/YYYY');
    return (today.diff(newDate, 'days') < 0);
  }

  static createDate(object: DefaultDate | null, control: AbstractControl) {
    object = new DefaultDate();
    control.valueChanges.subscribe(valor => {
      object.past = moment(valor, 'DD/MM/YYYY');
      return object;
    });
    return object;
  }
}

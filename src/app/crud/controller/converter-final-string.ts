export class ConverterFinalString {
  private _priority;
  private _literal;
  private _key;
  private _translator;

  public priority(priority: 'key' | 'literal') {
    this._priority = priority;
    return this;
  }

  public literal(literal: string) {
    this._literal = literal;
    return this;
  }

  public key(key: string) {
    this._key = key;
    return this;
  }

  public translator(translator: {getValue: (string) => string}) {
    this._translator = translator;
    return this;
  }

  public getFinalString() {
    if(!this._priority) throw new Error('ConverterFinalString - You should to set the priority');
    if(!this._translator) throw new Error('ConverterFinalString - You should to set the translator.');

    if(this._priority == 'literal' && this._literal) {
      return this._literal;
    }
    if(this._priority == 'key' && this._key) {
      return this._translator.getValue(this._key);
    }
    if(this._literal) {
      return this._literal;
    }
    if(this._key) {
      return this._translator.getValue(this._key);
    }
    return '';
  }
}

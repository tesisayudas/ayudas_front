export abstract class ItemDialog {
  abstract setEntity(entity);
  abstract open();
}

// noinspection JSAnnotator
export interface ItemDetailsDialog extends ItemDialog {}

// noinspection JSAnnotator
export interface ItemFormDialog extends ItemDialog {}

export interface ItemStateChanger {
  changeState(event, labelState, entity);
}

export interface ItemConfirmationDelete {
  deleteConfirm(entity);
}

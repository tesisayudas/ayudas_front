export class CustomMap {
  key: string;
  dataArray: Array<any>;
  requiredValue: string;
  dataMap = new Map<string, any>();

  constructor(dataArray: Array<any>, key: string, requiredValue: string) {
    if(!dataArray || !dataArray[0]) throw new Error('CustomMap -> dataArray cannot be (null | undefined | []).');
    this.dataArray = dataArray;
    if(!key) throw new Error('CustomMap -> key cannot be (null | undefined | "" | "0")');
    if(!(key in dataArray[0])) throw new Error('CustomMap -> key is a invalid field (not exit in the elements of dataArray).');
    this.key = key;
    if(!requiredValue) throw new Error('CustomMap -> requiredValue cannot be (null | undefined | "" | "0")');
    if(!(requiredValue in dataArray[0])) throw new Error('CustomMap -> requiredValue is a invalid field (not exit in the elements of dataArray).');
    this.requiredValue = requiredValue;
  }

  get(keyValue: string | any): any {
    keyValue = keyValue + "";
    let value = this.dataMap.get(keyValue);
    if (!value) {
      const element = this.dataArray.find(d => d[this.key] == keyValue);
      if (!element) throw new Error('CustomMap -> The element not exist in dataArray.');
      value = element[this.requiredValue];
      this.dataMap.set(keyValue, value);
    }
    return value;
  }
}

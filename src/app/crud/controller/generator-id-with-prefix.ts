var consecutiveId = 1;

export function generateIdWithPrefix(prefix: string) {
  if(!prefix) throw new Error('GeneratorIdWithPrefix - You should to set the prefix.');
  return prefix + (consecutiveId++);
}

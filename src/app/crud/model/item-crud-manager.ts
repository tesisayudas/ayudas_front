import {Subject} from "rxjs/Subject";
import {GetAllUrlParameters} from "./item-crud.service";
import {ItemCrudManager} from "./model-interfaces";
import {Observable} from "rxjs/Observable";
import {HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {HttpUtils, ManageUrls} from "@g3/crud/model/http.utils";
import {Router} from "@angular/router";

export class BackItemCrudManager implements ItemCrudManager {

  public listSource = new Subject<any>();
  public list$: Observable<any> = this.listSource.asObservable();
  public actualTotalItems: number;
  public defaultCrudUrls: ManageUrls;
  public resource: { route: string, back: string };

  constructor(public _http: any, resource: any, public _router: Router) {
    this.resource = resource;
    this.defaultCrudUrls = HttpUtils.manageCrudUrls(resource);
  }

  enable(id: any, enabled: boolean): Promise<any> {
    const url: string = `${this.defaultCrudUrls.enable()}${id}`;
    const observe = 'response';
    return this._http
      .patch(url, {enabled}, {observe})
      .toPromise()
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  disable(id: any): Promise<any> {
    const url: string = `${this.defaultCrudUrls.create()}`;
    return this._http
      .delete(url)
      .toPromise()
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  create(item: any, refreshTable: boolean = true): Promise<any> {
    const url: string = `${this.defaultCrudUrls.create()}`;
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    const responseType = 'text';
    const options = (refreshTable) ? {headers, responseType} : {headers};
    return this._http
      .post(url, item, options)
      .toPromise()
      .then((item) => {
        if (refreshTable) this.updateList();
        else return Promise.resolve(item);
      })
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  update(item: any, refreshTable: boolean = true): Promise<any> {
    const url: string = `${this.defaultCrudUrls.update()}${item.id}`;
    const responseType = 'text';
    return this._http
      .put(url, item, {responseType})
      .toPromise()
      .then(() => {refreshTable ? this.updateList() : null})
      .catch((error) => HttpUtils.handleError(error, this._router));

  }

  getById(id): Promise<any> {
    const url: string = `${this.defaultCrudUrls.getById() + id}`;
    return this._http
      .get(url)
      .toPromise()
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  getAll(): Promise<any> {
    const url: string = `${this.defaultCrudUrls.getAll()}`;
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    const observe = 'response';
    return this._http
      .get(url, {headers, observe})
      .toPromise()
      .then(res => {
        const totalItems = res.headers.get('X-Total-Count');
        const items = res.body;
        return Promise.resolve({totalItems, items})
      })
      .catch((error) => HttpUtils.handleError(error, this._router));
  };

  updateList(): void {
    this.getAll().then(all => {
      this.listSource.next(all);
    });
  }

  getEnabledList(): Promise<any> {
    const url: string = `${this.defaultCrudUrls.getEnabledList()}`;
    return this._http
      .get(url)
      .toPromise()
      .catch((error) => HttpUtils.handleError(error, this._router));
  }

  deleted(id: number, _paginator?: PaginatorService): Promise<any> {
    const url: string = `${this.defaultCrudUrls.disable()}`;
    const responseType = 'text';
    return this._http
      .delete(url + id, {responseType})
      .toPromise()
      .then((res) => {
        if (_paginator) {
          if (this.actualTotalItems == ((_paginator.page * _paginator.resultPerPage) + 1)) {
            _paginator.page = _paginator.page - 1;
          }
          _paginator.updateList();
        } else {
          Promise.resolve(res)
        }

      });
  }

  setIds(ids: number[], getAllUrlParameters: GetAllUrlParameters = {
    page: 0,
    resultsPerPage: 10
  }, resource = this.resource) {
    this.defaultCrudUrls = HttpUtils.manageCrudUrls(resource, ids, getAllUrlParameters);
  }

  setFilters(filters: Map<string, string>) {
    if (this.defaultCrudUrls.parameters.filter instanceof Map) {
      filters.forEach((value, key, map) => this.defaultCrudUrls.parameters.filter.set(key, value));
      return;
    }
    this.defaultCrudUrls.parameters.filter = filters;
  }

  clearFilters() {
    this.defaultCrudUrls.parameters.filter = null;
  }

}

@Injectable()
export class PaginatorService {

  private _service: BackItemCrudManager;

  updateList() {
    this.checkService(() => this._service.updateList());
  }

  set service(service: BackItemCrudManager) {
    this._service = service
  }

  get service(): BackItemCrudManager {
    return this._service
  }

  set resultPerPage(resultPerPage: number) {
    this.checkService(() => this._service.defaultCrudUrls.parameters.resultsPerPage = resultPerPage);
  }

  get resultPerPage(): number {
    return this.checkService(() => this._service.defaultCrudUrls.parameters.resultsPerPage);
  }

  set page(page: number) {
    this.checkService(() => this._service.defaultCrudUrls.parameters.page = page);
  }

  get page(): number {
    return this.checkService(() => this._service.defaultCrudUrls.parameters.page)
  }

  checkService(_function: Function) {
    if (this._service instanceof BackItemCrudManager) return _function();
    throw new Error("service is required. It must be initialized on ngOnInit like this.paginator.service = service")
  }

}

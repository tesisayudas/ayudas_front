import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import {environment} from '../../../environments/environment';

// @Injectable()
// export class BackHttpService {
//
//   //TODO : Pueden change a lo largo del proyecto al implementar la conexion con IAESTE, keycloak y I18N
//   private headers = new HttpHeaders({'Content-Type': 'application/json'});
//
//   private baseUrl = environment.sori.backBaseUrl.endsWith('/') ? environment.sori.backBaseUrl.slice(0, -1) : environment.sori.backBaseUrl;
//
//   constructor(private http: HttpClient) {
//   }
//
//   public get(url) {
//     return this.manageResponse(this.http.get(this.addBaseUrl(url)));
//   }
//
//   public setBaseUrl(value: string){
//     this.baseUrl = value;
//   }
//
//   //TODO : Cambiar el Manejor de Errores
//   public static handleError(error: any): Promise<any> {
//     if (error.status != 0) {
//       if (error.status == 409 || error.status == 400) {
//         let object = error.error;
//         object.status = error.status;
//         return Promise.resolve(object);
//       } else if (error.status == 404) {
//         return Promise.resolve(error.status);
//       } else if (error.status == 412) {
//         let object = error.error;
//         object.status = error.status;
//         return Promise.resolve(object);
//       } else if(error.status == 424){
//         return Promise.resolve(error.error.status);
//       }
//     } else {
//       error.status = 500;
//
//     }
//   }
//
//   //TODO : Agregar el objeto que se va enviar a la URL con el id, y el valor a modificar (enabled)
//   public patch(url, body) {
//     return this.managePatchResponse(this.http.patch(this.addBaseUrl(url), body, {
//       headers: this.headers,
//       observe: 'response'
//     }));
//   }
//
//   public delete(url) {
//     return this.manageResponse(this.http.delete(this.addBaseUrl(url)));
//   }
//
//   public list(url) {
//     return this.manageList(this.http.get(this.addBaseUrl(url), {headers: this.headers, observe: 'response'}));
//   }
//
//   public post(url, body) {
//     return this.manageResponse(this.http.post(this.addBaseUrl(url), body, {headers: this.headers}));
//   }
//
//   public put(url, body) {
//     return this.manageResponse(this.http.put(this.addBaseUrl(url), body, {headers: this.headers}));
//   }
//
//   public softDelete(url) {
//     return this.manageResponse(this.http.delete(this.addBaseUrl(url)));
//   }
//
//   private manageResponse(request: Observable<any>): Promise<any> {
//     return request
//       .toPromise()
//       .catch((error) => BackHttpService.handleError(error));
//   }
//
//   private managePatchResponse(request: Observable<any>): Promise<any> {
//     return request
//       .toPromise()
//       .then(response => response)
//       .catch((error) => BackHttpService.handleError(error));
//   }
//
//   private manageList(request: Observable<any>): Promise<any> {
//     return request.toPromise().then(response => {
//       const totalItems = response.headers.get('X-Total-Count');
//       const items = response.body;
//       return Promise.resolve({totalItems, items})
//     }).catch((error) => BackHttpService.handleError(error));
//   }
//
//   private addBaseUrl(url) {
//     if (!url.startsWith('/')) {
//       url = '/' + url;
//     }
//     return this.baseUrl + url;
//   }
//
// }

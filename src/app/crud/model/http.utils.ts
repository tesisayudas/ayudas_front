import {CrudUrls, GetAllUrlParameters} from "@g3/crud/model/item-crud.service";
import {Router} from "@angular/router";

export class HttpUtils {

  public static manageCrudUrls(urlName,
                               items: number[] = [],
                               getAllUrlParameters: GetAllUrlParameters = {page: 0, resultsPerPage: 10}): ManageUrls {
    return new ManageUrls(urlName, items, getAllUrlParameters);
  }

  /**
   * Method that manage the response errors
   * @param error
   * @param router
   * @returns {Promise<any>}
   */
  public static handleError(error: any, router: Router): Promise<any> {
    const {status} = error;
    if (status == 424 || status == 412 || status == 409 || status == 400) {
      return Promise.resolve(error);
    }
    return router.navigateByUrl(`error/${status}`)
  }


}


export class ManageUrls implements CrudUrls {

  constructor(protected urlName: { route: string, back: string },
              private items: number[],
              public parameters: GetAllUrlParameters) {
  }

  private getUrlWithEndSlash(url) {
    return !url.endsWith('/') ? url + '/' : url;
  }

  private getUrlWithoutEndSlash(url) {
    return !url.endsWith('/') ? url : url.substring(0, url.length - 1);
  }

  replaceURL() {
    const parts: string[] = this.urlName.route.split('{:id}');
    const _items = this.items.reverse().map(item => item.toString());
    let initLength = parts.length;
    if ((initLength - 1) !== this.items.length) throw new Error(`Was missing ${parts.length - 1} items but have been sended ${this.items.length}. Both must be equals.`);
    if ((initLength - 1) === 0 && this.items.length === 0) return `${this.urlName.back}/${this.urlName.route}`;
    for (let i = 1; i <= initLength; i++) {
      parts.splice(i, 0, _items.pop());
      initLength++;
      i++;
    }
    return `${this.urlName.back}/${parts.filter(item => item).join('')}`;
  }

  create(): string {
    return this.getUrlWithoutEndSlash(this.replaceURL());
  }

  update(): string {
    return this.getUrlWithEndSlash(this.replaceURL());
  }

  enable(): string {
    return this.getUrlWithEndSlash(this.replaceURL());
  }

  disable(): string {
    return this.getUrlWithEndSlash(this.replaceURL());
  }

  getById(): string {
    return this.getUrlWithEndSlash(this.replaceURL());
  }

  getAll(): string {
    return this._getAllUrl(this.getUrlWithoutEndSlash(this.replaceURL() + '/paginated'));
  }

  getEnabledList(): string {
    const url = {url: this.getUrlWithoutEndSlash(this.replaceURL())};
    this.addParameters(url, this.parameters.filter);
    return url.url;
  }

  basicUrl() {
    return this.getUrlWithoutEndSlash(this.replaceURL());
  }

  private static addParameter(urlObject: { url: string }, parameter: string, value: any) {
    if (value || value == 0) {
      if (parameter == 'page') {
        if (urlObject.url.includes('?')) {
          urlObject.url += `&${parameter}=${value + 1}`;
        } else {
          urlObject.url += `?${parameter}=${value + 1}`
        }
      } else {
        if (urlObject.url.includes('?')) {
          urlObject.url += `&${parameter}=${value}`;
        } else {
          urlObject.url += `?$${parameter}=${value}`
        }
      }
    }
  }

  public _getAllUrl(urlName: string): string {
    return `${urlName}?${this.getUrlParameters()}`;
  }

  public getUrlParameters(): string {
    let urlObject = {url: ''};
    ManageUrls.addParameter(urlObject, 'page', this.parameters.page);
    ManageUrls.addParameter(urlObject, 'limit', this.parameters.resultsPerPage);
    ManageUrls.addParameter(urlObject, 'del2', this.parameters.showDeleted);
    ManageUrls.addParameter(urlObject, 'sort', this.parameters.sortAttr);
    ManageUrls.addParameter(urlObject, 'enabled', this.parameters.enabled);
    this.addParameters(urlObject, this.parameters.filter);
    return urlObject.url.substring(1, urlObject.url.length);
  }

  private addParameters(urlObject: { url: string }, filters: Map<string, string>) {
    if (filters) {
      filters.forEach((value, key) => {
        if (value) {
          if (urlObject.url.includes('?')) {
            urlObject.url += `&${key}=${encodeURIComponent(value)}`
          } else {
            urlObject.url += `?${key}=${encodeURIComponent(value)}`
          }

        }
      });
    }
  }

}

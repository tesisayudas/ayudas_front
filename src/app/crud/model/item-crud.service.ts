import {Injectable} from '@angular/core';
import {ItemCrudModel} from "./model-interfaces";

/**
 * @deprecated
 */
// @Injectable()
// export class BackItemCrudService implements ItemCrudModel{
//
//   public urls: CrudUrls;
//
//   constructor(private _back: BackHttpService) {
//   }
//
//   public get back(){
//     return this._back;
//   }
//
//   public renew(id, body) {
//     this.validate();
//     return this._back.post(this.urls.update() + id + '/renewal', JSON.stringify(body));
//   }
//   public create(body) {
//     this.validate();
//     return this._back.post(this.urls.create(), JSON.stringify(body));
//   }
//
//   public update(body) {
//     this.validate();
//     return this._back.put(this.urls.update() + body.id, JSON.stringify(body));
//   }
//
//   public enable(id, enabled) {
//     this.validate();
//     return this._back.patch(this.urls.enable() + id, {enabled: enabled});
//   }
//
//   public disable(id) {
//     this.validate();
//     return this._back.delete(this.urls.disable() + id);
//   }
//
//   public getById(id) {
//     this.validate();
//     return this._back.get(this.urls.getById() + id);
//   }
//
//   public getAll() {
//     this.validate();
//     return this._back.list(this.urls.getAll());
//   }
//
//   public getEnabledList() {
//     this.validate();
//     return this._back.list(this.urls.getEnabledList());
//   }
//
//   softDelete(id) {
//     return this._back.softDelete(this.urls.getById() + id);
//   }
//
//   private validate() {
//     if (!this.urls) throw new Error("BackItemCrudService -> You must set the urls.");
//   }
// }

export interface CrudUrls {
  create(): string;

  update(): string;

  enable(): string;

  disable(): string;

  getById(): string;

  getAll(): string;

  getEnabledList(): string;

  basicUrl(): string

}

// /**
//  * @deprecated
//  */
// export class DefaultCrudUrls implements CrudUrls {
//
//   constructor(protected urlName: any, protected getAllUrl: GetAllUrl) {
//   }
//
//   public static getUrlWithEndSlash(url) {
//     return !url.endsWith('/') ? url + '/' : url;
//   }
//
//   public static getUrlWithoutEndSlash(url) {
//     return !url.endsWith('/') ? url : url.substring(0, url.length - 1);
//   }
//
//   create(): string {
//     return DefaultCrudUrls.getUrlWithoutEndSlash(this.urlName);
//   }
//
//   update(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.urlName);
//   }
//
//   enable(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.urlName);
//   }
//
//   getAll(): string {
//     return this.getAllUrl.getAllUrl(this.urlName + '/paginated');
//   }
//
//   disable(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.urlName);
//   }
//
//   getById(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.urlName);
//   }
//
//   getEnabledList(): string {
//     return DefaultCrudUrls.getUrlWithoutEndSlash(this.urlName);
//   }
//
//   basicUrl(): string {
//     return "";
//   }
//
// }
//
// /**
//  * @deprecated
//  */
// export class SubItemCrudUrls implements CrudUrls {
//
//   idItem: number;
//
//   constructor(protected urlName: any, idItem: number, protected getAllUrl: GetAllUrl) {
//     this.idItem = idItem;
//   }
//
//   replaceURL() {
//     return this.urlName.replace('{:id}', String(this.idItem));
//   }
//
//   create(): string {
//     return DefaultCrudUrls.getUrlWithoutEndSlash(this.replaceURL());
//   }
//
//   update(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.replaceURL());
//   }
//
//   enable(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.replaceURL());
//   }
//
//   disable(): string {
//     return DefaultCrudUrls.getUrlWithoutEndSlash(this.replaceURL());
//   }
//
//   getById(): string {
//     return DefaultCrudUrls.getUrlWithEndSlash(this.replaceURL());
//   }
//
//   getAll(): string {
//     return this.getAllUrl.getAllUrl(DefaultCrudUrls.getUrlWithoutEndSlash(this.replaceURL() + '/paginated'));
//   }
//
//   getEnabledList(): string {
//     return DefaultCrudUrls.getUrlWithoutEndSlash(this.replaceURL());
//   }
//
//   basicUrl(): string {
//     return "";
//   }
//
// }
//
// /**
//  * @deprecated
//  */
// export class GetAllUrl {
//
//   constructor(public parameters: GetAllUrlParameters) { }
//
//   private static addParameter(urlObject: { url: string }, parameter: string, value: any) {
//     if (value || value == 0) {
//       if (parameter == 'page') {
//         urlObject.url += `&${parameter}=${value + 1}`;
//       } else {
//         urlObject.url += `&${parameter}=${value}`;
//       }
//     }
//   }
//
//   public getAllUrl(urlName: string): string {
//     return `${urlName}?${this.getUrlParameters()}`;
//   }
//
//   public getUrlParameters(): string {
//     let urlObject = {url: ''};
//     GetAllUrl.addParameter(urlObject, 'page', this.parameters.page);
//     GetAllUrl.addParameter(urlObject, 'limit', this.parameters.resultsPerPage);
//     GetAllUrl.addParameter(urlObject, 'del2', this.parameters.showDeleted);
//     GetAllUrl.addParameter(urlObject, 'sort', this.parameters.sortAttr);
//     GetAllUrl.addParameter(urlObject, 'enabled', this.parameters.enabled);
//     this.addParameters(urlObject, this.parameters.filter);
//     return urlObject.url.substring(1, urlObject.url.length);
//   }
//
//   private addParameters(urlObject: {url: string}, filters: Map<string, string>) {
//     if(filters) {
//       filters.forEach((value, key) => {
//         if(value) {
//           urlObject.url +=  `&${key}=${encodeURIComponent(value)}`
//         }
//       });
//     }
//   }
//
// }

export class GetAllUrlParameters {
  page?: number;
  resultsPerPage?: number;
  showDeleted?: boolean;
  sortAttr?: string;
  enabled?: string;
  filter?: Map<string, string>
}


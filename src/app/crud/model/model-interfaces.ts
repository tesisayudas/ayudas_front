import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

export interface ItemListModel {
  list$: Observable<any>;
  actualTotalItems: number;
  updateList(): void;
}

export interface ItemPaginatorModel {
  setResultPerPage(resultPerPage: number);
  getResultPerPage(): number;
  setPage(page: number);
  getPage(): number;
}

export interface ItemDeleterModel {
  enable(itemId, enabled: boolean): Promise<any>;
  disable(itemId, deleted : boolean): Promise<any>;
}

export interface ItemSubmitterModel {
  create(item, refreshList: boolean): Promise<any>;
  update(item, refreshList: boolean): Promise<any>;
}

export interface ItemDetailModel {
  getById(itemId): Promise<any>;
}

export interface ItemCrudModel extends ItemSubmitterModel, ItemDeleterModel, ItemDetailModel {
  getAll(): Promise<any>;
}

export interface ItemCrudManager extends ItemListModel, ItemCrudModel {
}

export interface ItemCrudFilterMMager extends ItemCrudManager, ItemFilterableModel{

}

export interface ItemFilterableModel{
  setFilters(filters: Map<string, string> | null);
}

export interface ItemSearchableModel {
  search(terms: Observable<any>): Observable<any[]>;
  nextTerm(term: string);
  searchSubject(): Subject<any>;
  searchItems(value): Observable<any[]>;
}

export interface ItemDeletableModel {
  delete(id);
}

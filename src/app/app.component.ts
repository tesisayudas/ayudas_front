import {Component, OnInit} from '@angular/core';
import {PermissionService} from "@g3/core/auth/permission.service";
import {Router} from "@angular/router";

/**
 * Root component.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private permissions: PermissionService, private router: Router) { }

  ngOnInit(): void {
    if (this.permissions.token) {
      this.permissions.loadCurrentUser().subscribe()
    }
  }
}
